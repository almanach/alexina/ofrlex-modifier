# python3

import pprint, collections, sys, json, re, argparse
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import numpy as np

def getListIdsExcel(filePath):
    excelFile = pd.ExcelFile(filePath)
    df_entries = excelFile.parse("Feuil1")
    listIds = df_entries['__id__'].tolist()
    return listIds

def generateIdsLs(output, excelPath):
    verbLog = open(output, 'w')
    listIds = [ str(v) for v in getListIdsExcel(excelPath) ]
    verbLog.write( '\n'.join(listIds) )
    verbLog.close()

def lsFileToList(lsPath):
    idsFile = open(lsPath, 'r').read()
    ids = idsFile.strip('\n').split('\n')
    ids = [ int(v) for v in ids if v != 'x']
    return ids

def findDuplicates(listOfIds):
    return [item for item, count in collections.Counter(listOfIds).items() if count > 1]

def checkDiff(listToCheck, insideList):
    s = set(listToCheck)
    ids = set(insideList)
    print( len(s), len(ids) )
    missingOnes = [x for x in ids if x not in s]
    return missingOnes

# generateIdsLs('ids.ls', '../src/main/resources/lexicons/ofrlex4_verbs.xlsx')

ids = lsFileToList('ids.ls')
idsDb = lsFileToList('idsDB.ls')
idsVerbs = lsFileToList('idsVerbs.ls')
print( 'lists loaded' )

duplicates = findDuplicates(ids)
duplicatesVerbs = findDuplicates(idsVerbs)
duplicatesGlobal = duplicates + duplicatesVerbs
print(len(duplicates), 'duplicates in ids', len(duplicates))
print(len(duplicatesVerbs), 'duplicates in idsVerbs', len(duplicatesVerbs))
print(len(duplicatesGlobal), 'duplicates in all ofrlex', len(duplicatesGlobal))
print(ids[:3], ids[-3:], len(ids), len(ids)-len(duplicates))
print(idsDb[:3], idsDb[-3:], len(idsDb))

# missingOnes = [ i for i in idsDb if i not in ids ]

# print( set(ids) - set(idsDb))

# ids = [1,2,3]
# idsDb = [1,2]

diff = checkDiff(idsVerbs, ids)
# print( 'diff', diff)
print( len(diff))
commonElements = list(set(ids).intersection(idsVerbs))
print( 'common elements', len(commonElements) )

print( ids == idsDb )


s = set(idsDb)
ids = set(ids)
print( len(s), len(ids) )
missingOnes = [x for x in ids if x not in s]

print( 'missingOnes', len(missingOnes))
pprint.pprint(missingOnes)




exit()

# excelFile = pd.ExcelFile('../src/main/resources/lexicons/ofrlex4.xlsx')
# df_entries = excelFile.parse("Feuil1")
# rows = [row for index, row in df_entries.itertuples()]#.iterrows()]

ofrlex4 = open('ofrlex4.txt', 'r').read()
lines = ofrlex4.split('\n')
duplicatesEntries = [item for item, count in collections.Counter(lines).items() if count > 1]
print('duplicates ofrlex', duplicatesEntries)

ofrlex4_verbs = open('ofrlex4_verbs.txt', 'r').read()
lines = ofrlex4_verbs.split('\n')
duplicatesEntries = [item for item, count in collections.Counter(lines).items() if count > 1]
print('duplicates verbs', duplicatesEntries)
