package fr.inria.profiterole.almanach.ofrlexmodifier.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconOfrlex4;

@Repository
public interface LexiconOfrlexRepository extends PagingAndSortingRepository<LexiconOfrlex4, Long> {

	
//	(trecord REGEXP '^ALA[0-9]')
	// SELECT * FROM "PUBLIC"."OFRLEX" WHERE REGEXP_MATCHES(ofrlex_headword, '^deus')
	// SELECT * FROM "PUBLIC"."OFRLEX" WHERE  REGEXP_MATCHES(ofrlex_headword, '^amb.+?') OR REGEXP_MATCHES(ofrlex_headword, '^deus.+?')

	static final String QUERY_FILTER = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE id LIKE CONCAT('%',:p,'%') OR "
			+ "ofrlexHeadword LIKE CONCAT('%',:p,'%') OR "
			+ "cat LIKE CONCAT('%',:p,'%') OR "
			+ "tlhw LIKE CONCAT('%',:p,'%') OR "
			+ "tllemma LIKE CONCAT('%',:p,'%')";
	static final String QUERY_FILTER_ENDWITH = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE id LIKE CONCAT('%',:p) OR "
			+ "ofrlexHeadword LIKE CONCAT('%',:p) OR "
			+ "cat LIKE CONCAT('%',:p) OR "
			+ "tlhw LIKE CONCAT('%',:p) OR "
			+ "tllemma LIKE CONCAT('%',:p)";
	static final String QUERY_FILTER_STARTWITH = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE id LIKE CONCAT(:p,'%') OR "
			+ "ofrlexHeadword LIKE CONCAT(:p,'%') OR "
			+ "cat LIKE CONCAT(:p,'%') OR "
			+ "tlhw LIKE CONCAT(:p,'%') OR "
			+ "tllemma LIKE CONCAT(:p,'%')";
	static final String QUERY_FILTER_EXACT = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE "
			+ "ofrlexHeadword LIKE :p OR "
			+ "cat LIKE :p OR "
			+ "tlhw LIKE :p OR "
			+ "tllemma LIKE :p";
//	static final String QUERY_FILTER_REGEX = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE REGEXP_MATCHES(id, ':p') ";
	static final String QUERY_FILTER_CAT = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE cat LIKE CONCAT('%',:p,'%')";
	static final String QUERY_FILTER_CAT_STARTWITH = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE cat LIKE CONCAT(:p,'%')";
	static final String QUERY_FILTER_CAT_ENDWITH = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE cat LIKE CONCAT('%',:p)";
	static final String QUERY_FILTER_CAT_EXACT = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE cat LIKE :p";
	
	static final String QUERY_FILTER_HEADWORD = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE ofrlexHeadword LIKE CONCAT('%',:p,'%')";
	static final String QUERY_FILTER_HEADWORD_STARTWITH = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE ofrlexHeadword LIKE CONCAT(:p,'%')";
	static final String QUERY_FILTER_HEADWORD_ENDWITH = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE ofrlexHeadword LIKE CONCAT('%',:p)";
	static final String QUERY_FILTER_HEADWORD_EXACT = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE ofrlexHeadword LIKE :p";
	
	static final String QUERY_FILTER_FLEXION = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE inflclass LIKE CONCAT('%',:p,'%')";
	static final String QUERY_FILTER_FLEXION_STARTWITH = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE inflclass LIKE CONCAT(:p,'%')";
	static final String QUERY_FILTER_FLEXION_ENDWITH = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE inflclass LIKE CONCAT('%',:p)";
	static final String QUERY_FILTER_FLEXION_EXACT = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE inflclass LIKE :p";

	static final String QUERY_FILTER_REDISTRIBUTIONS = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE redistributions LIKE CONCAT('%',:p,'%')";
	static final String QUERY_FILTER_REDISTRIBUTIONS_STARTWITH = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE redistributions LIKE CONCAT(:p,'%')";
	static final String QUERY_FILTER_REDISTRIBUTIONS_ENDWITH = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE redistributions LIKE CONCAT('%',:p)";
	static final String QUERY_FILTER_REDISTRIBUTIONS_EXACT = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE redistributions LIKE :p";
	
	static final String QUERY_FILTER_SUBCAT = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE subcat LIKE CONCAT('%',:p,'%')";
	static final String QUERY_FILTER_SUBCAT_STARTWITH = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE subcat LIKE CONCAT(:p,'%')";
	static final String QUERY_FILTER_SUBCAT_ENDWITH = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE subcat LIKE CONCAT('%',:p)";
	static final String QUERY_FILTER_SUBCAT_EXACT = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE subcat LIKE :p";
	
	static final String QUERY_FIND_BY_IDS = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE str(id) IN :ids";
	
	static final String QUERY_FIND_VARIANTS = "SELECT ofrlex FROM LexiconOfrlex4 ofrlex WHERE tlvariant_of LIKE CONCAT('%',:p,'%') OR variant_of LIKE CONCAT('%',:p,'%')";

	static final String QUERY_COUNT_BY_VALIDATED = "SELECT COUNT(ofrlex) FROM LexiconOfrlex4 ofrlex WHERE validated = true";
	
	Page<LexiconOfrlex4> findAll(Pageable pageable);
	List<LexiconOfrlex4> findAll();
	List<LexiconOfrlex4> findByOfrlexHeadword(String headword);
	List<LexiconOfrlex4> findByCat(String cat);
	
	@Query(QUERY_COUNT_BY_VALIDATED)
	Long countByValidated();

	
	
	@Query(QUERY_FIND_VARIANTS)
	List<LexiconOfrlex4> findByVariantOfOrTlvariantOf(@Param("p") String variantOf);
	
	@Query(QUERY_FIND_BY_IDS)
	List<LexiconOfrlex4> findMultipleById(@Param("ids") List<String> ids); // query needed for bypassing strict type parameter inference
	
	@Query(QUERY_FILTER) 
    Page<LexiconOfrlex4> findByPattern(@Param("p") String pattern, Pageable pageable);
	
	@Query(QUERY_FILTER_STARTWITH)
	Page<LexiconOfrlex4> findByPatternStartWith(@Param("p") String pattern, Pageable pageable);
	
	@Query(QUERY_FILTER_ENDWITH)
	Page<LexiconOfrlex4> findByPatternEndWith(@Param("p") String pattern, Pageable pageable);
	
	@Query(QUERY_FILTER_EXACT)
	Page<LexiconOfrlex4> findByPatternExact(@Param("p") String pattern, Pageable pageable);
	
	@Query(QUERY_FILTER_CAT) 
    Page<LexiconOfrlex4> findCatByPattern(@Param("p") String pattern, Pageable pageable);
	@Query(QUERY_FILTER_CAT_STARTWITH) 
    Page<LexiconOfrlex4> findCatByPatternStartWith(@Param("p") String pattern, Pageable pageable);
	@Query(QUERY_FILTER_CAT_ENDWITH) 
    Page<LexiconOfrlex4> findCatByPatternEndWith(@Param("p") String pattern, Pageable pageable);
	@Query(QUERY_FILTER_CAT_EXACT) 
    Page<LexiconOfrlex4> findCatByPatternExact(@Param("p") String pattern, Pageable pageable);
	
	@Query(QUERY_FILTER_HEADWORD) 
    Page<LexiconOfrlex4> findHeadwordByPattern(@Param("p") String pattern, Pageable pageable);
	@Query(QUERY_FILTER_HEADWORD_ENDWITH) 
    Page<LexiconOfrlex4> findHeadwordByPatternEndWith(@Param("p") String pattern, Pageable pageable);
	@Query(QUERY_FILTER_HEADWORD_STARTWITH) 
    Page<LexiconOfrlex4> findHeadwordByPatternStartWith(@Param("p") String pattern, Pageable pageable);
	@Query(QUERY_FILTER_HEADWORD_EXACT) 
    Page<LexiconOfrlex4> findHeadwordByPatternExact(@Param("p") String pattern, Pageable pageable);
	
	@Query(QUERY_FILTER_FLEXION) 
    Page<LexiconOfrlex4> findFlexionByPattern(@Param("p") String pattern, Pageable pageable);
	@Query(QUERY_FILTER_FLEXION_STARTWITH) 
    Page<LexiconOfrlex4> findFlexionByPatternStartWith(@Param("p") String pattern, Pageable pageable);
	@Query(QUERY_FILTER_FLEXION_ENDWITH) 
    Page<LexiconOfrlex4> findFlexionByPatternEndWith(@Param("p") String pattern, Pageable pageable);
	@Query(QUERY_FILTER_FLEXION_EXACT) 
    Page<LexiconOfrlex4> findFlexionByPatternExact(@Param("p") String pattern, Pageable pageable);
	
	@Query(QUERY_FILTER_REDISTRIBUTIONS) 
    Page<LexiconOfrlex4> findRedistributionsByPattern(@Param("p") String pattern, Pageable pageable);
	@Query(QUERY_FILTER_REDISTRIBUTIONS_STARTWITH) 
    Page<LexiconOfrlex4> findRedistributionsByPatternStartWith(@Param("p") String pattern, Pageable pageable);
	@Query(QUERY_FILTER_REDISTRIBUTIONS_ENDWITH) 
    Page<LexiconOfrlex4> findRedistributionsByPatternEndWith(@Param("p") String pattern, Pageable pageable);
	@Query(QUERY_FILTER_REDISTRIBUTIONS_EXACT) 
    Page<LexiconOfrlex4> findRedistributionsByPatternExact(@Param("p") String pattern, Pageable pageable);
	
	@Query(QUERY_FILTER_SUBCAT) 
    Page<LexiconOfrlex4> findSubCatByPattern(@Param("p") String pattern, Pageable pageable);
	@Query(QUERY_FILTER_SUBCAT_STARTWITH) 
    Page<LexiconOfrlex4> findSubCatByPatternStartWith(@Param("p") String pattern, Pageable pageable);
	@Query(QUERY_FILTER_SUBCAT_ENDWITH) 
    Page<LexiconOfrlex4> findSubCatByPatternEndWith(@Param("p") String pattern, Pageable pageable);
	@Query(QUERY_FILTER_SUBCAT_EXACT) 
    Page<LexiconOfrlex4> findSubCatByPatternExact(@Param("p") String pattern, Pageable pageable);
	
	
	Page<LexiconOfrlex4> findByIdIn(List<Long> ids, Pageable pageable);
	

	

}
