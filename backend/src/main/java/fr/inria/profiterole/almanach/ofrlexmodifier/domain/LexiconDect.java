package fr.inria.profiterole.almanach.ofrlexmodifier.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="dect")
public class LexiconDect {

//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)


	@Id
	@GenericGenerator(name = "UseIdOrGenerate", strategy = "fr.inria.profiterole.almanach.ofrlexmodifier.repository.UseIdOrGenerate")
	@GeneratedValue(generator = "UseIdOrGenerate")
	@Column(unique = true, nullable = false)
	private Long id;
	private String godefroy;
	private String godefroyPage;
	private String tobler;
	private String toblerHeadword;
	private String entry;
	private String page;
	private String categoryMorphoSyntactic;
	private String subcat;
	private String lemma;
	private String freq;
	private String FB;
	private String fewEtymon;
	private String fewRef;
	private String TLF;
	private String dectAND;
	private String DMF;
	private String def;
	
	public LexiconDect() {}
	
	
	public LexiconDect(Long id, String godefroy, String godefroyPage, String tobler, String toblerHeadword,
			String entry, String page, String categoryMorphoSyntactic, String subcat, String lemma, String freq,
			String fB, String fewEtymon, String fewRef, String tLF, String dectAND, String dMF, String def) {
		super();
		this.id = id;
		this.godefroy = godefroy;
		this.godefroyPage = godefroyPage;
		this.tobler = tobler;
		this.toblerHeadword = toblerHeadword;
		this.entry = entry;
		this.page = page;
		this.categoryMorphoSyntactic = categoryMorphoSyntactic;
		this.subcat = subcat;
		this.lemma = lemma;
		this.freq = freq;
		FB = fB;
		this.fewEtymon = fewEtymon;
		this.fewRef = fewRef;
		TLF = tLF;
		this.dectAND = dectAND;
		DMF = dMF;
		this.def = def;
	}


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getGodefroy() {
		return godefroy;
	}
	public void setGodefroy(String godefroy) {
		this.godefroy = godefroy;
	}
	public String getGodefroyPage() {
		return godefroyPage;
	}
	public void setGodefroyPage(String godefroyPage) {
		this.godefroyPage = godefroyPage;
	}
	public String getTobler() {
		return tobler;
	}
	public void setTobler(String tobler) {
		this.tobler = tobler;
	}
	public String getToblerHeadword() {
		return toblerHeadword;
	}
	public void setToblerHeadword(String toblerHeadword) {
		this.toblerHeadword = toblerHeadword;
	}
	public String getEntry() {
		return entry;
	}
	public void setEntry(String entry) {
		this.entry = entry;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getCategoryMorphoSyntactic() {
		return categoryMorphoSyntactic;
	}
	public void setCategoryMorphoSyntactic(String categoryMorphoSyntactic) {
		this.categoryMorphoSyntactic = categoryMorphoSyntactic;
	}
	public String getSubcat() {
		return subcat;
	}
	public void setSubcat(String subcat) {
		this.subcat = subcat;
	}
	public String getLemma() {
		return lemma;
	}
	public void setLemma(String lemma) {
		this.lemma = lemma;
	}
	public String getFreq() {
		return freq;
	}
	public void setFreq(String freq) {
		this.freq = freq;
	}
	public String getFB() {
		return FB;
	}
	public void setFB(String fB) {
		FB = fB;
	}
	public String getFewEtymon() {
		return fewEtymon;
	}
	public void setFewEtymon(String fewEtymon) {
		this.fewEtymon = fewEtymon;
	}
	public String getFewRef() {
		return fewRef;
	}
	public void setFewRef(String fewRef) {
		this.fewRef = fewRef;
	}
	public String getTLF() {
		return TLF;
	}
	public void setTLF(String tLF) {
		TLF = tLF;
	}
	
	public String getDectAND() {
		return dectAND;
	}

	public void setDectAND(String dectAND) {
		this.dectAND = dectAND;
	}

	public String getDMF() {
		return DMF;
	}
	public void setDMF(String dMF) {
		DMF = dMF;
	}
	public String getDef() {
		return def;
	}
	public void setDef(String def) {
		this.def = def;
	}


	@Override
	public String toString() {
		return "LexiconDect [id=" + id + ", godefroy=" + godefroy + ", godefroyPage=" + godefroyPage + ", tobler="
				+ tobler + ", toblerHeadword=" + toblerHeadword + ", entry=" + entry + ", page=" + page
				+ ", categoryMorphoSyntactic=" + categoryMorphoSyntactic + ", subcat=" + subcat + ", lemma=" + lemma
				+ ", freq=" + freq + ", FB=" + FB + ", fewEtymon=" + fewEtymon + ", fewRef=" + fewRef + ", TLF=" + TLF
				+ ", dectAND=" + dectAND + ", DMF=" + DMF + ", def=" + def + "]";
	}

	
	
	
	
	
}
