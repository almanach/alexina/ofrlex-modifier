package fr.inria.profiterole.almanach.ofrlexmodifier.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Audited
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Pseudosyn implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "id")
    private String id;
	private Float distance;
	private String pos;
	private String form;
	private Boolean validated = false;
	@ManyToOne(fetch = FetchType.EAGER)
	@Cascade({CascadeType.SAVE_UPDATE, CascadeType.REFRESH})
	@JsonBackReference
	private LexiconOfrlex4 lexeme;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Float getDistance() {
		return distance;
	}
	public void setDistance(Float distance) {
		this.distance = distance;
	}
	public String getPos() {
		return pos;
	}
	public void setPos(String pos) {
		this.pos = pos;
	}
	public String getForm() {
		return form;
	}
	public void setForm(String form) {
		this.form = form;
	}
	public LexiconOfrlex4 getLexeme() {
		return lexeme;
	}
	public void setLexeme(LexiconOfrlex4 lexeme) {
		this.lexeme = lexeme;
	}
	public Boolean getValidated() {
		return validated;
	}
	public void setValidated(Boolean validated) {
		this.validated = validated;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
}
