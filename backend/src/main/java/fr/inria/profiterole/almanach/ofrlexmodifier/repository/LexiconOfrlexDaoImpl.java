package fr.inria.profiterole.almanach.ofrlexmodifier.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconOfrlex4;

@Transactional
@Repository
public class LexiconOfrlexDaoImpl implements LexiconOfrlexDao{

	@Autowired
	JdbcTemplate jt;
	
	@Override
	public List<LexiconOfrlex4> findByVariantOfOrTlvariantOf(String variantOf) {
		RowMapper<LexiconOfrlex4> rowMapper = new BeanPropertyRowMapper<LexiconOfrlex4>(LexiconOfrlex4.class);
		return jt.query("SELECT * FROM ofrlex WHERE variant_of = ?  OR tlvariant_of = ?", rowMapper, variantOf, variantOf);
	}
	
	@Override
	public List<String> getDifferentCats() {
		return jt.queryForList("SELECT DISTINCT cat FROM ofrlex WHERE cat != ''", String.class);
	}
	
	
	@Override
	public List<Long> getValidatorsIndexes() {
		return jt.queryForList("SELECT ofrlex_id FROM ofrlex_validations", Long.class);
	}
	
//	@Override
//	public List<LexiconOfrlex4> getCampaignEntries() {
//		RowMapper<LexiconOfrlex4> rowMapper = new BeanPropertyRowMapper<LexiconOfrlex4>(LexiconOfrlex4.class);
//		return jt.query("SELECT * FROM ofrlex WHERE variant_of = ?  OR tlvariant_of = ?", rowMapper, variantOf, variantOf);
//	}
	

}
