package fr.inria.profiterole.almanach.ofrlexmodifier.domain.dto;

import java.io.Serializable;


public class PseudosynSourceDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
    private String id;
	private Float distance;
	private String pos;
	private String form;
	private String sourceLexeme;
	private String sourcePos;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Float getDistance() {
		return distance;
	}
	public void setDistance(Float distance) {
		this.distance = distance;
	}
	public String getPos() {
		return pos;
	}
	public void setPos(String pos) {
		this.pos = pos;
	}
	public String getForm() {
		return form;
	}
	public void setForm(String form) {
		this.form = form;
	}
	public String getSourceLexeme() {
		return sourceLexeme;
	}
	public void setSourceLexeme(String sourceLexeme) {
		this.sourceLexeme = sourceLexeme;
	}
	public String getSourcePos() {
		return sourcePos;
	}
	public void setSourcePos(String sourcePos) {
		this.sourcePos = sourcePos;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "PseudosynSourceDto [id=" + id + ", distance=" + distance + ", pos=" + pos + ", form=" + form
				+ ", sourceLexeme=" + sourceLexeme + ", sourcePos=" + sourcePos + "]";
	}
	
	
	
	
}
