package fr.inria.profiterole.almanach.ofrlexmodifier.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconGodefroy;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.LexiconGodefroyDao;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.LexiconGodefroyRepository;

@Service
public class LexiconGodefroyService {
	
	@Autowired
	LexiconGodefroyRepository godefroyRepository;
	
	@Autowired
	LexiconGodefroyDao godefroyDao;
	
	public static final String GODEFROY_EXCEL = "lexicons/Godefroy.xlsx";
	
	private static final Logger log = LoggerFactory.getLogger(LexiconGodefroyService.class);
	
	public LexiconGodefroyService() {}
	
	@PostConstruct
    public void init() throws EncryptedDocumentException, InvalidFormatException, IOException {
		
//		godefroyRepository.deleteAll();
		
		if(godefroyRepository.count() < 1) {
		
	    	Workbook workbook = WorkbookFactory.create(getClass().getClassLoader().getResourceAsStream(GODEFROY_EXCEL));
	    	Sheet sheet = workbook.getSheetAt(0);
	    	Map<String, Integer> colMap = getLabelledColumns(sheet.getRow(0));
	    	sheet.removeRow(sheet.getRow(0)); // Remove header
	    	for (Row row: sheet) godefroyRepository.save(initRow(row, colMap));
//	    	DataFormatter dataFormatter = new DataFormatter();
//	    	for (Row row: sheet) {
//	    		
//	    		for(int i = 0; i < 9; i++) log.info("number {} = {}", i, dataFormatter.formatCellValue(row.getCell(0)));
//	    		
//	    		LexiconGodefroy gl = new LexiconGodefroy(dataFormatter.formatCellValue(row.getCell(0)),
//	    				dataFormatter.formatCellValue(row.getCell(1)), 
//	    				dataFormatter.formatCellValue(row.getCell(2)), 
//	    				dataFormatter.formatCellValue(row.getCell(3)),
//	    				dataFormatter.formatCellValue(row.getCell(4)), 
//	    				dataFormatter.formatCellValue(row.getCell(5)),
//	    				dataFormatter.formatCellValue(row.getCell(6))
//	    				);
//	    		log.info("{}", gl.getId());
//	    		godefroyRepository.save(gl);
//	        }
	    	log.info("Godefroy parsing SUCCESS with {} entries", godefroyRepository.count());
		}else {
			log.info("Godefroy data already in database. Skipping excel retrieval.");
		}
    }
	
	public LexiconGodefroy add(LexiconGodefroy entry) {
		return godefroyRepository.save(entry);
	}
	
	
	public void setEntries(List<LexiconGodefroy> entriesRow) {
		godefroyRepository.saveAll(entriesRow);
	}
	
	public List<LexiconGodefroy> getByForm(String form) {
//		return godefroyRepository.findByForm(form);
		return godefroyDao.findByForm(form);
	}
	
	public List<LexiconGodefroy> getEntries() {
		return godefroyRepository.findAll();
	}
	
	
	/**
	 * Because POI does not parse column names as a dataframe, with have to parse the header and put the name/index association into a map.
	 * @param header (POI Row class)
	 * @return Map<String, Integer> of Map<labelColumn, indexColumn>
	 */
	private Map<String, Integer> getLabelledColumns(Row header) {
		Map<String, Integer> map = new HashMap<String,Integer>();
    	short minColIx = header.getFirstCellNum(); //get the first column index for a row
    	short maxColIx = header.getLastCellNum(); //get the last column index for a row
    	for(short colIx=minColIx; colIx<maxColIx; colIx++) { //loop from first to last index
    	   Cell cell = header.getCell(colIx); //get the cell
    	   map.put(cell.getStringCellValue(),cell.getColumnIndex()); //add the cell contents (name of column) and cell index to the map
    	 }
    	return map;
	}
	
	/**
	 * Initialize the entry object of the lexicon with values based on the map of Labels and index columns.
	 * @param row A row from POI
	 * @param map String (label), Integer (index column) : example "Nom" , 2
	 * @return the entry object filled with values
	 */
	private LexiconGodefroy initRow(Row row, Map<String, Integer> map) {
		// form	pos	normedPos	unknown1	lemma	unknown2	pos2	definition	index
		DataFormatter dataFormatter = new DataFormatter();
		LexiconGodefroy gl = new LexiconGodefroy();
		gl.setForm( dataFormatter.formatCellValue(row.getCell(map.get("form"))) );
		gl.setPos(dataFormatter.formatCellValue(row.getCell(map.get("pos"))));
		gl.setNormedPOS(dataFormatter.formatCellValue(row.getCell(map.get("normedPos"))));
		gl.setLemmaVariation(dataFormatter.formatCellValue(row.getCell(map.get("lemma"))));
		gl.setPos2(dataFormatter.formatCellValue(row.getCell(map.get("pos2"))));
		gl.setDefinition(String.valueOf(dataFormatter.formatCellValue(row.getCell(map.get("definition"))) ) );
		gl.setIndexGL(dataFormatter.formatCellValue(row.getCell(map.get("index"))));
		return gl;
	}
	
	
	
	
}
