package fr.inria.profiterole.almanach.ofrlexmodifier.service;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.DBState;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.ValidationCampaign;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.DBStateRepository;

@Service
public class DBStateService {

	@Autowired
	DBStateRepository dr;
	
	@Autowired
	LexiconOfrlex4Service os;
	
	
	@PostConstruct
    public void init() {
		if(!dr.findById("ofrlex").isPresent()) {
			dr.save(new DBState(false, "ofrlex", "ofrlex", LocalDateTime.now().toString(), os.getEntriesCount(), os.getValidatedCount()) );
		}
	}
	
	public DBState toggle(String author, String target) {
		Optional<DBState> optDbs = dr.findById(target);
		if (optDbs.isPresent()) {
			DBState dbs = optDbs.get();
			dbs.setLocked(!dbs.isLocked());
			dbs.setAuthor(author);
			dbs.setDate(LocalDateTime.now().toString());
			return dr.save(dbs);
		}else {
			DBState dbs = new DBState(true, target, author, LocalDateTime.now().toString(), os.getEntriesCount(), os.getValidatedCount() );
			return dbs;
		}
	}
	
	public DBState getStatus(String target) {
		DBState dbs = dr.findById(target).get();
		dbs.setDate(dbs.getDate().replace("T", " ").replaceAll("\\.[0-9]+$", ""));
		dbs.setProgress(os.getValidatedCount());
		dbs.setTotal(os.getEntriesCount());
		return dbs;
	}
	
	public DBState startCampaign(ValidationCampaign vc, String target) {
		DBState dbs = dr.findById(target).get();
		dbs.setCampaignOngoing(true);
		dbs.setCampaign(vc.getId());
		DBState updated = dr.save(dbs);
		return updated;
	}
	
	public DBState resetCampaign(String target) {
		DBState dbs = dr.findById(target).get();
		dbs.setCampaignOngoing(false);
		dbs.setCampaign(null);
		DBState updated = dr.save(dbs);
		return updated;
	}
	
	public DBState endCampaign(String target) {
		DBState dbs = dr.findById(target).get();
		dbs.setCampaignOngoing(false);
		dbs.setCampaign(null);
		DBState updated = dr.save(dbs);
		return updated;
	}
	
}
