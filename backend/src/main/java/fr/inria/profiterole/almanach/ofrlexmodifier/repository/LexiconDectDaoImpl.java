package fr.inria.profiterole.almanach.ofrlexmodifier.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconDect;

@Transactional
@Repository
public class LexiconDectDaoImpl implements LexiconDectDao{

	@Autowired
	JdbcTemplate jt;
	
	@Override
	public LexiconDect findById(Long id) {
		String sql = "SELECT * FROM dect WHERE id = ?";
		RowMapper<LexiconDect> rowMapper = new BeanPropertyRowMapper<LexiconDect>(LexiconDect.class);
		return jt.queryForObject(sql, rowMapper, id);
	}

}
