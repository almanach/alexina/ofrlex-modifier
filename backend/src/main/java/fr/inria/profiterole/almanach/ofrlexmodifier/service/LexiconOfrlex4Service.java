package fr.inria.profiterole.almanach.ofrlexmodifier.service;

import java.beans.IntrospectionException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.google.common.base.Joiner;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multiset;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconOfrlex4;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.LexiconOfrlexDao;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.LexiconOfrlexRepository;
import fr.inria.profiterole.almanach.ofrlexmodifier.utils.ExcelGenerator;
import fr.inria.profiterole.almanach.ofrlexmodifier.utils.ShellTools;
import fr.inria.profiterole.almanach.ofrlexmodifier.utils.Tools;
import fr.inria.profiterole.almanach.ofrlexmodifier.utils.Utils;


@Service
public class LexiconOfrlex4Service {

	@Autowired
	LexiconOfrlexRepository or;
	
	@Autowired
	LexiconOfrlexDao dao;
	
	@Autowired
	ValidationCampaignService vcs;
	
	@Autowired
	AuditReader auditReader;
	
	private static final String OFRLEX4_EXCEL = "lexicons/ofrlex4.xlsx";
	private static final String OFRLEX4_VERBS_EXCEL = "lexicons/ofrlex4_verbs.xlsx";
	private static final String VERB_ILEX = "lexicons/VERB.ilex";
	private static final Map<String, String> TAG_MAP = Stream.of(new Object[][] { 
	    { "ADJ", "ADJ" }, 
	    { "ADP", "PRE" },
	    { "ADV", "ADV" },
	    { "AUX", "nopenopenope" },
	    { "CCONJ", "CONcoo" },
	    { "DET", "DET" },
	    { "INTJ", "INJ" },
	    { "NOUN", "NOMcom" },
	    { "NUM", "nopenopenope" },
	    { "PART", "nopenopenope" },
	    { "PRON", "PRO" },
	    { "PROPN", "NOMpro" },
	    { "PUNCT", "PON" },
	    { "SCONJ", "CONsub" },
	    { "SYM", "nopenopenope" },
	    { "VERB", "VER" },
	    { "X", "x" }		    
	}).collect(Collectors.toMap(data -> (String) data[0], data -> (String) data[1]));
	

	
	private static final Logger log = LoggerFactory.getLogger(LexiconOfrlex4Service.class);
	
	public LexiconOfrlex4Service() {}
	
	@PostConstruct
    public void init() throws EncryptedDocumentException, InvalidFormatException, IOException, NoSuchFieldException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		
//		log.info("ofrlesize {}", or.count());
//		System.exit(0);
		
		// // display diff ids
//		List<Integer> idsExcel = new ArrayList<Integer>(
//				Arrays.asList(1, 2)
//		);
		
//		List<String> idsDB = new ArrayList<String>();
//		for(LexiconOfrlex4 entry : or.findAll()) idsDB.add( String.valueOf(entry.getId()) );
//		Tools.ecrire("idsDB.ls", Joiner.on("\n").join(idsDB));
//		System.exit(0);
		
		// // display cat row numbers
//		List<String> cats = new ArrayList<String>( 
//	            Arrays.asList("ADJcar","ADJdem","ADJind","ADJint","ADJord","ADJpos","ADJqua",
//	            		"ADVgen","ADVint","ADVneg","ADVrel",
//	            		"CONcoo","CONsub","CONsub_o","CONsub_pre",
//	            		"DETcar","DETdef","DETdem","DETind","DETint","DETndf","DETpos","DETrel","DETrel_o",
//	            		"epsilon","INT",
//	            		"NOMcom","NOMpro",
//	            		"parentf","parento","poncts","ponctw",
//	            		"PRE", "PREFIX",
//	            		"PROcar", "PROdem", "PROimp","PROind","PROint","PROper","PROper_refl","PROpos","PROrel","PROrel_adv","PROrel_o")); 
//		for(String cat : cats) {
//			log.info("{} : {}", cat,  or.findByCat(cat).size() );
//		}
//		log.info("{} total entries", or.count());
//		System.exit(0);
		
//		for(LexiconOfrlex4 entry : or.findAll()) {
//			if(entry.getCat())
//		}
		
		
		
//		List<String> cats = Arrays.asList(new String[]{ "VER", "NOMcom", "PERpos", "NOMpro", "PROper"});
//		for(String cat : cats) log.info("{} : {}",cat, or.findByCat(cat).size());
		
		// // transfer info from real cat to manual cat in order for the editMask component to correctly display infos and colors
//		for(LexiconOfrlex4 entry : or.findAll()) {
//			if(entry.getManualCat() == null) {
//				entry.setManualCat(entry.getCat());
//				or.save(entry);
//			}
//		}
		
		
		
////		displayModifs();
//		ByteArrayInputStream byteArrayInputStream  = getExcel4Ilex();
////		Tools.ecrire("ofrlex4ilex.xlsx", outStr);
//		File newFile=new File("/Users/gguibon/eclipse-workspace/ofrlexexceltoilex/ofrlex4ilex.xlsx");
//		FileOutputStream fos = new FileOutputStream(newFile);
//		int data;
//		while((data=byteArrayInputStream.read())!=-1)
//		{
//		char ch = (char)data;
//		fos.write(ch);
//		}
//		fos.flush();
//		fos.close();
		
		
		
		
//		Tools.ecrire("/Users/gguibon/eclipse-workspace/ofrlexexceltoilex/ofrlex.tsv", toTSV(true));
//		System.exit(0);
		
//		// get data into latex table format
//		List<String> data = new ArrayList<String>();
//		for(String cat : getCategories()) data.add(String.format("\\bf %s & %s", cat, or.findByCat(cat).size() ) );
//		System.out.println(Joiner.on("\n").join(data) );
//		
//		System.out.println("====== coarse =====");
//		data = new ArrayList<String>();
//		for(Entry<String, List<String>> entry : getCoarseCategories().entrySet()) {
//			int count = 0;
//			for(String cat : entry.getValue()) count+= or.findByCat(cat).size();
//			data.add( String.format("\\bf %s & %s", entry.getKey(), count) );
//		}
//		System.out.println(Joiner.on("\n").join(data));
//		System.exit(0);
		
		
//		initAdjSubcat();
		
//		System.exit(0);
		
//		or.deleteAll();
//		putScatFromIlex();
		if(or.count() < 1) {
	    	Workbook workbook = WorkbookFactory.create(getClass().getClassLoader().getResourceAsStream(OFRLEX4_EXCEL));	
	    	Sheet sheet = workbook.getSheetAt(0);
	    	// Obtaining the columns labels from header
	    	Map<String, Integer> colMap = getLabelledColumns(sheet.getRow(0));
	    	// Remove header
	    	sheet.removeRow(sheet.getRow(0));
	    	// Fill each row into new entry. Add it to the list or repository.
	    	for (Row row: sheet )  initAndAdd(row, colMap, true);
	    	log.info("Ofrlex parsing DONE with {} entries", or.count() );
	    	
	    	
	    	
	    	workbook = WorkbookFactory.create(getClass().getClassLoader().getResourceAsStream(OFRLEX4_VERBS_EXCEL));
	    	sheet = workbook.getSheetAt(0);
	    	colMap = getLabelledColumns(sheet.getRow(0));
	    	sheet.removeRow(sheet.getRow(0));	
	    	for (Row row: sheet ) initAndAdd(row, colMap, true);
	    	log.info("Ofrlex and OfrlexVerbs parsing SUCCESS");
	    	
	    	
	    	putScatFromIlex();
	    	initAdjSubcat();
	    	addModifs();
	    	
	    	
		}else {
			log.info("Ofrlex data already in database. Skipping excel retrieval.");
		}
		
//		addMainVerbsRegnault(); // one shot
//		addInitMissingSourceInfos(); // one shot
    }
	
	/**
	 * One-shot method to extract verbs from one specific ilex (modified manually by mregnault) and persist it into the database.
	 * Should be applied only once, otherwise duplicates woud be created.
	 * @throws IOException
	 */
	private void addMainVerbsRegnault() throws IOException {
		InputStream is = getClass().getClassLoader().getResourceAsStream("lexicons/mregnaultVerbs.ilex");
		String content = IOUtils.toString(is, StandardCharsets.UTF_8.name());
		List<String> lines = Tools.StringToList(content);
		String verb = "";
		Map<String, Integer> counts = new HashMap<String, Integer>() {
			private static final long serialVersionUID = 1L;
			{
			    put("updated", 0);
			    put("added", 0);
			}};
		for(String line : lines) {
			String[] cols = line.split("\t");
			try{
				log.info(String.format("COL 2 = %s", cols[2]));
			}catch(Exception e) { log.error(String.format("error for %s", line)); } 
			// if first occurrence (for modified occurrences)
			if(!verb.equals(cols[0])) {
				verb = cols[0];
				List<LexiconOfrlex4> entries = or.findByOfrlexHeadword(cols[0]);
				if(entries.size() > 0) {
					LexiconOfrlex4 entry = null;
					// filter by pos
					for(LexiconOfrlex4 e : entries) {
						if(e.getCat().startsWith("VER")) { entry = e; break; }
					}
					if(entry != null) {
						String[] valuesCol2 = cols[2].split(";");
						String valuesToParse = Joiner.on(";").join( Arrays.copyOfRange(valuesCol2, 3, valuesCol2.length) );
						Map<String, String> syntInfos = parseSyntFeats( valuesToParse );
						entry.setSyntInfo(syntInfos.get("synt"));
						entry.setSubCat("subcat");
						entry.setRedistributions("redistribution");
						LexiconOfrlex4 saved = or.save(entry);
						log.info(String.format("UPDATED : %s", saved.toString()));
						counts.put("updated", counts.get("updated")+1);
						continue;
					}
				}
			}
			// for new occurrences
			else {
				LexiconOfrlex4 e = new LexiconOfrlex4();
				e.setCat("VER");
				e.setInflclass(cols[1]);
				String[] valuesCol2 = cols[2].split(";");
				String valuesToParse = Joiner.on(";").join( Arrays.copyOfRange(valuesCol2, 3, valuesCol2.length) );
				Map<String, String> infos = parseSyntFeats( valuesToParse );
				e.setSyntInfo(infos.get("synt"));
				e.setSubCat(infos.get("subcat"));
				e.setRedistributions(infos.get("redistribution"));
				e.setOfrlexHeadword(cols[0]);
				String glose = Joiner.on(" ").join( Arrays.copyOfRange(cols, 3, cols.length) );
				if(glose.length() != 0) e.setPseudoGlosses(glose);
				else e.setPseudoGlosses("");
				LexiconOfrlex4 saved = or.save(e);
				counts.put("added", counts.get("added")+1);
				log.info(String.format("ADDED : %s", saved.toString()));
			}
		}
		log.info(counts.toString());
		
	}
	
	
	/**
	 * Add missing infos for sources (DECT, etc) and map every entry with information (double or not)
	 * Hence the POJO contains every info the excel source file contained (no need for table relationship)
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	@SuppressWarnings("unused")
	private void addInitMissingSourceInfos() throws EncryptedDocumentException, InvalidFormatException, IOException {
		log.info("remapping for general excel START");
		Workbook workbook = WorkbookFactory.create(getClass().getClassLoader().getResourceAsStream(OFRLEX4_EXCEL));	
    	Sheet sheet = workbook.getSheetAt(0);
    	// Obtaining the columns labels from header
    	Map<String, Integer> colMap = getLabelledColumns(sheet.getRow(0));
    	// Remove header
    	sheet.removeRow(sheet.getRow(0));
    	for(Row row: sheet) initAndInsertSources(row, colMap, false);
    	
    	log.info("remapping for general excel DONE");
    	
    	log.info("remapping for verb excel START");
    	workbook = WorkbookFactory.create(getClass().getClassLoader().getResourceAsStream(OFRLEX4_VERBS_EXCEL));
    	sheet = workbook.getSheetAt(0);
    	colMap = getLabelledColumns(sheet.getRow(0));
    	sheet.removeRow(sheet.getRow(0));	
    	for (Row row: sheet ) initAndInsertSources(row, colMap, true); // boolean set to true because the source is the verbs excel
    	log.info("remapping for verb excel DONE");
	}
	
	/**
	 * Add a lexicon entry to the list (or the repository if database)
	 * @param entry Object mapping non calculated external values from the lexicon
	 * @return the added entry (useful to get the generated Id from database just in case)
	 */
	public LexiconOfrlex4 add(LexiconOfrlex4 entry) {
		entry.setManualCat(entry.getCat()); entry.setCat("");
		if(entry.getManualSubCat().isEmpty()) entry.setManualSubCat(entry.getSubCat()); 
		entry.setSubCat("");
		entry.setManualInflclass(entry.getInflclass()); entry.setInflclass("");
		entry.setManualRedistributions(entry.getRedistributions()); entry.setRedistributions("");
		entry.setManualSyntfeats(entry.getSyntInfo()); entry.setSyntInfo("");

		return or.save(entry);
	}
	
	/**
	 * Add a lexicon entry to the list (or the repository if database)
	 * @param entry Object mapping non calculated external values from the lexicon
	 * @return the added entry (useful to get the generated Id from database just in case)
	 */
	public LexiconOfrlex4 add(LexiconOfrlex4 entry, String username) {
		entry.setManualCat(entry.getCat()); entry.setCat("");
		if(entry.getManualSubCat().isEmpty()) entry.setManualSubCat(entry.getSubCat()); 
		entry.setSubCat("");
		entry.setManualInflclass(entry.getInflclass()); entry.setInflclass("");
		entry.setManualRedistributions(entry.getRedistributions()); entry.setRedistributions("");
		entry.setManualSyntfeats(entry.getSyntInfo()); entry.setSyntInfo("");
		entry.setEditor(username);
		return or.save(entry);
	}
	
	public void update(List<LexiconOfrlex4> entries) {
//		for(LexiconOfrlex4 entry : entries)
//			for(Pseudosyn ps : entry.getPseudosyns()) System.out.println(String.format("%s : %s", ps.getForm(), ps.getValidated()));
		or.saveAll(entries);
	}
	
	/**
	 * Update version for entries and log username. No field to be indicated.
	 * @param entries
	 * @param username
	 */
	public void update(List<LexiconOfrlex4> entries, String username) {
		for(LexiconOfrlex4 entry : entries) { entry.setEditor(username);  }
		or.saveAll(entries);
	}
	
	/**
	 * Update version for entries and log username. with indication for audited table
	 * @param entries
	 * @param username
	 * @param modifiedField
	 */
	public void update(List<LexiconOfrlex4> entries, String username, String modifiedField) {
		for(LexiconOfrlex4 entry : entries) { entry.setEditor(username); entry.setModifiedField(modifiedField);  }
		or.saveAll(entries);
	}
	
	/**
	 * Save or update one entry (to reduce post size) with indication for audited table
	 * @param entry
	 * @param username
	 * @param modifiedField
	 */
	public LexiconOfrlex4 update(LexiconOfrlex4 entry, String username, String modifiedField) {
		entry.setEditor(username); entry.setModifiedField(modifiedField);
		System.out.println(entry.toString());
		return or.save(entry);
	}
	 
	
	public void remove(Long id) {
		or.deleteById(id);
	}
	
	/**
	 * remove one entry and keep its username for audited table
	 * @param id
	 * @param username
	 */
	public void remove(Long id, String username) {
		LexiconOfrlex4 entry = or.findById(id).get(); entry.setEditor(username); 
		or.delete(entry);
	}
	
	public void removeMultiple(List<LexiconOfrlex4> entries) {
		or.deleteAll(entries);
	}

	/**
	 * remove multiple entries using the name for editor in audited table
	 * @param entries
	 * @param username
	 */
	public void removeMultiple(List<LexiconOfrlex4> entries, String username) {
		for(LexiconOfrlex4 entry : entries) entry.setEditor(username);
		or.deleteAll(entries);
	}
	
	/**
	 * Directly set the lexicon entries
	 * @param entriesRow
	 */
	public void setEntries(List<LexiconOfrlex4> entriesRow) {
		or.saveAll(entriesRow);
	}
	
	/**
	 * Directly set the lexicon entries
	 * @param entriesRow
	 * @param username String for audited table
	 */
	public void setEntries(List<LexiconOfrlex4> entriesRow, String username) {
		for(LexiconOfrlex4 entry : entriesRow) entry.setEditor(username);
		or.saveAll(entriesRow);
	}
	
	/**
	 * Retrieve the lexicon entries
	 * @return
	 */
	public List<LexiconOfrlex4> getEntries() {
		return or.findAll();
	}
	
	/**
	 * Retrieve the lexicon entries based on a list of ids
	 * Convenient method to avoid reparsing pages after an update.
	 * @param ids
	 * @return
	 */
	public List<LexiconOfrlex4> getEntries(List<Long> ids) {
		return (List<LexiconOfrlex4>) or.findAllById(ids);
	}
	
	
	
	/**
	 * Retrieve by headword
	 * @param headword
	 * @return
	 */
	public List<LexiconOfrlex4> getByHeadword(String headword) {
		return or.findByOfrlexHeadword(headword);		
	}
	
	/**
	 * retrieve by headword and pos. used for lexeme finding to link pseudosyns
	 * The pos is actually a pseudo matching between Upos and cattex
	 * @param headword
	 * @param pos
	 * @return
	 */
	public List<LexiconOfrlex4> getByHeadwordAndPos(String headword, String pos) {
		List<LexiconOfrlex4> sourceLexemes = new ArrayList<LexiconOfrlex4>();
		for(LexiconOfrlex4 entry : or.findByOfrlexHeadword(headword))
			if(entry.getCat().startsWith(TAG_MAP.get(pos))) sourceLexemes.add(entry);
		return sourceLexemes;
	}

	
	public List<LexiconOfrlex4> getVariantOf(String variantOf){
//		return or.findByVariantOfOrTlvariantOf(variantOf);
		return dao.findByVariantOfOrTlvariantOf(variantOf);
	}
	
	/**
	 * Filter method from repository. The filter is a SQL query
	 * @param pattern
	 * @param pageIndex
	 * @param PageSize
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public Map<String, Object> filter(String pattern, int pageIndex, int pageSize, String col, String type) throws UnsupportedEncodingException {
		Pageable pageable = PageRequest.of(pageIndex, pageSize, Sort.by("id"));
		Map<String, Object> res = new HashMap<String, Object>();
		String decodedPattern = Utils.decode(pattern);
		Page<LexiconOfrlex4> page;
		if(col.equals("cat")) {	page = or.findCatByPattern(decodedPattern, pageable); }
		else if(col.equals("ofrlexHeadword")) { 
			if(type.equals("startwith")) page = or.findHeadwordByPatternStartWith(decodedPattern, pageable);
			else if(type.equals("endwith")) page = or.findHeadwordByPatternEndWith(decodedPattern, pageable);
			else if(type.equals("exact")) page = or.findHeadwordByPatternExact(decodedPattern, pageable);
			else page = or.findHeadwordByPattern(decodedPattern, pageable); 
		}
		else if(col.equals("inflclass")) { 
			if(type.equals("startwith")) page = or.findFlexionByPatternStartWith(decodedPattern, pageable);
			else if(type.equals("endwith")) page = or.findFlexionByPatternEndWith(decodedPattern, pageable);
			else if(type.equals("exact")) page = or.findFlexionByPatternExact(decodedPattern, pageable);
			else page = or.findFlexionByPattern(decodedPattern, pageable); }
		else if(col.equals("redistributions")) { 
			if(type.equals("startwith")) page = or.findRedistributionsByPatternStartWith(decodedPattern, pageable);
			else if(type.equals("endwith"))page = or.findRedistributionsByPatternEndWith(decodedPattern, pageable);
			else if(type.equals("exact"))page = or.findRedistributionsByPatternExact(decodedPattern, pageable);
			else page = or.findRedistributionsByPattern(decodedPattern, pageable); }
		else if(col.equals("subCat")) { 
			if(type.equals("startwith")) page = or.findSubCatByPatternStartWith(decodedPattern, pageable);
			else if(type.equals("endwith")) page = or.findSubCatByPatternEndWith(decodedPattern, pageable);
			else if(type.equals("exact")) page = or.findSubCatByPatternExact(decodedPattern, pageable);
			else page = or.findSubCatByPattern(decodedPattern, pageable); }
		else {
			if(type.equals("startwith")) page = or.findByPatternStartWith(decodedPattern, pageable);
			else if(type.equals("endwith")) page = or.findByPatternEndWith(decodedPattern, pageable);
			else if(type.equals("exact")) page = or.findByPatternExact(decodedPattern, pageable);
			else page = or.findByPattern(decodedPattern, pageable); 
		}
		res.put("entries", page.getContent());
		res.put("totalPages", page.getTotalElements());
		return res;
	}
	
	public Map<String, Object> getSortedPage(int pageIndex, int pageSize, String sort, boolean descending) throws UnsupportedEncodingException  {
		Pageable pageable ;
		if(descending) pageable = PageRequest.of(pageIndex, pageSize, Sort.by(Utils.decode(sort)).descending());
		else pageable = PageRequest.of(pageIndex, pageSize, Sort.by(Utils.decode(sort)).ascending());
		Map<String, Object> res = new HashMap<String, Object>();
		Page<LexiconOfrlex4> page = or.findAll(pageable);
		res.put("entries", page.getContent());
		res.put("totalPages", page.getTotalElements());
		log.info("{} {} {} {}", pageIndex, pageSize, sort, descending);
		return res;
	}
	
	public Map<String, Object> filterAndSort(String pattern, int pageIndex, int pageSize, String sort, boolean descending) throws UnsupportedEncodingException {
		Pageable pageable ;
		if(descending) pageable = PageRequest.of(pageIndex, pageSize, Sort.by(Utils.decode(sort)).descending());
		else pageable = PageRequest.of(pageIndex, pageSize, Sort.by(Utils.decode(sort)).ascending());
		Map<String, Object> res = new HashMap<String, Object>();
		Page<LexiconOfrlex4> page = or.findByPattern(Utils.decode(pattern), pageable);
		res.put("entries", page.getContent());
		res.put("totalPages", page.getTotalElements());
		return res;
	}
	
	/**
	 * 
	 * Get the total number of entries in the table
	 * @return
	 */
	public Long getTotalEntries() {
		return or.count();
	}
	
	public Integer getNumberOfPages(int pageIndex, int PageSize) {
		Pageable pageable = PageRequest.of(pageIndex, PageSize, Sort.by("id"));
		return or.findAll(pageable).getTotalPages();
	}
	
	public Map<String, Object> getPage(int pageIndex, int pageSize) {
		Pageable pageable = PageRequest.of(pageIndex, pageSize, Sort.by("id"));
		Map<String, Object> res = new HashMap<String, Object>();
		Page<LexiconOfrlex4> page = or.findAll(pageable);
		res.put("entries", page.getContent());
		res.put("totalPages", page.getTotalElements());
		return res;
	}
	
	public Map<String, Object> getPageForValidation(int pageIndex, int pageSize) {
		Pageable pageable = PageRequest.of(pageIndex, pageSize, Sort.by("id"));
		Map<String, Object> res = new HashMap<String, Object>();
		Page<LexiconOfrlex4> page = or.findByIdIn(vcs.getTargetIds(), pageable);
		res.put("entries", page.getContent());
		res.put("totalPages", page.getTotalElements());
		return res;
	}
	
	public ByteArrayInputStream getExcel() throws NoSuchFieldException, SecurityException, IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		log.info("getExcel()");
		return ExcelGenerator.ofrlex2Excel(or.findAll());
	}
	
	public ByteArrayInputStream getExcel4Ilex() throws NoSuchFieldException, SecurityException, IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		log.info("getExcel4Ilex()");
		return ExcelGenerator.ofrlex2Excel4Ilex(or.findAll());
	}
	
	/**
	 * Get the Excel file containing selected rows from ofrlex. Rows are selected according to a list of ids
	 * @param ids List of String of row ids (entries)
	 * @return
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws IOException
	 * @throws IntrospectionException
	 */
	public ByteArrayInputStream getSelectionExcel(List<String> ids) throws NoSuchFieldException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException, IntrospectionException {
		return ExcelGenerator.ofrlex2Excel(or.findMultipleById(ids));
	}
	
//	public ByteArrayInputStream getCategoryExcels(List<String> categories) throws NoSuchFieldException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException, IntrospectionException {
//		List<ByteArrayInputStream> excels = new ArrayList<ByteArrayInputStream>();
//		for (String cat : categories) excels.add( ExcelGenerator.ofrlex2Excel( or.findByCat(cat)) );
//		return 
//	}
	
	/**
	 * Get inputstream for excel download file with entries for a specific category
	 * @param cat
	 * @return
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws IOException
	 * @throws IntrospectionException
	 */
	public ByteArrayInputStream getCategoryExcel(String cat) throws NoSuchFieldException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException, IntrospectionException {
		List<LexiconOfrlex4> lexemes = or.findByCat(cat);
		log.info("{} : {}", cat, lexemes.size());
		return ExcelGenerator.ofrlex2Excel( lexemes ) ; 
	}
	
	/**
	 * Get the different (non empty) categories from the ofrlex table (column: cat)
	 * @return list of String
	 */
	public List<String> getCategories() {
		return dao.getDifferentCats();
	}
	
	/**
	 * Get the different (non empty) categories from the ofrlex table (column: cat). Merge infos by meta cat : DET = DETdef + other DETxxx
	 * @return list of String
	 */
	public Map<String, List<String>> getCoarseCategories() {
		List<String> cats = dao.getDifferentCats();
		Map<String, List<String>> coarseOrga = new HashMap<String, List<String>>();
		for(String cat : cats) {
			if(cat.endsWith("_o")) continue;
			if(cat.matches("[A-Z]+[a-z_]+")) {
				String coarseCat = cat.replaceAll("[a-z_]", "");
				List<String> targets = coarseOrga.getOrDefault(coarseCat, new ArrayList<String>());
				targets.add(cat);
				coarseOrga.put(coarseCat, targets);
			}else {
				coarseOrga.put(cat, Arrays.asList(cat));
			}
		}
		return coarseOrga;
	}
	
	
	
	/**
	 * Update or override the lexicon based on a new imported excel file.
	 * For the update, if data does not exists it is created and no row deletion is performed.
	 * For the override, the table is emptied first hand.
	 * @param resource
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws IntrospectionException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 */
	public Map<String, String>  updateFromExcel(InputStream resource, boolean override) throws EncryptedDocumentException, InvalidFormatException, IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		if(override) or.deleteAll(); // empty ofrlex
		Workbook workbook = WorkbookFactory.create(resource);	
    	Sheet sheet = workbook.getSheetAt(0); // Obtaining the columns labels from header
    	Map<String, Integer> colMap = getLabelledColumns(sheet.getRow(0));
    	sheet.removeRow(sheet.getRow(0)); // Remove header
    	int adds = 0; int changes = 0;
    	for (Row row: sheet ) {
    		Map<String, String> res = initAndAddUpload(row, colMap);
    		if(res.get("type").equals("add")) adds += 1;
    		else if(res.get("type").equals("changed")) changes += 1;
    	}
    	if(override) log.info("ofrlex totally overrided.");
    	else log.info("ofrlex updated");
    	Map<String, String> modifs = Stream.of(new String[][] {
			  { "adds", String.valueOf(adds) }, 
			  { "changes", String.valueOf(changes) }
			}).collect(Collectors.toMap(data -> data[0], data -> data[1]));
    	return modifs;
	}
	
	/**
	 * 
	 * Faster and optimized version (batch save)
	 * Update or override the lexicon based on a new imported excel file.
	 * For the update, if data does not exists it is created and no row deletion is performed.
	 * For the override, the table is emptied first hand.
	 * @param resource
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws IntrospectionException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 */
	public void updateBatchFromExcel(InputStream resource, boolean override) throws EncryptedDocumentException, InvalidFormatException, IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		if(override) or.deleteAll(); // empty ofrlex
		Workbook workbook = WorkbookFactory.create(resource);	
    	Sheet sheet = workbook.getSheetAt(0); // Obtaining the columns labels from header
    	Map<String, Integer> colMap = getLabelledColumns(sheet.getRow(0));
    	sheet.removeRow(sheet.getRow(0)); // Remove header
    	List<LexiconOfrlex4> entries = new ArrayList<LexiconOfrlex4>();
    	int adds = 0; int changes = 0;
    	for (Row row: sheet ) {
    		LexiconOfrlex4 entry = initAndGetEntry(row, colMap);
    		if(entry != null) entries.add(entry);
    	}
    	
    	or.saveAll(entries);
    	
    	if(override) log.info("ofrlex totally overrided.");
    	else log.info("ofrlex updated");
	}
	
	/**
	 * Delete multiple entities based on an excel with id as first cell for each row
	 * @param resource
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public void deleteFromExcel(InputStream resource) throws EncryptedDocumentException, InvalidFormatException, IOException {
		Workbook workbook = WorkbookFactory.create(resource);
		Sheet sheet = workbook.getSheetAt(0);
		for (Row row: sheet ) {
			try {
				DataFormatter dataFormatter = new DataFormatter();
				Long id = Long.valueOf( dataFormatter.formatCellValue(  row.getCell(0)  ) );
				if(id != null) or.deleteById(id);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * get all entries with the argument category and transform it into .ilex 
	 * uses perl script behind it.
	 * @param cat
	 * @return stream of the ilex
	 */
	public ByteArrayInputStream getIlex(String cat) {
		Tools.ecrire(String.format("creaIlex/ofrlex_%s.tsv", cat ) , toTSV(true, cat));
		String ilex = ShellTools.generateIlex(cat);
		return new ByteArrayInputStream(ilex.getBytes(StandardCharsets.UTF_8));
	}
	
	/**
	 * get all entries with the argument category and transform it into .ilex 
	 * uses perl script behind it.
	 * @param cat
	 * @return string content of the ilex
	 */
	public String getIlexString(String cat) {
		Tools.ecrire(String.format("creaIlex/ofrlex_%s.tsv", cat ) , toTSV(true, cat));
		String ilex = ShellTools.generateIlex(cat);
		return ilex;
	}
	
	/**
	 * get all entries with the argument category and transform it into .ilex 
	 * merge contents into one ilex
	 * uses perl script behind it.
	 * @param cat
	 * @return stream of the ilex
	 */
	public ByteArrayInputStream getIlex(String cat, List<String> cats) {
		List<String> tsvs = new ArrayList<String>();
		String strHeaders = "__id__	OFrLex headword	scat	TLhw	TLlemma	TLhw ms	TL ref	TLhw_scat	inflclass	headword__scat	CAT	synt info	Godefroy (BS)	Godefroy cat(s) (BS)	Godefroy (entry)	Godefroy (pseudoCAT)	Godefroy (def)	Godefroy (ref)	GD (from DECT)	Godefroy page (from DECT)	TL (from DECT)	TL from DECT is hw	DECT entry	DECT page	DECT cat.ms	DECT scat	DECT lemma	DECT freq	F-B (from DECT)	FEW etymon (from DECT)	FEW ref (from DECT)	TLF (from DECT)	AND (from DECT)	DMF (from DECT)	DECT def	BFM freq	gloss fr	descendent	variant of by TL	variant of	descendent by TL	pseudo-glosses	Manual subcat	Manual syntfeats	Manual redistributions";
		List<String> headers = Arrays.asList(strHeaders.split("\t"));						
		tsvs.add(Joiner.on("\t").join(headers));
		for (String c : cats) tsvs.add( toTSV(false, c) );
		String tsv = Joiner.on("\n").join(tsvs);
		Tools.ecrire(String.format("creaIlex/ofrlex_%s.tsv", cat ) , tsv);
		String ilex = ShellTools.generateIlex(cat);
		return new ByteArrayInputStream(ilex.getBytes(StandardCharsets.UTF_8));
	}
	
	
	/**
	 * get all entires with the argument category and transforms it into tsv
	 * @param cat
	 * @return stream of the tsv
	 */
	public ByteArrayInputStream getTsv(String cat) {
		String tsv = toTSV(true, cat);
		return new ByteArrayInputStream( tsv.getBytes(StandardCharsets.UTF_8 ) );
	}
	
	/**
	 * get all entires with the argument category and transforms it into tsv
	 * merge contents into one tsv. allows meta category
	 * @param cat
	 * @return stream of the tsv
	 */
	public ByteArrayInputStream getTsv(String cat, List<String> cats) {
		List<String> tsvs = new ArrayList<String>();
		String strHeaders = "__id__	OFrLex headword	scat	TLhw	TLlemma	TLhw ms	TL ref	TLhw_scat	inflclass	headword__scat	CAT	synt info	Godefroy (BS)	Godefroy cat(s) (BS)	Godefroy (entry)	Godefroy (pseudoCAT)	Godefroy (def)	Godefroy (ref)	GD (from DECT)	Godefroy page (from DECT)	TL (from DECT)	TL from DECT is hw	DECT entry	DECT page	DECT cat.ms	DECT scat	DECT lemma	DECT freq	F-B (from DECT)	FEW etymon (from DECT)	FEW ref (from DECT)	TLF (from DECT)	AND (from DECT)	DMF (from DECT)	DECT def	BFM freq	gloss fr	descendent	variant of by TL	variant of	descendent by TL	pseudo-glosses	Manual subcat	Manual syntfeats	Manual redistributions";
		List<String> headers = Arrays.asList(strHeaders.split("\t"));						
		tsvs.add(Joiner.on("\t").join(headers));
		for (String c : cats) tsvs.add( toTSV(false, c) );
		String tsv = Joiner.on("\n").join(tsvs);
		return new ByteArrayInputStream( tsv.getBytes(StandardCharsets.UTF_8 ) );
	}
	
	/**
	 * get all entires with the argument category and transforms it into tsv
	 * merge contents into one tsv. allows meta category
	 * @param cat
	 * @return stream of the tsv
	 */
	public ByteArrayInputStream getTsvFromIds(String cat, List<Long> ids) {
		List<String> tsvs = new ArrayList<String>();
		String strHeaders = "__id__	OFrLex headword	scat	TLhw	TLlemma	TLhw ms	TL ref	TLhw_scat	inflclass	headword__scat	CAT	synt info	Godefroy (BS)	Godefroy cat(s) (BS)	Godefroy (entry)	Godefroy (pseudoCAT)	Godefroy (def)	Godefroy (ref)	GD (from DECT)	Godefroy page (from DECT)	TL (from DECT)	TL from DECT is hw	DECT entry	DECT page	DECT cat.ms	DECT scat	DECT lemma	DECT freq	F-B (from DECT)	FEW etymon (from DECT)	FEW ref (from DECT)	TLF (from DECT)	AND (from DECT)	DMF (from DECT)	DECT def	BFM freq	gloss fr	descendent	variant of by TL	variant of	descendent by TL	pseudo-glosses	Manual subcat	Manual syntfeats	Manual redistributions	validated";
		List<String> headers = Arrays.asList(strHeaders.split("\t"));						
		tsvs.add(Joiner.on("\t").join(headers));
		List<LexiconOfrlex4> entries = (List<LexiconOfrlex4>) or.findAllById(ids);
		
//		for ( Long id : ids) tsvs.add( prepaRow4Ilex(or.findById(id).get()) );
		
		for ( LexiconOfrlex4 entry : entries) 
			if(prepaRow4Ilex(entry).length() > 0) tsvs.add( prepaRow4Ilex(entry) );
		System.out.println(String.format("%s : %s : %s", cat, entries.size(), tsvs.size()));
		String tsv = Joiner.on("\n").join(tsvs);
		return new ByteArrayInputStream( tsv.getBytes(StandardCharsets.UTF_8 ) );
	}
	
	/**
	 * adapted version of the perl function by Benoît Sagot
	 * @param cat
	 * @param lemma
	 * @return
	 */
	public String catLemma2Upos(String cat, String lemma) {
		cat = cat.trim(); lemma = lemma.trim();
		if(cat.equals("ADJcar")) return "DET";
		if(cat.equals("ADJdem")) return "ADJ";
		if(cat.equals("ADJind")) return "DET";
		if(cat.equals("ADJint")) return "ADJ";
		  if(cat.equals("ADJord")) return "ADJ";
		  if(cat.equals("ADJpos")) return "ADJ";
		  if(cat.equals("ADJqua")) return "ADJ";
		  if(cat.equals("ADJgen")) return "ADV";
		  if(cat.equals("ADVint")) return "PRON";
		  if(cat.equals("ADJneg")) return "ADV";
		if(cat.equals("ADVrel")) return "ADV";
		  if(cat.equals("CONcoo")) return "CCONJ";
		if(cat.equals("CONsub") && lemma.equals("que")) return "SCONJ";
		  if(cat.equals("CONsub") && !lemma.equals("que")) return "SCONJ";
		  if(cat.equals("CONsub_o")) return "SCONJ";
				  if(cat.equals("CONsub_pre")) return "SCONJ";
				  if(cat.equals("DETcar")) return "DET";
//		  return "DET" if $cat eq "DETdef"; # define=+,det=+
				  if(cat.equals("DETdef")) return "DET";
//		  return "DET" if $cat eq "DETdem"; # demonstrative=+,det=+
				  if(cat.equals("DETdem")) return "DET";
//		  return "DET" if $cat eq "DETind"; # define=-,det=+
				  if(cat.equals("DETind")) return "DET";
//		  return "DET" if $cat eq "DETint"; # det=+,qu=+
				  if(cat.equals("DETint")) return "DET";
//		  return "DET" if $cat eq "DETndf"; # define=-,det=+
				  if(cat.equals("DETndf")) return "DET";
//		  return "DET" if $cat eq "DETpos"; # @poss,det=+
				  if(cat.equals("DETpos")) return "DET";
//		  return "PRON" if $cat eq "DETrel"; # @pro_nom|@pro_acc|@pro_gen|@pro_loc|ε
		  if(cat.equals("DETrel")) return "PRON";
//		  return "PRON" if $cat eq "DETrel_o";
		  if(cat.equals("DETrel_o")) return "PRON";
//		  return "INTJ" if $cat eq "INT";
		  if(cat.equals("INT")) return "INTJ";
//		  return "NOUN" if $cat eq "NOMcom";
		  if(cat.equals("NOMcom")) return "NOUN";
//		  return "PROPN" if $cat eq "NOMpro";
		  if(cat.equals("NOMpro")) return "PROPN";
//		  return "ADP" if $cat eq "PRE"; #?
				  if(cat.equals("PRE")) return "ADP";
//		  return "ADV" if $cat eq "PREFIX";
		  if(cat.equals("PREFIX")) return "ADV";
//		  return "PRON" if $cat eq "PROcar"; # ε
		  if(cat.equals("PROcar")) return "PRON";
//		  return "PRON" if $cat eq "PROdem"; # ε
		  if(cat.equals("PROdem")) return "PRON";
//		  return "PRON" if $cat eq "PROimp"; # imp=+
				  if(cat.equals("PROimp")) return "PRON";
//		  return "PRON" if $cat eq "PROind"; # define=-
				  if(cat.equals("PROind")) return "PRON";
//		  return "PRON" if $cat eq "PROint"; # @pro_nom|@pro_acc|ε
		  if(cat.equals("PROint")) return "PRON";
//		  return "PRON" if $cat eq "PROper"; # ε
		  if(cat.equals("PROper")) return "PRON";
//		  return "PRON" if $cat eq "PROper_refl"; # refl=+
				  if(cat.equals("PROper_refl")) return "PRON";
//		  return "ADJ" if $cat eq "PROpos";
		  if(cat.equals("PROpos")) return "ADJ";
//		  return "PRON" if $cat eq "PROrel"; # @pro_nom|@pro_acc|@pro_gen|@pro_loc|ε
		  if(cat.equals("PROrel")) return "PRON";
//		  return "PRON" if $cat eq "PROrel_adv"; # @pro_nom|@pro_acc|@pro_gen|@pro_loc|ε
		  if(cat.equals("PROrel_adv")) return "PRON";
//		  return "SCONJ" if $cat eq "PROrel_ind"; # wh=+
				  if(cat.equals("PROrel_ind")) return "SCONJ";
//		  return "PRON" if $cat eq "PROrel_o"; # @pro_nom|@pro_acc|@pro_gen|@pro_loc|ε
		  if(cat.equals("PROrel_o")) return "PRON";
//		  return "VERB" if $cat eq "VER";
		  if(cat.equals("VER")) return "VERB";
//		  return "PUNCT" if $cat =~ /^parentf|parento|poncts|ponctw$/;
		  if(cat.equals("parentf") || cat.equals("parento") || cat.equals("poncts") || cat.equals("ponctw")) return "PUNCT";
//		  return "epsilon" if $cat eq "epsilon";
		  if(cat.equals("epsilon")) return "epsilon";
		  else return "";
	}
	
	
	/**
	 * Retrieve LexiconOfrlex4 history as a json
	 * @return String json string representation
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public String getHistory() throws JsonGenerationException, JsonMappingException, IOException {
		AuditQuery query = auditReader.createQuery().forRevisionsOfEntity(LexiconOfrlex4.class, false, true)
				.addOrder(AuditEntity.revisionNumber().desc());
		List<Object[]> resultList = query.getResultList();
		List<Map<String, String>> listOfObj = new ArrayList<Map<String, String>>();
		for(Object[] o : resultList) {
			Map<String, String> map = new HashMap<String, String>();
			LexiconOfrlex4 entry = (LexiconOfrlex4) o[0];
			map.put("editor", entry.getEditor());
			map.put("modifiedField", entry.getModifiedField());
			DefaultRevisionEntity ri = (DefaultRevisionEntity) o[1];
			map.put("date", ri.getRevisionDate().toString());
			map.put("revisionId", String.valueOf(ri.getId()) );
			RevisionType revisionType = (RevisionType) o[2];
			map.put("type", revisionType.toString());
			
			if(revisionType.toString().equals("MOD")) {
				map.put("msg", String.format("Entry %s (%s) %s value was updated by %s", entry.getOfrlexHeadword(), String.valueOf(entry.getId()), entry.getModifiedField(), entry.getEditor() ) );
			}else {
				map.put("msg", String.format("Entry %s (%s) was %s by %s", entry.getOfrlexHeadword(), String.valueOf(entry.getId()), revisionType.toString(), entry.getEditor() ) );
			}
			
			listOfObj.add(map);
		}
		String json = new ObjectMapper().writeValueAsString(listOfObj);
		return json;
	}
	
	/**
	 * Validate all entries with validators size >= min param
	 * @param min
	 * @return list of validated entries
	 */
	public int validateEntriesByMinValidators(int min, String username){
		List<LexiconOfrlex4> entries = new ArrayList<LexiconOfrlex4>();
		
		List<Long> indexes = new ArrayList<Long>();
		Multiset<Long> multiset = HashMultiset.create();
		multiset.addAll(dao.getValidatorsIndexes());
		for (Multiset.Entry<Long> entry : multiset.entrySet()) {
	         if(entry.getCount()>=min) indexes.add(entry.getElement());
	    }
		
		for(LexiconOfrlex4 o : getEntries(indexes)){
			if(o.getValidators().size() >= min && !o.isValidated()) o.setValidated(true); entries.add(o);
		}
		
		update(entries, username, "validated");
		
		return entries.size();
	}
	
	/**
	 * Return the total entry count
	 * @return
	 */
	public Long getEntriesCount() {
		return or.count();
	}
	
	/**
	 * Return the tnumber of validated entries (final validation)
	 * @return
	 */
	public Long getValidatedCount() {
		return or.countByValidated();
	}
	
	
	/**
	 * Because POI does not parse column names as a dataframe, with have to parse the header and put the name/index association into a map.
	 * @param header (POI Row class)
	 * @return Map<String, Integer> of Map<labelColumn, indexColumn>
	 */
	private Map<String, Integer> getLabelledColumns(Row header) {
		Map<String, Integer> map = new HashMap<String,Integer>();
    	short minColIx = header.getFirstCellNum(); //get the first column index for a row
    	short maxColIx = header.getLastCellNum(); //get the last column index for a row
    	for(short colIx=minColIx; colIx<maxColIx; colIx++) { //loop from first to last index
    	   Cell cell = header.getCell(colIx); //get the cell
    	   map.put(cell.getStringCellValue(),cell.getColumnIndex()); //add the cell contents (name of column) and cell index to the map
    	 }
    	return map;
	}
	
	/**
	 * Initialize the entry object of the lexicon with values based on the map of Labels and index columns.
	 * @param row A row from POI
	 * @param map String (label), Integer (index column) : example "Nom" , 2
	 * @return the entry object filled with values
	 */
	private LexiconOfrlex4 initRow(Row row, Map<String, Integer> map, boolean init) {
		// __id__	OFrLex headword	scat	TLhw	TLlemma	TLhw ms	TL ref	TLhw_scat	inflclass	headword__scat	CAT	synt info	Godefroy (BSkey)	Godefroy cat(s) (BScatkey)	Godefroy (entry)	Godefroy (pseudoCAT)	Godefroy (def)	Godefroy (ref)	GD (from DECT)	GD page (from DECT)	TL (from DECT)	TL from DECT is hw	DECT entry	DECT page	DECT cat.ms	DECT scat	DECT lemma	DECT freq	F-B (from DECT)	FEW etymon (from DECT)	FEW ref (from DECT)	TLF (from DECT)	AND (from DECT)	DMF (from DECT)	DECT def	BFM freq	gloss fr	descendent	variant of by TL	variant of	descendent by TL	pseudo-glosses	Manual subcat	Manual syntfeats	Manual redistributions	
		DataFormatter dataFormatter = new DataFormatter();
		LexiconOfrlex4 gl = new LexiconOfrlex4();
		boolean syntEmpty = false;
		Map<String, String> parsedInfos = new HashMap<String, String>();
		if(dataFormatter.formatCellValue(row.getCell(map.get("synt info"))).equals("") ) syntEmpty = true;
		else parsedInfos = parseSyntFeats( dataFormatter.formatCellValue(row.getCell(map.get("synt info") )) );
		try {
//			gl.setId( String.format("%07d", Integer.parseInt( dataFormatter.formatCellValue(row.getCell(map.get("__id__"))) ) ) ) ;
			Long id = Long.parseLong( dataFormatter.formatCellValue(row.getCell(map.get("__id__"))) );
			if(init) if(or.findById(id).isPresent()) id += 700000;
			gl.setId(id);
			
//			gl.setId( Long.parseLong( dataFormatter.formatCellValue(row.getCell(map.get("__id__"))) ) )  ;
		}catch(Exception e) {
			log.error("id is not an integer! \nTrace: {}", e);
//			gl.setId( dataFormatter.formatCellValue(row.getCell(map.get("__id__"))) );
		}
		
		String tmp = "";
		
		gl.setOfrlexHeadword( dataFormatter.formatCellValue(row.getCell(map.get("OFrLex headword"))) );
		gl.setScat( dataFormatter.formatCellValue(row.getCell(map.get("scat"))) );
		gl.setTlhw( dataFormatter.formatCellValue(row.getCell(map.get("TLhw"))) );
		gl.setTllemma( dataFormatter.formatCellValue(row.getCell(map.get("TLlemma") )) ) ;
		gl.setTlhwms( dataFormatter.formatCellValue(row.getCell(map.get("TLhw ms") )) );
		gl.setTlref( dataFormatter.formatCellValue(row.getCell(map.get("TL ref") )) );
		gl.setTlhwscat( dataFormatter.formatCellValue(row.getCell(map.get("TLhw_scat") )) );
		if( !dataFormatter.formatCellValue(row.getCell(map.get("inflclass"))).equals("1") ) gl.setInflclass( dataFormatter.formatCellValue(row.getCell(map.get("inflclass") )) );
		gl.setHeadwordscat(  dataFormatter.formatCellValue(row.getCell(map.get("headword__scat") ))  );
		gl.setCat( dataFormatter.formatCellValue(row.getCell(map.get("CAT") )) );
		gl.setSubCat( parsedInfos.get("subcat") );
		if(!syntEmpty) gl.setSyntInfo( parsedInfos.get("synt") );
		else gl.setSyntInfo( dataFormatter.formatCellValue(row.getCell(map.get("synt info") ) ) );
		gl.setBfmfreq( dataFormatter.formatCellValue(row.getCell(map.get("BFM freq") )) ) ;
		gl.setGlossFr( dataFormatter.formatCellValue(row.getCell(map.get("gloss fr") )) );
		gl.setDescendent( dataFormatter.formatCellValue(row.getCell(map.get("descendent") )) );
		gl.setTlvariantOf( dataFormatter.formatCellValue(row.getCell(map.get("variant of by TL") )) );
		gl.setVariantOf( dataFormatter.formatCellValue(row.getCell(map.get("variant of") )) );
		gl.setTldescendent( dataFormatter.formatCellValue(row.getCell(map.get("descendent by TL") )) );
		gl.setPseudoGlosses( dataFormatter.formatCellValue(row.getCell(map.get("pseudo-glosses") )) );
		if(!syntEmpty) gl.setRedistributions( parsedInfos.get("redistribution") );
		else gl.setRedistributions("");
		
		
		tmp = dataFormatter.formatCellValue(row.getCell(map.get("Manual subcat") ));
		if( tmp.equals("") || tmp == null || tmp.isEmpty() ) gl.setManualSubCat( gl.getSubCat() );
		else gl.setManualSubCat( tmp );
		tmp = dataFormatter.formatCellValue(row.getCell(map.get("Manual syntfeats") ));
		if( tmp.equals("") ||  tmp == null || tmp.isEmpty()) gl.setManualSyntfeats( gl.getSyntInfo() );
		else gl.setManualSyntfeats( tmp );
		tmp = dataFormatter.formatCellValue(row.getCell(map.get("Manual redistributions") ));
		if( tmp.equals("") ||  tmp == null || tmp.isEmpty())  gl.setManualRedistributions( gl.getRedistributions() );
		else gl.setManualRedistributions( tmp ); 
		
		
		
		// non existing fields in source excel
		
		if( gl.getManualVariantOf() == "" || gl.getManualVariantOf() == null ) {
			if(!gl.getVariantOf().isEmpty()) gl.setManualVariantOf(gl.getVariantOf());
			else if(!gl.getTlvariantOf().isEmpty()) gl.setManualVariantOf(gl.getTlvariantOf());
		}
		if( gl.getManualCat() == "" || gl.getManualCat() == null || gl.getManualCat().isEmpty() ) 	gl.setManualCat( gl.getCat() );
		if(gl.getManualGlossFr() == "" || gl.getManualGlossFr() == null || gl.getManualGlossFr().isEmpty() ) gl.setManualGlossFr(gl.getGlossFr());
		if(gl.getManualDescendent() == "" || gl.getManualDescendent() == null || gl.getManualDescendent().isEmpty() ) gl.setManualDescendent(gl.getDescendent());
		if(gl.getManualPseudoGlosses() == "" || gl.getManualPseudoGlosses() == null || gl.getManualPseudoGlosses().isEmpty() ) gl.setManualPseudoGlosses(gl.getPseudoGlosses());
		if(gl.getManualInflclass() == "" || gl.getManualInflclass() == null || gl.getManualInflclass().isEmpty() ) gl.setManualInflclass(gl.getInflclass());
		
		return gl;
	}
	
	/**
	 * Initialize the entry object of the lexicon with values based on the map of Labels and index columns.
	 * @param row A row from POI
	 * @param map String (label), Integer (index column) : example "Nom" , 2
	 * @return the entry object filled with values
	 */
	private LexiconOfrlex4 initRowSourcesRetrieval(Row row, Map<String, Integer> map, boolean verbs, LexiconOfrlex4 gl) {
		// __id__	Godefroy (BSkey)	Godefroy cat(s) (BScatkey)	Godefroy (entry)	Godefroy (pseudoCAT)	Godefroy (def)	Godefroy (ref)	GD (from DECT)	GD page (from DECT)	TL (from DECT)	TL from DECT is hw	
//		DECT entry	DECT page	DECT cat.ms	DECT scat	DECT lemma	DECT freq	F-B (from DECT)	FEW etymon (from DECT)	FEW ref (from DECT)	TLF (from DECT)	AND (from DECT)	DMF (from DECT)	DECT def	
//		BFM freq	gloss fr	descendent	variant of by TL	variant of	descendent by TL	pseudo-glosses	Manual subcat	Manual syntfeats	Manual redistributions	
		DataFormatter dataFormatter = new DataFormatter();
		
		String tmp = "";
		
		if(!verbs) {
			gl.setGodefroyBS( dataFormatter.formatCellValue(row.getCell(map.get("Godefroy (BSkey)"))) );
			gl.setGodefroyCatBScatkey( dataFormatter.formatCellValue(row.getCell(map.get("Godefroy cat(s) (BScatkey)"))) );
		}else {
			gl.setGodefroyBS( dataFormatter.formatCellValue(row.getCell(map.get("Godefroy (BS)"))) );
			gl.setGodefroyCatBScatkey( dataFormatter.formatCellValue(row.getCell(map.get("Godefroy cat(s) (BS)"))) );
		}
		
		gl.setGodefroyEntry( dataFormatter.formatCellValue(row.getCell(map.get("Godefroy (entry)"))) );
		gl.setGodefroyPseudoCAT( dataFormatter.formatCellValue(row.getCell(map.get("Godefroy (pseudoCAT)"))) );
		gl.setGodefroyDef( dataFormatter.formatCellValue(row.getCell(map.get("Godefroy (def)"))) );
		gl.setGodefroyRef( dataFormatter.formatCellValue(row.getCell(map.get("Godefroy (ref)"))) );
		gl.setDectGD( dataFormatter.formatCellValue(row.getCell(map.get("GD (from DECT)"))) );
		gl.setDectGDpage( dataFormatter.formatCellValue(row.getCell(map.get("GD page (from DECT)"))) );
		gl.setDectTL( dataFormatter.formatCellValue(row.getCell(map.get("TL (from DECT)"))) );
		if(!verbs) gl.setDectTLhw( dataFormatter.formatCellValue(row.getCell(map.get("TL from DECT is hw"))) );
		else gl.setDectTLhw("");
		if(!verbs) gl.setDectEntry( dataFormatter.formatCellValue(row.getCell(map.get("DECT entry"))) );
		else gl.setDectEntry("");
		gl.setDectPage( dataFormatter.formatCellValue(row.getCell(map.get("DECT page"))) );
		gl.setDectCatMs( dataFormatter.formatCellValue(row.getCell(map.get("DECT cat.ms"))) );
		gl.setDectScat( dataFormatter.formatCellValue(row.getCell(map.get("DECT scat"))) );
		gl.setDectLemma( dataFormatter.formatCellValue(row.getCell(map.get("DECT lemma"))) );
		gl.setDectFreq( dataFormatter.formatCellValue(row.getCell(map.get("DECT freq"))) );
		gl.setDectFB( dataFormatter.formatCellValue(row.getCell(map.get("F-B (from DECT)"))) );
		gl.setDectEtymonFEW( dataFormatter.formatCellValue(row.getCell(map.get("FEW etymon (from DECT)"))) );
		gl.setDectRefFEW( dataFormatter.formatCellValue(row.getCell(map.get("FEW ref (from DECT)"))) );
		gl.setDectTLF( dataFormatter.formatCellValue(row.getCell(map.get("TLF (from DECT)"))) );
		gl.setDectAND( dataFormatter.formatCellValue(row.getCell(map.get("AND (from DECT)"))) );
		gl.setDectDMF( dataFormatter.formatCellValue(row.getCell(map.get("DMF (from DECT)"))) );
		gl.setDectDef( dataFormatter.formatCellValue(row.getCell(map.get("DECT def"))) );
		
		
		tmp = dataFormatter.formatCellValue(row.getCell(map.get("Manual subcat") ));
		if( tmp.equals("") || tmp == null || tmp.isEmpty() ) gl.setManualSubCat( gl.getSubCat() );
		else gl.setManualSubCat( tmp );
		tmp = dataFormatter.formatCellValue(row.getCell(map.get("Manual syntfeats") ));
		if( tmp.equals("") ||  tmp == null || tmp.isEmpty()) gl.setManualSyntfeats( gl.getSyntInfo() );
		else gl.setManualSyntfeats( tmp );
		tmp = dataFormatter.formatCellValue(row.getCell(map.get("Manual redistributions") ));
		if( tmp.equals("") ||  tmp == null || tmp.isEmpty())  gl.setManualRedistributions( gl.getRedistributions() );
		else gl.setManualRedistributions( tmp ); 

		return gl;
	}
	
	
	/**
	 * Initialize the entry object of the lexicon with values based on the map of Labels and index columns.
	 * @param row A row from POI
	 * @param map String (label), Integer (index column) : example "Nom" , 2
	 * @return the entry object filled with values
	 */
	private LexiconOfrlex4 initRowUpload(Row row, Map<String, Integer> map) {
		//id	ofrlexHeadword	tlhw	tllemma	tlhwms	tlref	tlhwscat	inflclass	headwordscat	cat	subCat	syntInfo	bfmfreq	glossFr	descendent	tlvariantOf	variantOf	tldescendent	pseudoGlosses	
		// redistributions	manualCat	manualSubCat	manualSyntfeats	manualRedistributions	manualVariantOf	manualGlossFr	manualPseudoGlosses	manualInflclass	manualDescendent
		DataFormatter dataFormatter = new DataFormatter();
		LexiconOfrlex4 gl = new LexiconOfrlex4();
		try {
			Long id = Long.parseLong( dataFormatter.formatCellValue(row.getCell(map.get("id"))) );
			gl.setId(id);
		}catch(Exception e) {
			log.error("id is not an integer! \nTrace: {}", e);
		}
		
		
		gl.setOfrlexHeadword( dataFormatter.formatCellValue(row.getCell(map.get("ofrlexHeadword"))) );
		if(map.get("scat") != null)  gl.setScat( dataFormatter.formatCellValue(row.getCell(map.get("scat"))) ); 
		gl.setTlhw( dataFormatter.formatCellValue(row.getCell(map.get("tlhw"))) );
		gl.setTllemma( dataFormatter.formatCellValue(row.getCell(map.get("tllemma") )) ) ;
		gl.setTlhwms( dataFormatter.formatCellValue(row.getCell(map.get("tlhwms") )) );
		gl.setTlref( dataFormatter.formatCellValue(row.getCell(map.get("tlref") )) );
		gl.setTlhwscat( dataFormatter.formatCellValue(row.getCell(map.get("tlhwscat") )) );
		gl.setInflclass( dataFormatter.formatCellValue(row.getCell(map.get("inflclass") )) );
		gl.setHeadwordscat(  dataFormatter.formatCellValue(row.getCell(map.get("headwordscat") ))  );
		gl.setCat( dataFormatter.formatCellValue(row.getCell(map.get("cat") )) );
		gl.setSubCat( dataFormatter.formatCellValue(row.getCell(map.get("subCat") )) );
		gl.setSyntInfo( dataFormatter.formatCellValue(row.getCell(map.get("syntInfo") )) );
		gl.setBfmfreq( dataFormatter.formatCellValue(row.getCell(map.get("bfmfreq") )) ) ;
		gl.setGlossFr( dataFormatter.formatCellValue(row.getCell(map.get("glossFr") )) );
		gl.setDescendent( dataFormatter.formatCellValue(row.getCell(map.get("descendent") )) );
		gl.setTlvariantOf( dataFormatter.formatCellValue(row.getCell(map.get("tlvariantOf") )) );
		gl.setVariantOf( dataFormatter.formatCellValue(row.getCell(map.get("variantOf") )) );
		gl.setTldescendent( dataFormatter.formatCellValue(row.getCell(map.get("tldescendent") )) );
		gl.setPseudoGlosses( dataFormatter.formatCellValue(row.getCell(map.get("pseudoGlosses") )) );
		gl.setRedistributions( dataFormatter.formatCellValue(row.getCell(map.get("redistributions") )) );	
		
		gl.setManualCat(  formatManualValue( dataFormatter.formatCellValue(row.getCell(map.get("manualCat") )),  gl.getCat()  ) );
		gl.setManualSubCat( formatManualValue( dataFormatter.formatCellValue(row.getCell(map.get("manualSubCat" )) ), gl.getSubCat() ) );
		gl.setManualSyntfeats( formatManualValue( dataFormatter.formatCellValue(row.getCell(map.get("manualSyntfeats" )) ), gl.getSyntInfo() ) );
		gl.setManualRedistributions( formatManualValue( dataFormatter.formatCellValue(row.getCell(map.get("manualRedistributions" )) ), gl.getRedistributions()   ) );
		gl.setManualGlossFr( formatManualValue( dataFormatter.formatCellValue(row.getCell(map.get("manualGlossFr" )) ), gl.getGlossFr() ) );
		gl.setManualPseudoGlosses( formatManualValue( dataFormatter.formatCellValue(row.getCell(map.get("manualPseudoGlosses" )) ), gl.getPseudoGlosses() ) );
		gl.setManualInflclass( formatManualValue( dataFormatter.formatCellValue(row.getCell(map.get("manualInflclass" )) ), gl.getInflclass() ) );
		gl.setManualDescendent( formatManualValue( dataFormatter.formatCellValue(row.getCell(map.get("manualDescendent" )) ), gl.getDescendent() ) );
		
		gl.setManualVariantOf( formatManualValue( dataFormatter.formatCellValue(row.getCell(map.get("manualVariantOf" )) ), gl.getVariantOf(), gl.getTlvariantOf() ) );
		return gl;
	}
	
	
	
	
	/**
	 * 
	 * Parse the syntactic informations which contains three parts : syntactic info; pos informations; redistributions
	 * @param syntFeats
	 * @return a hashmap with three keys : synt, subcat and redistribution
	 */
	private Map<String, String> parseSyntFeats(String syntFeats){
		Map<String, String> parsed = new HashMap<String, String>();
		String[] values = syntFeats.split(";"); // possible formats : <Suj:cln|sn>;cat=VER;%actif  <Suj:cln|sn>;cat=VER,@estre;%actif  ;cat=ADJqua,type=comp;%default
		parsed.put("subcat", values[0].replaceAll("(<|>)", ""));
		System.out.println(syntFeats);
		if(values.length > 1) parsed.put("synt", values[1]);
		else parsed.put("synt", "");
		if(values.length > 2) parsed.put("redistribution", values[2].replaceFirst("%", ""));
		else parsed.put("redistribution", "");
		return parsed;
	}
	
	/**
	 * Wrap the initRow method and the database population in order to ignore row with non integer id.
	 * @param row
	 * @param map
	 */
	private void initAndAdd(Row row, Map<String, Integer> map, boolean init ) {
		DataFormatter dataFormatter = new DataFormatter();
		boolean correctId = true;
		try {
			Long.parseLong( dataFormatter.formatCellValue(row.getCell(map.get("__id__"))) );
		}catch(Exception e) {
			correctId = false;
			log.debug("id is not an integer! : {} \n", dataFormatter.formatCellValue(row.getCell(map.get("__id__"))));
		}
		if(correctId) or.save(initRow(row, map, init));
	}
	
	/**
	 * Wrap the initRow method and the database population in order to ignore row with non integer id.
	 * @param row
	 * @param map
	 * @throws IntrospectionException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 */
	private Map<String, String> initAndAddUpload(Row row, Map<String, Integer> map ) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		DataFormatter dataFormatter = new DataFormatter();
		boolean correctId = true;
		Long id = Long.valueOf(0);
		try {
			id = Long.parseLong( dataFormatter.formatCellValue(row.getCell(map.get("id"))) );
		}catch(Exception e) {
			correctId = false;
			log.debug("id is not an integer! : {} \n", dataFormatter.formatCellValue(row.getCell(map.get("id"))));
		}
		Map<String, String> res = Stream.of(new String[][] {
			  { "type", "none" }, 
			  { "id", String.valueOf(id) }
			}).collect(Collectors.toMap(data -> data[0], data -> data[1]));
		if(correctId) {
			Optional<LexiconOfrlex4> beforeOpt = or.findById(id);
			LexiconOfrlex4 before = new LexiconOfrlex4();
			boolean add = false;
			if(!beforeOpt.isPresent()) {res.put("type", "add"); add = true;}
			else before = beforeOpt.get();
			LexiconOfrlex4 after = initRowUpload(row, map);
			boolean changed = compareModifs(before, after);
			if(changed && !add) res.put("type", "changed");
			or.save(after);
		}
		return res;
	}
	
	/**
	 * Wrap the initRow method and ignore row with non integer id.
	 * Returns null if the id is not correct or the LExiconOfrlex4 to save
	 * @param row
	 * @param map
	 * @throws IntrospectionException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 */
	private LexiconOfrlex4 initAndGetEntry(Row row, Map<String, Integer> map ) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		DataFormatter dataFormatter = new DataFormatter();
		boolean correctId = true;
		Long id = Long.valueOf(0);
		try {
			id = Long.parseLong( dataFormatter.formatCellValue(row.getCell(map.get("id"))) );
		}catch(Exception e) {
			correctId = false;
			log.debug("id is not an integer! : {} \n", dataFormatter.formatCellValue(row.getCell(map.get("id"))));
		}
		if(correctId) {
			LexiconOfrlex4 after = initRowUpload(row, map);
			return after;
		}
		return null;
	}
	
	
	/**
	 * Wrap the initRowSourcesRetireval method to update LexiconOfrlex4 with additional sources infos so all the source excel contents are in one POJO.
	 * For verbs, add 7 before the id to look for it.
	 * @param row
	 * @param map
	 */
	private void initAndInsertSources(Row row, Map<String, Integer> map, boolean verbs ) {
		DataFormatter dataFormatter = new DataFormatter();
		boolean correctId = true;
		try {
			Long.parseLong( dataFormatter.formatCellValue(row.getCell(map.get("__id__"))) );
		}catch(Exception e) {
			correctId = false;
			log.debug("id is not an integer! : {} \n", dataFormatter.formatCellValue(row.getCell(map.get("__id__"))));
		}
		
		if(correctId) {	
			Long id = Long.parseLong( dataFormatter.formatCellValue(row.getCell(map.get("__id__"))) );
			Optional<LexiconOfrlex4> opt = or.findById(id);
			LexiconOfrlex4 entry = new LexiconOfrlex4();
			if(opt.isPresent()) {
				entry = opt.get();
				entry = initRowSourcesRetrieval(row, map, verbs, entry);
				or.save(entry);
			}
			// check if a double exists (double or additions possess a 700000 bonus to still be able to link them
			Long otherPossibleId = id + 700000;
			if(or.existsById(otherPossibleId)) {
				// checks if the cat is the same (otherwise it is not a double but something new from which we should not dispatch infos)
				if(or.findById(otherPossibleId).get().getCat().equals( entry.getCat() )) {
				entry = or.findById(otherPossibleId).get();
				entry = initRowSourcesRetrieval(row, map, verbs, entry);
				or.save(entry);
				}
			}
		}
	}
	
	
	/**
	 * Compare two entries and iterate throught the public field to know if there is at least one modification.
	 * If modified, returns true. false otherwise.
	 * @param before
	 * @param after
	 * @return
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws IntrospectionException
	 */
	private boolean compareModifs(LexiconOfrlex4 before, LexiconOfrlex4 after) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		Field[] fields = LexiconOfrlex4.class.getDeclaredFields();
		List<String> columns = new ArrayList<String>();
		for (Field field : fields) columns.add(field.getName());
		for (int col = 0; col < columns.size(); col++) {
			if(col > 0) { 
				String b = (String) Tools.callGetter(before, columns.get(col)); 
				String a = (String) Tools.callGetter(after, columns.get(col));
				if(!a.equals(b)) return true;
			}
		}
		return false;
	}
	
	/**
	 * Replace the existing scat from excel file with the scat value comming from the create_ilex.pl
	 * @throws IOException
	 */
	private void putScatFromIlex() throws IOException {
		log.info("Inserting correct scat values in ofrlex");
		Map<String, Integer> colMap = Maps.newHashMap(ImmutableMap.of("id", 0, "form", 1, "cat", 2, "infos", 3, "comments", 4));
		Tools tools = new Tools();
		List<String> lines = tools.resource2Lines(VERB_ILEX);
		for(String line : lines) {
			String[] cols = line.split("\t");
			Optional<LexiconOfrlex4> entryOpt = or.findById( Long.valueOf( cols[ Integer.valueOf(colMap.get("id")) ]  ) );
			if(entryOpt.isPresent() && !line.isEmpty()) {
				LexiconOfrlex4 entry = entryOpt.get();
				// check if entry is one of the duplicate non verb that was move by modified id 
				if(!entry.getCat().equals("VER")) 
					entry = or.findById(entry.getId() + 700000).get();
				
				String[] infos = cols[ colMap.get("infos") ].split(";");
				if(infos.length > 1) {
					String subCat = infos[3].replaceAll("(<|>)", "");
					if(subCat != "") {
						entry.setSubCat(subCat);
//						if( entry.getManualSubCat() == null || entry.getManualSubCat().equals("") ) entry.setManualSubCat(entry.getSubCat());
						entry.setManualSubCat(subCat);
						or.save(entry);					
					}
				}
			}
		}
		log.info("Inserting correct scat values in ofrlex [DONE]");
	}
	
	/**
	 * Initialize all the ADJ with different auto value based on mregnault.
	 */
	private void initAdjSubcat() {
		log.info("Initializing ADJ subcats");
		for(LexiconOfrlex4 entry : or.findAll()) {
			if(entry.getCat().contains("ADJ")) {
				entry.setSubCat("Suj:(cln|sn)");
				if(entry.getManualSubCat() == "" || entry.getManualSubCat() == null || entry.getManualSubCat().isEmpty()) entry.setManualSubCat(entry.getSubCat());
				or.save(entry);
			}
		}
		log.info("ADJ subcats initialized.");
	}
	
	/**
	 * Analytic method to display modifications in order to stash them (for db overrride or changes backend)
	 * Ususally not used in production hence the suppressed warning
	 */
	@SuppressWarnings("unused")
	private void displayModifs() {
		// display modifications : cat, subcat, syntactic info, redistributions, variantof
		for(LexiconOfrlex4 entry : or.findAll()) {
			if(!entry.getCat().equals(entry.getManualCat())) {
				log.info("MODIF cat for {} ", entry.toString());
			}
			if(!entry.getSubCat().equals(entry.getManualSubCat())) {
				log.info("MODIF subcat for {} ", entry.toString());
			}
			if(!entry.getSyntInfo().equals(entry.getManualSyntfeats())) {
				log.info("MODIF syntinfo for {} ", entry.toString());
			}
			if(!entry.getRedistributions().equals(entry.getManualRedistributions())) {
				log.info("MODIF redistrib for {} ", entry.toString());
			}
			if(!entry.getInflclass().equals(entry.getManualInflclass())) {
				log.info("MODIF inflclass for {} ", entry.toString());
			}
			if(!entry.getDescendent().equals(entry.getManualDescendent())) {
				log.info("MODIF descendent for {} ", entry.toString());
			}
			if(!entry.getGlossFr().equals(entry.getManualGlossFr())) {
				log.info("MODIF gloss fr for {} ", entry.toString());
			}
			if(!entry.getInflclass().equals(entry.getManualInflclass())) {
				log.info("MODIF inflclass for {} ", entry.toString());
			}
			if(!entry.getPseudoGlosses().equals(entry.getManualPseudoGlosses())) {
				log.info("MODIF pseudo glosses for {} ", entry.toString());
			}
			if(entry.getManualVariantOf() != null) {
				String variant = "";
				String tlvariant = "";
				if(entry.getTlvariantOf() != null) 
					tlvariant = entry.getTlvariantOf(); 
				if(entry.getVariantOf() != null) 
					variant = entry.getVariantOf();
				if(!entry.getManualVariantOf().equals(tlvariant) && !entry.getManualVariantOf().equals(variant)) 
					log.info("MODIF variantof for {} ", entry.toString());
			}
			if(entry.getId() > 790372) log.info("ADDED {}", entry.toString());
			
		}
	}
	
	/**
	 * insert traced modifs. Would need better automation.
	 */
	@SuppressWarnings("unused")
	private void addModifs() {
		log.info("START insert modifs");
		LexiconOfrlex4 entry = or.findById(Long.valueOf(1629) ).get();
		entry.setManualSubCat("Suj:(cln|sn),Objde:(de-scompl|de-sinf|de-sn|en|scompl)");
		or.save(entry);
		entry = or.findById(Long.valueOf(39818) ).get();
		if(entry.getOfrlexHeadword().equals("penser")) {
			entry.setManualSubCat("Suj:cln|sn,Obj:(cla|sn|qcompl)");
			or.save(entry);
		}else {
			entry = or.findById(Long.valueOf(739818) ).get();
			if(entry.getOfrlexHeadword().equals("penser")) {
				entry.setManualSubCat("Suj:cln|sn,Obj:(cla|sn|qcompl)");
				or.save(entry);
			}
		}
		
		entry = new LexiconOfrlex4();
		entry.setOfrlexHeadword("einz");
		entry.setManualCat("ADVgen");
		entry.setInflclass("adv");
		entry.setPseudoGlosses("ainz");
		entry.setRedistributions("default");
		or.save(entry);
		log.info("DONE insert modifs");
	}
	
	/**
	 * Transforms Ofrlex into a tsv String
	 * @return
	 */
	private String toTSV(boolean header, String cat) {
//		log.info("creating tsv ofrlex");
		List<String> lines = new ArrayList<String>();
		if(header) {
			String strHeaders = "__id__	OFrLex headword	scat	TLhw	TLlemma	TLhw ms	TL ref	TLhw_scat	inflclass	headword__scat	CAT	synt info	Godefroy (BS)	Godefroy cat(s) (BS)	Godefroy (entry)	Godefroy (pseudoCAT)	Godefroy (def)	Godefroy (ref)	GD (from DECT)	Godefroy page (from DECT)	TL (from DECT)	TL from DECT is hw	DECT entry	DECT page	DECT cat.ms	DECT scat	DECT lemma	DECT freq	F-B (from DECT)	FEW etymon (from DECT)	FEW ref (from DECT)	TLF (from DECT)	AND (from DECT)	DMF (from DECT)	DECT def	BFM freq	gloss fr	descendent	variant of by TL	variant of	descendent by TL	pseudo-glosses	Manual subcat	Manual syntfeats	Manual redistributions";
			List<String> headers = Arrays.asList(strHeaders.split("\t"));							
			lines.add(Joiner.on("\t").join(headers));
		}
		for(LexiconOfrlex4 entry : or.findByCat(cat))  lines.add( prepaRow4Ilex(entry));
//		log.info("tsv ofrlex created");
		return Joiner.on("\n").join(lines);
	}
	
	
	
	/**
	 * prepare a row in the good format and good values for the tsv need for ilex creation with create_ilex2.pl
	 * @param entry LexiconOfrlex4
	 * @return String 
	 */
	private String prepaRow4Ilex(LexiconOfrlex4 entry) {
		List<String> cols = new ArrayList<String>();
		cols.add(  String.valueOf( entry.getId() ) );
		cols.add( entry.getOfrlexHeadword() );
		cols.add( entry.getScat()  );
		cols.add( entry.getTlhw()  );
		cols.add( entry.getTllemma()  );
		cols.add( entry.getTlhwms()  );
		cols.add( entry.getTlref()  );
		cols.add( entry.getTlhwscat()  );
		cols.add( entry.getInflclass()  );
		cols.add( entry.getHeadwordscat()  );
		cols.add( putManualForAuto(entry.getCat(), entry.getManualCat()) );
		cols.add( recreateSyntInfos(entry.getSubCat(), entry.getSyntInfo(), entry.getRedistributions(), entry.getManualSubCat(), entry.getManualSyntfeats(), entry.getManualRedistributions()) );
		cols.add( entry.getGodefroyBS() ); // Godefroy (BS)
		cols.add( entry.getGodefroyCatBScatkey() ); // Godefroy cat(s) (BS)
		cols.add( entry.getGodefroyEntry() ); // Godefroy (entry)
		cols.add( entry.getGodefroyPseudoCAT() ); // Godefroy (pseudoCAT)
		cols.add( entry.getGodefroyDef() ); // Godefroy (def)
		cols.add( entry.getGodefroyRef() ); // Godefroy (ref)
		cols.add( entry.getDectGD() ); // GD (from DECT)
		cols.add( entry.getDectGDpage() ); // Godefroy page (from DECT)
		cols.add( entry.getDectTL() ); // TL (from DECT)
		cols.add( entry.getDectTLhw() ); // TL from DECT is hw
		cols.add( entry.getDectEntry() ); // DECT entry
		cols.add( entry.getDectPage() ); // DECT page
		cols.add( entry.getDectCatMs() ); // DECT cat.ms
		cols.add( entry.getDectScat() ); // DECT scat
		cols.add( entry.getDectLemma() ); // DECT lemma
		cols.add( entry.getDectFreq() ); // DECT freq
		cols.add( entry.getDectFB() ); // F-B (from DECT)
		cols.add( entry.getDectEtymonFEW() ); // FEW etymon (from DECT)
		cols.add( entry.getDectRefFEW() ); // FEW ref (from DECT)
		cols.add( entry.getDectTLF() ); // TLF (from DECT)
		cols.add( entry.getDectAND() ); // AND (from DECT)
		cols.add( entry.getDectDMF() ); // DMF (from DECT)
		cols.add( entry.getDectDef() ); //DECT def
		cols.add( entry.getBfmfreq() );
		cols.add( putManualForAuto(entry.getGlossFr(), entry.getManualGlossFr()) );
		cols.add( putManualForAuto(entry.getDescendent(), entry.getManualDescendent()) );
		cols.add( entry.getTlvariantOf());
		cols.add( entry.getVariantOf() );//cols.add( putManualForAuto(entry.getVariantOf(), entry.getManualVariantOf()) );
		cols.add( entry.getTldescendent() );
		cols.add( putManualForAuto(entry.getPseudoGlosses(), entry.getManualPseudoGlosses()) );
		cols.add( formatAndSelectValue(entry.getSubCat(),entry.getManualSubCat()) );
		cols.add( formatAndSelectValue(entry.getSyntInfo(),entry.getManualSyntfeats()) );
		cols.add( formatAndSelectValue(entry.getRedistributions(),entry.getManualRedistributions()) );
		cols.add( String.valueOf( entry.isValidated() ) );
		try { 
			return Joiner.on("\t").join(cols);
		}catch(Exception e) {
			List<String> newCols = new ArrayList<String>();
			for(String col : cols)
				if(col == null || col.isEmpty()) newCols.add("");
				else newCols.add(col);
			return Joiner.on("\t").join(newCols);
		}
	}
	
	private static String recreateSyntInfos(String subCat, String synt, String redist, String manSubCat, String manSynt, String manRedist) {
		//<Suj:cln|sn>;cat=VER;%actif
		String scat = ""; String syntfeats = ""; String redistributions = "";
		subCat = emptyIfNull(subCat); synt = emptyIfNull(synt); redist = emptyIfNull(redist); manSubCat = emptyIfNull(manSubCat); manSynt = emptyIfNull(manSynt); manRedist = emptyIfNull(manRedist);
		if(subCat.equals(manSubCat)) scat = subCat; else scat = manSubCat;
		if(synt.equals(manSynt)) syntfeats = synt; else syntfeats = manSynt;
		if(redist.equals(manRedist)) redistributions = redist; else redistributions = manRedist;
		return String.format("<%s>;%s;%%%s", scat, syntfeats, redistributions);
	}
	
	/**
	 * format the manual value given its value compared with the auto value. Put a ! if the manual value is the same as auto (meaning that it is verified).
	 * @param auto
	 * @param manual
	 * @return String value to insert into a manual property of the lexeme POJO
	 */
	private static String formatManualValue(String manual, String auto) {
		auto = emptyIfNull(auto); manual = emptyIfNull(manual);
		if(manual.isEmpty()) return auto;
		else if(manual.equals(auto)) return String.format("!%s", manual);
		else return manual;
	}
	
	/**
	 * format the manual value given its value compared with the auto value. Put a ! if the manual value is the same as auto (meaning that it is verified).
	 * Version with 2 args for the variantOf. Prioritize the auto value in manual instead of auto2
	 * @param auto
	 * @param manual
	 * @return String value to insert into a manual property of the lexeme POJO
	 */
	private static String formatManualValue(String manual, String auto, String auto2) {
		auto = emptyIfNull(auto); manual = emptyIfNull(manual); auto2 = emptyIfNull(auto2);
		if(manual.isEmpty()) {
			if(auto.isEmpty()) return auto2;
			else return auto;
		}else if(manual.equals(auto) || manual.equals(auto2)) return String.format("!%s", manual);
		else return manual;
	}
	
	/**
	 * For tsv creation : get the good value for auto row and removes validation indications
	 * @param manual
	 * @param auto
	 * @return
	 */
	private static String putManualForAuto(String manual, String auto) {
		auto = emptyIfNull(auto); manual = emptyIfNull(manual);
		if(manual.isEmpty()) return auto;
		else if(manual.equals(auto)) return auto;
		else return manual.replaceFirst("^!", "");
	}
	
	private static String emptyIfNull(String value) {
		if(value == null) return "";  else return value;
	}
	
	/**
	 * Format the manual value given a comparison with the auto value and the check for ! char which indicates a manual validation of the auto value.
	 * @param auto
	 * @param manual
	 * @return a String never null
	 */
	private static String formatAndSelectValue(String auto, String manual) {
		auto = emptyIfNull(auto); manual = emptyIfNull(manual);
		boolean verified = manual.startsWith("!");
		manual = manual.replaceFirst("!", "");
		if(auto.equals(manual)){
			if(verified) return manual;
			else return "";
		} else return manual;
	}
	
	/**
	 * Format the manual value given a comparison with the auto value and the check for ! char which indicates a manual validation of the 2 auto values (needed for variantOf).
	 * Prioritize auto over auto2 if equal
	 * @param auto
	 * @param auto2
	 * @param manual
	 * @return a String never null
	 */
	private static String formatAndSelectValue(String auto, String auto2, String manual) {
		auto = emptyIfNull(auto); auto2 = emptyIfNull(auto2); manual = emptyIfNull(manual);
		boolean verified = manual.startsWith("!");
		manual = manual.replaceFirst("!", "");
		if(auto.equals(manual) || auto2.equals(manual)) {
			if(verified)return manual;
			else return "";
		} else return manual;
	}
	
	
	/**
	 * Delete multiple entries based on a file (one id per line).
	 * Useful for some batch operations, hence the unused warning suppressed
	 * @param pathFileLs
	 * @throws IOException
	 */
	@SuppressWarnings("unused")
	private void deleteEntriesBasedOnFilels(String pathFileLs) throws IOException {
		String content = Tools.readFile(pathFileLs);
		List<String> ids = Arrays.asList(content.split("\n"));
		for(String id : ids) { or.deleteById( Long.valueOf(id) ); log.info("DLETED : {}", id); }
	}
	
	
	
	
}
