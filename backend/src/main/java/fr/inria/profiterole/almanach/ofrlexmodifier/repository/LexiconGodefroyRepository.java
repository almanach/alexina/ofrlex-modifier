package fr.inria.profiterole.almanach.ofrlexmodifier.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconGodefroy;

@Repository
public interface LexiconGodefroyRepository extends CrudRepository<LexiconGodefroy, Long>{
	
	static final String QUERY_FORM = "SELECT godefroy FROM LexiconGodefroy godefroy WHERE form LIKE CONCAT('%',:p,'%')";
	
	public List<LexiconGodefroy> findAll();
	@Query(QUERY_FORM)
	public List<LexiconGodefroy> findByForm(@Param("p") String form);
}
