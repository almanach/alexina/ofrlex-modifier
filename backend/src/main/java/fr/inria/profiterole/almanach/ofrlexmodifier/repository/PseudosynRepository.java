package fr.inria.profiterole.almanach.ofrlexmodifier.repository;


import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.Pseudosyn;

@Repository
public interface PseudosynRepository extends PagingAndSortingRepository<Pseudosyn, String> {
	List<Pseudosyn> findAll();
}
