package fr.inria.profiterole.almanach.ofrlexmodifier.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="godefroy")
public class LexiconGodefroy {
	
	@Id
	@GenericGenerator(name = "UseIdOrGenerate", strategy = "fr.inria.profiterole.almanach.ofrlexmodifier.repository.UseIdOrGenerate")
	@GeneratedValue(generator = "UseIdOrGenerate")
	@Column(unique = true, nullable = false)
	private Long id;
	private String form;
	private String pos;
	private String normedPOS;
	private String lemmaVariation;
	private String pos2;
	@Lob
	@Column(length = 3000)
	private String definition;
	private String indexGL;
	
	public LexiconGodefroy() {}
	
	public LexiconGodefroy(String form, String pos, String normedPOS, String lemmaVariation,
			 String pOS2, String definition, String indexGL) {
		super();
		this.form = form;
		this.pos = pos;
		this.normedPOS = normedPOS;
		this.lemmaVariation = lemmaVariation;
		this.pos2 = pOS2;
		this.definition = definition;
		this.indexGL = indexGL;
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getForm() {
		return form;
	}
	public void setForm(String form) {
		this.form = form;
	}
	public String getPos() {
		return pos;
	}
	public void setPos(String pos) {
		this.pos = pos;
	}
	public String getNormedPOS() {
		return normedPOS;
	}
	public void setNormedPOS(String normedPOS) {
		this.normedPOS = normedPOS;
	}
	
	public String getLemmaVariation() {
		return lemmaVariation;
	}
	public void setLemmaVariation(String lemmaVariation) {
		this.lemmaVariation = lemmaVariation;
	}
	
	public String getPos2() {
		return pos2;
	}
	public void setPos2(String pOS) {
		pos2 = pOS;
	}
	public String getDefinition() {
		return definition;
	}
	public void setDefinition(String definition) {
		this.definition = definition;
	}
	public String getIndexGL() {
		return indexGL;
	}
	public void setIndexGL(String indexGL) {
		this.indexGL = indexGL;
	}

	@Override
	public String toString() {
		return "LexiconGodefroy [id=" + id + ", form=" + form + ", pos=" + pos + ", normedPOS=" + normedPOS
				 + ", lemmaVariation=" + lemmaVariation  + ", pos2="
				+ pos2 + ", definition=" + definition + ", indexGL=" + indexGL + "]";
	}

	
	
	
	
	
}
