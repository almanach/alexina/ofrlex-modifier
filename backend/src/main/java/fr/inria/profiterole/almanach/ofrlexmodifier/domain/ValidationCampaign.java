package fr.inria.profiterole.almanach.ofrlexmodifier.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Audited
public class ValidationCampaign {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String author="";
	@Basic
	@Temporal(TemporalType.DATE)
	private java.util.Date startDate;
	@Basic
	@Temporal(TemporalType.DATE)
	private java.util.Date endDate;
	
//	@OneToMany( fetch = FetchType.EAGER, mappedBy = "validationCampaign")
//	@Cascade({CascadeType.SAVE_UPDATE, CascadeType.MERGE, CascadeType.REFRESH})
//	@JsonManagedReference
//	private List<LexiconOfrlex4> targets = new ArrayList<LexiconOfrlex4>();
	
	@ManyToMany(fetch = FetchType.EAGER)
	@Cascade({CascadeType.SAVE_UPDATE, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(
            name = "vc_ofrlex",
            joinColumns = @JoinColumn(
                    name = "vc_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "ofrlex_id", referencedColumnName = "id"))
	@JsonManagedReference
	private List<LexiconOfrlex4> targets = new ArrayList<LexiconOfrlex4>();
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public java.util.Date getStartDate() {
		return startDate;
	}
	public void setStartDate(java.util.Date startDate) {
		this.startDate = startDate;
	}
	public java.util.Date getEndDate() {
		return endDate;
	}
	public void setEndDate(java.util.Date endDate) {
		this.endDate = endDate;
	}
	public List<LexiconOfrlex4> getTargets() {
		return targets;
	}
	public void setTargets(List<LexiconOfrlex4> targets) {
		this.targets = targets;
	}
	
}
