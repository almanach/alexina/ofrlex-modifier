package fr.inria.profiterole.almanach.ofrlexmodifier.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconDect;

@Repository
public interface LexiconDectRepository extends CrudRepository<LexiconDect, Long>{
	public List<LexiconDect> findAll();
}
