//package fr.inria.profiterole.almanach.ofrlexmodifier.repository;
//
//
//import java.util.List;
//
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.PagingAndSortingRepository;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
//import fr.inria.profiterole.almanach.ofrlexmodifier.domain.VCEntry;
//
//@Repository
//public interface VCEntryRepository extends PagingAndSortingRepository<VCEntry, Long> {
//
//	
////	(trecord REGEXP '^ALA[0-9]')
//	// SELECT * FROM "PUBLIC"."OFRLEX" WHERE REGEXP_MATCHES(ofrlex_headword, '^deus')
//	// SELECT * FROM "PUBLIC"."OFRLEX" WHERE  REGEXP_MATCHES(ofrlex_headword, '^amb.+?') OR REGEXP_MATCHES(ofrlex_headword, '^deus.+?')
//
//	static final String QUERY_FILTER = "SELECT ofrlex FROM VCEntry ofrlex WHERE id LIKE CONCAT('%',:p,'%') OR "
//			+ "ofrlexHeadword LIKE CONCAT('%',:p,'%') OR "
//			+ "cat LIKE CONCAT('%',:p,'%') OR "
//			+ "tlhw LIKE CONCAT('%',:p,'%') OR "
//			+ "tllemma LIKE CONCAT('%',:p,'%')";
//	static final String QUERY_FILTER_ENDWITH = "SELECT ofrlex FROM VCEntry ofrlex WHERE id LIKE CONCAT('%',:p) OR "
//			+ "ofrlexHeadword LIKE CONCAT('%',:p) OR "
//			+ "cat LIKE CONCAT('%',:p) OR "
//			+ "tlhw LIKE CONCAT('%',:p) OR "
//			+ "tllemma LIKE CONCAT('%',:p)";
//	static final String QUERY_FILTER_STARTWITH = "SELECT ofrlex FROM VCEntry ofrlex WHERE id LIKE CONCAT(:p,'%') OR "
//			+ "ofrlexHeadword LIKE CONCAT(:p,'%') OR "
//			+ "cat LIKE CONCAT(:p,'%') OR "
//			+ "tlhw LIKE CONCAT(:p,'%') OR "
//			+ "tllemma LIKE CONCAT(:p,'%')";
//	static final String QUERY_FILTER_EXACT = "SELECT ofrlex FROM VCEntry ofrlex WHERE "
//			+ "ofrlexHeadword LIKE :p OR "
//			+ "cat LIKE :p OR "
//			+ "tlhw LIKE :p OR "
//			+ "tllemma LIKE :p";
////	static final String QUERY_FILTER_REGEX = "SELECT ofrlex FROM VCEntry ofrlex WHERE REGEXP_MATCHES(id, ':p') ";
//	static final String QUERY_FILTER_CAT = "SELECT ofrlex FROM VCEntry ofrlex WHERE cat LIKE CONCAT('%',:p,'%')";
//	static final String QUERY_FILTER_CAT_STARTWITH = "SELECT ofrlex FROM VCEntry ofrlex WHERE cat LIKE CONCAT(:p,'%')";
//	static final String QUERY_FILTER_CAT_ENDWITH = "SELECT ofrlex FROM VCEntry ofrlex WHERE cat LIKE CONCAT('%',:p)";
//	static final String QUERY_FILTER_CAT_EXACT = "SELECT ofrlex FROM VCEntry ofrlex WHERE cat LIKE :p";
//	
//	static final String QUERY_FILTER_HEADWORD = "SELECT ofrlex FROM VCEntry ofrlex WHERE ofrlexHeadword LIKE CONCAT('%',:p,'%')";
//	static final String QUERY_FILTER_HEADWORD_STARTWITH = "SELECT ofrlex FROM VCEntry ofrlex WHERE ofrlexHeadword LIKE CONCAT(:p,'%')";
//	static final String QUERY_FILTER_HEADWORD_ENDWITH = "SELECT ofrlex FROM VCEntry ofrlex WHERE ofrlexHeadword LIKE CONCAT('%',:p)";
//	static final String QUERY_FILTER_HEADWORD_EXACT = "SELECT ofrlex FROM VCEntry ofrlex WHERE ofrlexHeadword LIKE :p";
//	
//	static final String QUERY_FILTER_FLEXION = "SELECT ofrlex FROM VCEntry ofrlex WHERE inflclass LIKE CONCAT('%',:p,'%')";
//	static final String QUERY_FILTER_FLEXION_STARTWITH = "SELECT ofrlex FROM VCEntry ofrlex WHERE inflclass LIKE CONCAT(:p,'%')";
//	static final String QUERY_FILTER_FLEXION_ENDWITH = "SELECT ofrlex FROM VCEntry ofrlex WHERE inflclass LIKE CONCAT('%',:p)";
//	static final String QUERY_FILTER_FLEXION_EXACT = "SELECT ofrlex FROM VCEntry ofrlex WHERE inflclass LIKE :p";
//
//	static final String QUERY_FILTER_REDISTRIBUTIONS = "SELECT ofrlex FROM VCEntry ofrlex WHERE redistributions LIKE CONCAT('%',:p,'%')";
//	static final String QUERY_FILTER_REDISTRIBUTIONS_STARTWITH = "SELECT ofrlex FROM VCEntry ofrlex WHERE redistributions LIKE CONCAT(:p,'%')";
//	static final String QUERY_FILTER_REDISTRIBUTIONS_ENDWITH = "SELECT ofrlex FROM VCEntry ofrlex WHERE redistributions LIKE CONCAT('%',:p)";
//	static final String QUERY_FILTER_REDISTRIBUTIONS_EXACT = "SELECT ofrlex FROM VCEntry ofrlex WHERE redistributions LIKE :p";
//	
//	static final String QUERY_FILTER_SUBCAT = "SELECT ofrlex FROM VCEntry ofrlex WHERE subcat LIKE CONCAT('%',:p,'%')";
//	static final String QUERY_FILTER_SUBCAT_STARTWITH = "SELECT ofrlex FROM VCEntry ofrlex WHERE subcat LIKE CONCAT(:p,'%')";
//	static final String QUERY_FILTER_SUBCAT_ENDWITH = "SELECT ofrlex FROM VCEntry ofrlex WHERE subcat LIKE CONCAT('%',:p)";
//	static final String QUERY_FILTER_SUBCAT_EXACT = "SELECT ofrlex FROM VCEntry ofrlex WHERE subcat LIKE :p";
//	
//	static final String QUERY_FIND_BY_IDS = "SELECT ofrlex FROM VCEntry ofrlex WHERE str(id) IN :ids";
//	
//	static final String QUERY_FIND_VARIANTS = "SELECT ofrlex FROM VCEntry ofrlex WHERE tlvariant_of LIKE CONCAT('%',:p,'%') OR variant_of LIKE CONCAT('%',:p,'%')";
//
//	Page<VCEntry> findAll(Pageable pageable);
//	List<VCEntry> findAll();
//	List<VCEntry> findByOfrlexHeadword(String headword);
//	List<VCEntry> findByCat(String cat);
//	
//	@Query(QUERY_FIND_VARIANTS)
//	List<VCEntry> findByVariantOfOrTlvariantOf(@Param("p") String variantOf);
//	
//	@Query(QUERY_FIND_BY_IDS)
//	List<VCEntry> findMultipleById(List<String> ids); // query needed for bypassing strict type parameter inference
//	
//	@Query(QUERY_FILTER) 
//    Page<VCEntry> findByPattern(@Param("p") String pattern, Pageable pageable);
//	
//	@Query(QUERY_FILTER_STARTWITH)
//	Page<VCEntry> findByPatternStartWith(@Param("p") String pattern, Pageable pageable);
//	
//	@Query(QUERY_FILTER_ENDWITH)
//	Page<VCEntry> findByPatternEndWith(@Param("p") String pattern, Pageable pageable);
//	
//	@Query(QUERY_FILTER_EXACT)
//	Page<VCEntry> findByPatternExact(@Param("p") String pattern, Pageable pageable);
//	
//	@Query(QUERY_FILTER_CAT) 
//    Page<VCEntry> findCatByPattern(@Param("p") String pattern, Pageable pageable);
//	@Query(QUERY_FILTER_CAT_STARTWITH) 
//    Page<VCEntry> findCatByPatternStartWith(@Param("p") String pattern, Pageable pageable);
//	@Query(QUERY_FILTER_CAT_ENDWITH) 
//    Page<VCEntry> findCatByPatternEndWith(@Param("p") String pattern, Pageable pageable);
//	@Query(QUERY_FILTER_CAT_EXACT) 
//    Page<VCEntry> findCatByPatternExact(@Param("p") String pattern, Pageable pageable);
//	
//	@Query(QUERY_FILTER_HEADWORD) 
//    Page<VCEntry> findHeadwordByPattern(@Param("p") String pattern, Pageable pageable);
//	@Query(QUERY_FILTER_HEADWORD_ENDWITH) 
//    Page<VCEntry> findHeadwordByPatternEndWith(@Param("p") String pattern, Pageable pageable);
//	@Query(QUERY_FILTER_HEADWORD_STARTWITH) 
//    Page<VCEntry> findHeadwordByPatternStartWith(@Param("p") String pattern, Pageable pageable);
//	@Query(QUERY_FILTER_HEADWORD_EXACT) 
//    Page<VCEntry> findHeadwordByPatternExact(@Param("p") String pattern, Pageable pageable);
//	
//	@Query(QUERY_FILTER_FLEXION) 
//    Page<VCEntry> findFlexionByPattern(@Param("p") String pattern, Pageable pageable);
//	@Query(QUERY_FILTER_FLEXION_STARTWITH) 
//    Page<VCEntry> findFlexionByPatternStartWith(@Param("p") String pattern, Pageable pageable);
//	@Query(QUERY_FILTER_FLEXION_ENDWITH) 
//    Page<VCEntry> findFlexionByPatternEndWith(@Param("p") String pattern, Pageable pageable);
//	@Query(QUERY_FILTER_FLEXION_EXACT) 
//    Page<VCEntry> findFlexionByPatternExact(@Param("p") String pattern, Pageable pageable);
//	
//	@Query(QUERY_FILTER_REDISTRIBUTIONS) 
//    Page<VCEntry> findRedistributionsByPattern(@Param("p") String pattern, Pageable pageable);
//	@Query(QUERY_FILTER_REDISTRIBUTIONS_STARTWITH) 
//    Page<VCEntry> findRedistributionsByPatternStartWith(@Param("p") String pattern, Pageable pageable);
//	@Query(QUERY_FILTER_REDISTRIBUTIONS_ENDWITH) 
//    Page<VCEntry> findRedistributionsByPatternEndWith(@Param("p") String pattern, Pageable pageable);
//	@Query(QUERY_FILTER_REDISTRIBUTIONS_EXACT) 
//    Page<VCEntry> findRedistributionsByPatternExact(@Param("p") String pattern, Pageable pageable);
//	
//	@Query(QUERY_FILTER_SUBCAT) 
//    Page<VCEntry> findSubCatByPattern(@Param("p") String pattern, Pageable pageable);
//	@Query(QUERY_FILTER_SUBCAT_STARTWITH) 
//    Page<VCEntry> findSubCatByPatternStartWith(@Param("p") String pattern, Pageable pageable);
//	@Query(QUERY_FILTER_SUBCAT_ENDWITH) 
//    Page<VCEntry> findSubCatByPatternEndWith(@Param("p") String pattern, Pageable pageable);
//	@Query(QUERY_FILTER_SUBCAT_EXACT) 
//    Page<VCEntry> findSubCatByPatternExact(@Param("p") String pattern, Pageable pageable);
//
//	
//
//}
