//package fr.inria.profiterole.almanach.ofrlexmodifier.domain;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.Id;
//import javax.persistence.Lob;
//import javax.persistence.ManyToOne;
//import javax.persistence.OneToMany;
//import javax.persistence.Table;
//
//import org.hibernate.annotations.Cascade;
//import org.hibernate.annotations.CascadeType;
//import org.hibernate.annotations.GenericGenerator;
//import org.hibernate.envers.Audited;
//
//import com.fasterxml.jackson.annotation.JsonBackReference;
//import com.fasterxml.jackson.annotation.JsonManagedReference;
//import com.google.common.base.Joiner;
//
//
//@Entity
//@Audited
//@Table(name="ofrlex")
//public class VCEntry {
//	
//	@Id
//	@GenericGenerator(name = "UseIdOrGenerate", strategy = "fr.inria.profiterole.almanach.ofrlexmodifier.repository.UseIdOrGenerate")
//	@GeneratedValue(generator = "UseIdOrGenerate")
//	@Column(unique = true, nullable = false)
//	private Long id;
//	private String ofrlexHeadword="";
//	private String scat="";
//	private String tlhw="";
//	private String tllemma="";
//	private String tlhwms="";
//	private String tlref="";
//	private String tlhwscat="";
//	private String inflclass="";
//	private String headwordscat="";
//	private String cat="";
//	private String subCat="";
//	private String syntInfo="";
//	private String godefroyBS="";
//	private String godefroyCatBScatkey="";
//	private String godefroyEntry="";
//	private String godefroyPseudoCAT="";
//	@Lob
//	@Column(length = 2000)
//	private String godefroyDef="";
//	private String godefroyRef="";
//	private String dectGD="";
//	private String dectGDpage="";
//	private String dectTL="";
//	private String dectTLhw="";
//	private String dectEntry="";
//	private String dectPage="";
//	private String dectCatMs="";
//	private String dectScat="";
//	private String dectLemma="";
//	private String dectFreq="";
//	private String dectFB="";
//	private String dectEtymonFEW="";
//	private String dectRefFEW="";
//	private String dectTLF="";
//	private String dectAND="";
//	private String dectDMF="";
//	@Lob
//	@Column(length = 2000)
//	private String dectDef="";
//	private String bfmfreq="";
//	private String glossFr="";
//	private String descendent="";
//	private String tlvariantOf="";
//	private String variantOf="";
//	private String tldescendent="";
//	private String pseudoGlosses="";
//	private String redistributions="";
//	private String manualCat="";
//	private String manualSubCat="";
//	private String manualSyntfeats="";
//	private String manualRedistributions="";
//	private String manualVariantOf ="";
//	private String manualGlossFr ="";
//	private String manualPseudoGlosses = "";
//	private String manualInflclass = "";
//	private String manualDescendent = "";
//	@OneToMany( fetch = FetchType.EAGER, mappedBy = "lexeme")
////	@Cascade({CascadeType.SAVE_UPDATE, CascadeType.REFRESH,CascadeType.REMOVE})
//	@Cascade({CascadeType.ALL})
//	@JsonManagedReference
//	private List<Pseudosyn> pseudosyns = new ArrayList<Pseudosyn>();
//	private String editor = "";
//	private String modifiedField = "";
//	@ManyToOne(fetch = FetchType.EAGER)
//	@Cascade({CascadeType.SAVE_UPDATE, CascadeType.REFRESH, CascadeType.PERSIST})
////	@Cascade({CascadeType.PERSIST})
//	@JsonBackReference
//	private ValidationCampaign validationCampaign;
//	private boolean validated = false;
//	
//	
//	
//	public VCEntry() {}
//
//	
//	
//	public VCEntry(Long id, String ofrlexHeadword, String scat, String tlhw, String tllemma, String tlhwms,
//			String tlref, String tlhwscat, String inflclass, String headwordscat, String cat, String subCat,
//			String syntInfo, String godefroyBS, String godefroyCatBScatkey, String godefroyEntry,
//			String godefroyPseudoCAT, String godefroyDef, String godefroyRef, String dectGD, String dectGDpage,
//			String dectTL, String dectTLhw, String dectEntry, String dectPage, String dectCatMs, String dectScat,
//			String dectLemma, String dectFreq, String dectFB, String dectEtymonFEW, String dectRefFEW, String dectTLF,
//			String dectAND, String dectDMF, String dectDef, String bfmfreq, String glossFr, String descendent,
//			String tlvariantOf, String variantOf, String tldescendent, String pseudoGlosses, String redistributions,
//			String manualCat, String manualSubCat, String manualSyntfeats, String manualRedistributions,
//			String manualVariantOf, String manualGlossFr, String manualPseudoGlosses, String manualInflclass,
//			String manualDescendent, List<Pseudosyn> pseudosyns, String editor, String modifiedField,
//			ValidationCampaign validationCampaign, boolean validated) {
//		super();
//		this.id = id;
//		this.ofrlexHeadword = ofrlexHeadword;
//		this.scat = scat;
//		this.tlhw = tlhw;
//		this.tllemma = tllemma;
//		this.tlhwms = tlhwms;
//		this.tlref = tlref;
//		this.tlhwscat = tlhwscat;
//		this.inflclass = inflclass;
//		this.headwordscat = headwordscat;
//		this.cat = cat;
//		this.subCat = subCat;
//		this.syntInfo = syntInfo;
//		this.godefroyBS = godefroyBS;
//		this.godefroyCatBScatkey = godefroyCatBScatkey;
//		this.godefroyEntry = godefroyEntry;
//		this.godefroyPseudoCAT = godefroyPseudoCAT;
//		this.godefroyDef = godefroyDef;
//		this.godefroyRef = godefroyRef;
//		this.dectGD = dectGD;
//		this.dectGDpage = dectGDpage;
//		this.dectTL = dectTL;
//		this.dectTLhw = dectTLhw;
//		this.dectEntry = dectEntry;
//		this.dectPage = dectPage;
//		this.dectCatMs = dectCatMs;
//		this.dectScat = dectScat;
//		this.dectLemma = dectLemma;
//		this.dectFreq = dectFreq;
//		this.dectFB = dectFB;
//		this.dectEtymonFEW = dectEtymonFEW;
//		this.dectRefFEW = dectRefFEW;
//		this.dectTLF = dectTLF;
//		this.dectAND = dectAND;
//		this.dectDMF = dectDMF;
//		this.dectDef = dectDef;
//		this.bfmfreq = bfmfreq;
//		this.glossFr = glossFr;
//		this.descendent = descendent;
//		this.tlvariantOf = tlvariantOf;
//		this.variantOf = variantOf;
//		this.tldescendent = tldescendent;
//		this.pseudoGlosses = pseudoGlosses;
//		this.redistributions = redistributions;
//		this.manualCat = manualCat;
//		this.manualSubCat = manualSubCat;
//		this.manualSyntfeats = manualSyntfeats;
//		this.manualRedistributions = manualRedistributions;
//		this.manualVariantOf = manualVariantOf;
//		this.manualGlossFr = manualGlossFr;
//		this.manualPseudoGlosses = manualPseudoGlosses;
//		this.manualInflclass = manualInflclass;
//		this.manualDescendent = manualDescendent;
//		this.pseudosyns = pseudosyns;
//		this.editor = editor;
//		this.modifiedField = modifiedField;
//		this.validationCampaign = validationCampaign;
//		this.validated = validated;
//	}
//
//
//
//	public Long getId() {
//		return id;
//	}
//	
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public String getOfrlexHeadword() {
//		return ofrlexHeadword;
//	}
//
//	public void setOfrlexHeadword(String ofrlexHeadword) {
//		this.ofrlexHeadword = ofrlexHeadword;
//	}
//	
//	public String getScat() {
//		return scat;
//	}
//
//	public void setScat(String scat) {
//		this.scat = scat;
//	}
//	
//	public String getTlhw() {
//		return tlhw;
//	}
//
//	public void setTlhw(String tlhw) {
//		this.tlhw = tlhw;
//	}
//
//	public String getTllemma() {
//		return tllemma;
//	}
//
//	public void setTllemma(String tllemma) {
//		this.tllemma = tllemma;
//	}
//
//	public String getTlhwms() {
//		return tlhwms;
//	}
//
//	public void setTlhwms(String tlhwms) {
//		this.tlhwms = tlhwms;
//	}
//
//	public String getTlref() {
//		return tlref;
//	}
//
//	public void setTlref(String tlref) {
//		this.tlref = tlref;
//	}
//
//	public String getTlhwscat() {
//		return tlhwscat;
//	}
//
//	public void setTlhwscat(String tlhwscat) {
//		this.tlhwscat = tlhwscat;
//	}
//
//	public String getInflclass() {
//		return inflclass;
//	}
//
//	public void setInflclass(String inflclass) {
//		this.inflclass = inflclass;
//	}
//
//	public String getHeadwordscat() {
//		return headwordscat;
//	}
//
//	public void setHeadwordscat(String headwordscat) {
//		this.headwordscat = headwordscat;
//	}
//
//	public String getCat() {
//		return cat;
//	}
//
//	public void setCat(String cat) {
//		this.cat = cat;
//	}
//
//	public String getSubCat() {
//		return subCat;
//	}
//
//	public void setSubCat(String subCat) {
//		this.subCat = subCat;
//	}
//
//	public String getSyntInfo() {
//		return syntInfo;
//	}
//
//	public void setSyntInfo(String syntInfo) {
//		this.syntInfo = syntInfo;
//	}
//
//	public String getGodefroyBS() {
//		return godefroyBS;
//	}
//
//	public void setGodefroyBS(String godefroyBS) {
//		this.godefroyBS = godefroyBS;
//	}
//
//	public String getGodefroyCatBScatkey() {
//		return godefroyCatBScatkey;
//	}
//
//
//	public void setGodefroyCatBScatkey(String godefroyCatBScatkey) {
//		this.godefroyCatBScatkey = godefroyCatBScatkey;
//	}
//
//
//	public String getGodefroyEntry() {
//		return godefroyEntry;
//	}
//
//
//
//
//	public void setGodefroyEntry(String godefroyEntry) {
//		this.godefroyEntry = godefroyEntry;
//	}
//
//
//
//
//	public String getGodefroyPseudoCAT() {
//		return godefroyPseudoCAT;
//	}
//
//	public void setGodefroyPseudoCAT(String godefroyPseudoCAT) {
//		this.godefroyPseudoCAT = godefroyPseudoCAT;
//	}
//
//	public String getGodefroyDef() {
//		return godefroyDef;
//	}
//
//	public void setGodefroyDef(String godefroyDef) {
//		this.godefroyDef = godefroyDef;
//	}
//
//	public String getGodefroyRef() {
//		return godefroyRef;
//	}
//
//	public void setGodefroyRef(String godefroyRef) {
//		this.godefroyRef = godefroyRef;
//	}
//
//	public String getDectGD() {
//		return dectGD;
//	}
//
//	public void setDectGD(String dectGD) {
//		this.dectGD = dectGD;
//	}
//
//	public String getDectGDpage() {
//		return dectGDpage;
//	}
//
//	public void setDectGDpage(String dectGDpage) {
//		this.dectGDpage = dectGDpage;
//	}
//
//	public String getDectTL() {
//		return dectTL;
//	}
//
//	public void setDectTL(String dectTL) {
//		this.dectTL = dectTL;
//	}
//
//	public String getDectTLhw() {
//		return dectTLhw;
//	}
//
//	public void setDectTLhw(String dectTLhw) {
//		this.dectTLhw = dectTLhw;
//	}
//
//	public String getDectEntry() {
//		return dectEntry;
//	}
//
//	public void setDectEntry(String dectEntry) {
//		this.dectEntry = dectEntry;
//	}
//
//	public String getDectPage() {
//		return dectPage;
//	}
//
//	public void setDectPage(String dectPage) {
//		this.dectPage = dectPage;
//	}
//
//	public String getDectCatMs() {
//		return dectCatMs;
//	}
//
//	public void setDectCatMs(String dectCatMs) {
//		this.dectCatMs = dectCatMs;
//	}
//
//	public String getDectScat() {
//		return dectScat;
//	}
//
//	public void setDectScat(String dectScat) {
//		this.dectScat = dectScat;
//	}
//
//	public String getDectLemma() {
//		return dectLemma;
//	}
//
//	public void setDectLemma(String dectLemma) {
//		this.dectLemma = dectLemma;
//	}
//
//	public String getDectFreq() {
//		return dectFreq;
//	}
//
//	public void setDectFreq(String dectFreq) {
//		this.dectFreq = dectFreq;
//	}
//
//	public String getDectFB() {
//		return dectFB;
//	}
//
//	public void setDectFB(String dectFB) {
//		this.dectFB = dectFB;
//	}
//
//	public String getDectEtymonFEW() {
//		return dectEtymonFEW;
//	}
//
//	public void setDectEtymonFEW(String dectEtymonFEW) {
//		this.dectEtymonFEW = dectEtymonFEW;
//	}
//
//	public String getDectRefFEW() {
//		return dectRefFEW;
//	}
//
//	public void setDectRefFEW(String dectRefFEW) {
//		this.dectRefFEW = dectRefFEW;
//	}
//
//	public String getDectTLF() {
//		return dectTLF;
//	}
//
//	public void setDectTLF(String dectTLF) {
//		this.dectTLF = dectTLF;
//	}
//
//	public String getDectAND() {
//		return dectAND;
//	}
//
//	public void setDectAND(String dectAND) {
//		this.dectAND = dectAND;
//	}
//
//	public String getDectDMF() {
//		return dectDMF;
//	}
//
//	public void setDectDMF(String dectDMF) {
//		this.dectDMF = dectDMF;
//	}
//
//	public String getDectDef() {
//		return dectDef;
//	}
//
//	public void setDectDef(String dectDef) {
//		this.dectDef = dectDef;
//	}
//
//	public String getBfmfreq() {
//		return bfmfreq;
//	}
//
//	public void setBfmfreq(String bfmfreq) {
//		this.bfmfreq = bfmfreq;
//	}
//
//	public String getGlossFr() {
//		return glossFr;
//	}
//
//	public void setGlossFr(String glossFr) {
//		this.glossFr = glossFr;
//	}
//
//	public String getDescendent() {
//		return descendent;
//	}
//
//	public void setDescendent(String descendent) {
//		this.descendent = descendent;
//	}
//
//	public String getTlvariantOf() {
//		return tlvariantOf;
//	}
//
//	public void setTlvariantOf(String tlvariantOf) {
//		this.tlvariantOf = tlvariantOf;
//	}
//
//	public String getVariantOf() {
//		return variantOf;
//	}
//
//	public void setVariantOf(String variantOf) {
//		this.variantOf = variantOf;
//	}
//
//	public String getTldescendent() {
//		return tldescendent;
//	}
//
//	public void setTldescendent(String tldescendent) {
//		this.tldescendent = tldescendent;
//	}
//
//	public String getPseudoGlosses() {
//		return pseudoGlosses;
//	}
//
//	public void setPseudoGlosses(String pseudoGlosses) {
//		this.pseudoGlosses = pseudoGlosses;
//	}
//
//	public String getRedistributions() {
//		return redistributions;
//	}
//
//	public void setRedistributions(String redistributions) {
//		this.redistributions = redistributions;
//	}
//
//	public String getManualSyntfeats() {
//		return manualSyntfeats;
//	}
//
//	public void setManualSyntfeats(String manualSyntfeats) {
//		this.manualSyntfeats = manualSyntfeats;
//	}
//
//	public String getManualRedistributions() {
//		return manualRedistributions;
//	}
//
//	public void setManualRedistributions(String manualRedistributions) {
//		this.manualRedistributions = manualRedistributions;
//	}
//
//	
//	
//	public String getManualCat() {
//		return manualCat;
//	}
//
//	public void setManualCat(String manualCat) {
//		this.manualCat = manualCat;
//	}
//
//	
//	public String getManualSubCat() {
//		return manualSubCat;
//	}
//
//	public void setManualSubCat(String manualSubCat) {
//		this.manualSubCat = manualSubCat;
//	}
//
//	public String getManualVariantOf() {
//		return manualVariantOf;
//	}
//
//	public void setManualVariantOf(String manualVariantOf) {
//		this.manualVariantOf = manualVariantOf;
//	}
//
//	public String getManualGlossFr() {
//		return manualGlossFr;
//	}
//
//	public void setManualGlossFr(String manualGlossFr) {
//		this.manualGlossFr = manualGlossFr;
//	}
//
//	public String getManualPseudoGlosses() {
//		return manualPseudoGlosses;
//	}
//
//	public void setManualPseudoGlosses(String manualPseudoGlosses) {
//		this.manualPseudoGlosses = manualPseudoGlosses;
//	}
//
//	public String getManualInflclass() {
//		return manualInflclass;
//	}
//
//	public void setManualInflclass(String manualInflclass) {
//		this.manualInflclass = manualInflclass;
//	}
//
//	public String getManualDescendent() {
//		return manualDescendent;
//	}
//
//	public void setManualDescendent(String manualDescendent) {
//		this.manualDescendent = manualDescendent;
//	}
//
//
//	public List<Pseudosyn> getPseudosyns() {
//		return pseudosyns;
//	}
//
//	public void setPseudosyns(List<Pseudosyn> pseudosyns) {
//		this.pseudosyns = pseudosyns;
//	}
//	
//	public String getEditor() {
//		return editor;
//	}
//
//	public void setEditor(String editor) {
//		this.editor = editor;
//	}
//
//	public String getModifiedField() {
//		return modifiedField;
//	}
//
//	public void setModifiedField(String modifiedField) {
//		this.modifiedField = modifiedField;
//	}
//	
//	
//
//	
//	public ValidationCampaign getValidationCampaign() {
//		return validationCampaign;
//	}
//
//
//
//
//	public void setValidationCampaign(ValidationCampaign validationCampaign) {
//		this.validationCampaign = validationCampaign;
//	}
//
//
//
//
//	public boolean isValidated() {
//		return validated;
//	}
//
//
//
//
//	public void setValidated(boolean validated) {
//		this.validated = validated;
//	}
//
//
//
//
//
//	@Override
//	public String toString() {
//		return "LexiconOfrlex4 [id=" + id + ", ofrlexHeadword=" + ofrlexHeadword + ", scat=" + scat + ", tlhw=" + tlhw
//				+ ", tllemma=" + tllemma + ", tlhwms=" + tlhwms + ", tlref=" + tlref + ", tlhwscat=" + tlhwscat
//				+ ", inflclass=" + inflclass + ", headwordscat=" + headwordscat + ", cat=" + cat + ", subCat=" + subCat
//				+ ", syntInfo=" + syntInfo + ", godefroyBS=" + godefroyBS + ", godefroyCatBScatkey="
//				+ godefroyCatBScatkey + ", godefroyEntry=" + godefroyEntry + ", godefroyPseudoCAT=" + godefroyPseudoCAT
//				+ ", godefroyDef=" + godefroyDef + ", godefroyRef=" + godefroyRef + ", dectGD=" + dectGD
//				+ ", dectGDpage=" + dectGDpage + ", dectTL=" + dectTL + ", dectTLhw=" + dectTLhw + ", dectEntry="
//				+ dectEntry + ", dectPage=" + dectPage + ", dectCatMs=" + dectCatMs + ", dectScat=" + dectScat
//				+ ", dectLemma=" + dectLemma + ", dectFreq=" + dectFreq + ", dectFB=" + dectFB + ", dectEtymonFEW="
//				+ dectEtymonFEW + ", dectRefFEW=" + dectRefFEW + ", dectTLF=" + dectTLF + ", dectAND=" + dectAND
//				+ ", dectDMF=" + dectDMF + ", dectDef=" + dectDef + ", bfmfreq=" + bfmfreq + ", glossFr=" + glossFr
//				+ ", descendent=" + descendent + ", tlvariantOf=" + tlvariantOf + ", variantOf=" + variantOf
//				+ ", tldescendent=" + tldescendent + ", pseudoGlosses=" + pseudoGlosses + ", redistributions="
//				+ redistributions + ", manualCat=" + manualCat + ", manualSubCat=" + manualSubCat + ", manualSyntfeats="
//				+ manualSyntfeats + ", manualRedistributions=" + manualRedistributions + ", manualVariantOf="
//				+ manualVariantOf + ", manualGlossFr=" + manualGlossFr + ", manualPseudoGlosses=" + manualPseudoGlosses
//				+ ", manualInflclass=" + manualInflclass + ", manualDescendent=" + manualDescendent + ", pseudosyns="
//				+ pseudosyns + ", editor=" + editor + ", modifiedField=" + modifiedField + ", validationCampaign="
//				+ validationCampaign + ", validated=" + String.valueOf(validated) + "]";
//	}
//
//
//
//
//	public String toTsv() {
//		List<String> values = Arrays.asList( String.valueOf(id), ofrlexHeadword, scat, tlhw, tllemma, tlhwms, tlref, tlhwscat, inflclass, headwordscat, cat, subCat, syntInfo,
//				godefroyBS, godefroyCatBScatkey, godefroyPseudoCAT, godefroyDef, godefroyRef, dectGD, dectGDpage, dectTL, dectTLhw, dectEntry, dectPage, dectCatMs, dectScat, dectLemma, dectFreq, dectFB, dectEtymonFEW, dectRefFEW, dectTLF,
//				dectAND, dectDMF, dectDef,
//				bfmfreq, glossFr, descendent,
//				tlvariantOf, variantOf, tldescendent, pseudoGlosses, redistributions, manualCat, manualSubCat, manualSyntfeats, manualRedistributions, manualVariantOf, manualGlossFr, manualPseudoGlosses, manualInflclass,
//				manualDescendent );		
//		for(int i = 0; i < values.size(); i++) if(values.get(i) == null) values.set(i, "");
//		return Joiner.on("\t").join(values);
//	}
//	
//
//	
//}
