package fr.inria.profiterole.almanach.ofrlexmodifier.utils;

import java.beans.IntrospectionException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Stopwatch;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconOfrlex4;

public class ExcelGenerator {

	private static final Logger LOG = LoggerFactory.getLogger(ExcelGenerator.class);

	/**
	 * Method to get excel inputstream. Useful for centralized process during export (all type of export except for ilex)
	 * @param entries
	 * @return
	 * @throws IOException
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws IntrospectionException
	 */
	public static ByteArrayInputStream ofrlex2Excel(List<LexiconOfrlex4> entries)
			throws IOException, NoSuchFieldException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, IntrospectionException {

		Stopwatch stopwatch = Stopwatch.createStarted();
		LOG.debug("ofrlex2Excel started");

		Field[] fields = LexiconOfrlex4.class.getDeclaredFields();
		List<String> columns = new ArrayList<String>();
		for (Field field : fields)
			columns.add(field.getName());

		LOG.info("fields generated, starting workbook creation : {}", stopwatch);

		try (SXSSFWorkbook workbook = new SXSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) { // streaming
																														// version
			workbook.setCompressTempFiles(true); // specify the temporary files compression

//			CreationHelper createHelper = workbook.getCreationHelper();

			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("ofrlex"); // streaming version faster and controlled
																			// memory
			sheet.setRandomAccessWindowSize(100); // batch size of elements to continuously flush

			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLUE.getIndex());

			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);

			// Row for Header
			Row headerRow = sheet.createRow(0);

			LOG.debug("generating headers : {}", stopwatch);
			// Header
			for (int col = 0; col < columns.size(); col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(columns.get(col));
				cell.setCellStyle(headerCellStyle);
			}
			LOG.debug("Setting entries : {}", stopwatch);
			int rowIdx = 1;
			for (LexiconOfrlex4 entry : entries) {
				SXSSFRow row = sheet.createRow(rowIdx++); // SXSSFRow for streaming row
				for (int col = 0; col < columns.size(); col++) {
					if (col > 0) {
						if (columns.get(col).equals("manualSyntfeats") ) row.createCell(col).setCellValue( formatAndSelectValue(entry.getSyntInfo(), entry.getManualSyntfeats()) );
						else if (columns.get(col).equals("manualCat") ) row.createCell(col).setCellValue( formatAndSelectValue(entry.getCat(), entry.getManualCat()) );
						else if (columns.get(col).equals("manualSubCat") ) row.createCell(col).setCellValue( formatAndSelectValue(entry.getSubCat(), entry.getManualSubCat()) );
						else if (columns.get(col).equals("manualRedistributions") ) row.createCell(col).setCellValue( formatAndSelectValue(entry.getRedistributions(), entry.getManualRedistributions()) );
						else if (columns.get(col).equals("manualGlossFr") ) row.createCell(col).setCellValue( formatAndSelectValue(entry.getGlossFr(), entry.getManualGlossFr()) );
						else if (columns.get(col).equals("manualPseudoGlosses") ) row.createCell(col).setCellValue( formatAndSelectValue(entry.getPseudoGlosses(), entry.getManualPseudoGlosses()) );
						else if (columns.get(col).equals("manualInflclass") ) row.createCell(col).setCellValue( formatAndSelectValue(entry.getInflclass(), entry.getManualInflclass()) );
						else if (columns.get(col).equals("manualDescendent") ) row.createCell(col).setCellValue( formatAndSelectValue(entry.getDescendent(), entry.getManualDescendent()) );
						else if (columns.get(col).equals("manualVariantOf") ) row.createCell(col).setCellValue( formatAndSelectValue(entry.getVariantOf(), entry.getTlvariantOf(), entry.getManualVariantOf()) ); // three values
						else row.createCell(col).setCellValue( String.valueOf( Tools.callGetter(entry, columns.get(col)) )  );
					}else
						row.createCell(col).setCellValue((Long) Tools.callGetter(entry, columns.get(col)));
				}
			}

			LOG.debug("Entries done : {}", stopwatch);
			workbook.write(out);
			LOG.debug("ofrlex2Excel time = {}", stopwatch);
			stopwatch.stop();
			return new ByteArrayInputStream(out.toByteArray());
		}
	}

	public static ByteArrayInputStream ofrlex2Excel4Ilex(List<LexiconOfrlex4> entries)
			throws IOException, NoSuchFieldException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, IntrospectionException {

		Stopwatch stopwatch = Stopwatch.createStarted();
		LOG.debug("ofrlex2Excel started");

		Field[] fields = LexiconOfrlex4.class.getDeclaredFields();
		List<String> columns = new ArrayList<String>();
		for (Field field : fields)
			columns.add(field.getName());

		LOG.info("fields generated, starting workbook creation 4 ilex : {}", stopwatch);

		try (SXSSFWorkbook workbook = new SXSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) { // streaming
																														// version
			workbook.setCompressTempFiles(true); // specify the temporary files compression

//			CreationHelper createHelper = workbook.getCreationHelper();

			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("ofrlex"); // streaming version faster and controlled
																			// memory
			sheet.setRandomAccessWindowSize(100); // batch size of elements to continuously flush

			// Row for Header
			Row headerRow = sheet.createRow(0);
			
			Map<String, String> mapCols = Stream.of(new String[][] {
				  { "id", "__id__" }, 
				  { "ofrlexHeadword", "OFrLex headword" }, 
				  { "scat", "scat"},
				  { "tlhw", "TLhw"},
				  { "tllemma", "TLlemma"},
				  { "tlhwms", "TLhw ms"},
				  { "tlref", "TL ref"},
				  { "tlhwscat", "TLhw_scat"},
				  { "inflclass", "inflclass"},
				  { "headwordscat", "headword__scat"},
				  { "cat", "CAT"},
				  { "subCat", "subCat"}, //
				  { "bfmfreq", "BFM freq"},
				  { "glossFr", "gloss fr"},
				  { "descendent", "descendent"},
				  { "tlvariantOf", "variant of by TL"},
				  { "variantOf", "variant of"},
				  { "tldescendent", "descendent by TL"},
				  { "pseudoGlosses", "pseudo-glosses"},
				  { "redistributions", "Manual redistributions"}, //
				  { "manualSubCat", "Manual subcat"},
				  { "manualSyntfeats", "Manual syntfeats"},
				  { "manualRedistributions", "Manual redistributions"},
				  { "syntInfo", "synt info"},
				  { "manualVariantOf", "manualVariantOf"},
				  { "manualGlossFr", "manualGlossFr" },
				  { "manualPseudoGlosses", "manualPseudoGlosses"},
				  { "manualInflclass", "manualInflclass"},
				  { "manualDescendent", "manualDescendent"},
				  { "manualCat", "Manual cat"},
				  // syntInfos
				}).collect(Collectors.toMap(data -> data[0], data -> data[1]));
			

			LOG.debug("generating headers : {}", stopwatch);
			// Header
			for (int col = 0; col < columns.size(); col++) {
				Cell cell = headerRow.createCell(col);
//				cell.setCellValue(columns.get(col));
				cell.setCellValue(  mapCols.get( columns.get(col)  )   );
			}
//			Cell cell = headerRow.createCell(columns.size() );
//			cell.setCellValue( "synt info"  );
			
			
			LOG.debug("Setting entries : {}", stopwatch);
			int rowIdx = 1;
			for (LexiconOfrlex4 entry : entries) {
				SXSSFRow row = sheet.createRow(rowIdx++); // SXSSFRow for streaming row
				for (int col = 0; col < columns.size(); col++) {
					if (col > 0) {
						if (columns.get(col).equals("syntInfo") ) {
							String syntInfosRedone = recreateSyntInfos(entry.getSubCat(), entry.getSyntInfo(), entry.getRedistributions(), 
									entry.getManualSubCat(), entry.getManualSyntfeats(), entry.getManualRedistributions());
							row.createCell(col).setCellValue( syntInfosRedone );
						}else if(columns.get(col).equals("inflclass")) {
							row.createCell(col).setCellValue(  chooseValue(entry.getInflclass(), entry.getManualInflclass())  );
						}else if(columns.get(col).equals("cat")) {
							row.createCell(col).setCellValue(  chooseValue(entry.getCat(), entry.getManualCat()) )  ;
						}else
							row.createCell(col).setCellValue((String) Tools.callGetter(entry, columns.get(col) ));
					}else
						row.createCell(col).setCellValue((Long) Tools.callGetter(entry, columns.get(col)  ));
				}
			}

			LOG.debug("Entries done : {}", stopwatch);
			workbook.write(out);
			LOG.debug("ofrlex2Excel time = {}", stopwatch);
			stopwatch.stop();
			return new ByteArrayInputStream(out.toByteArray());
		}
		
	}
	
	private static String recreateSyntInfos(String subCat, String synt, String redist, String manSubCat, String manSynt, String manRedist) {
		//<Suj:cln|sn>;cat=VER;%actif
		String scat = ""; String syntfeats = ""; String redistributions = "";
		subCat = emptyIfNull(subCat); synt = emptyIfNull(synt); redist = emptyIfNull(redist); manSubCat = emptyIfNull(manSubCat); manSynt = emptyIfNull(manSynt); manRedist = emptyIfNull(manRedist);
		if(subCat.equals(manSubCat)) scat = subCat; else scat = manSubCat;
		if(synt.equals(manSynt)) syntfeats = synt; else syntfeats = manSynt;
		if(redist.equals(manRedist)) redistributions = redist; else redistributions = manRedist;
		return String.format("<%s>;%s;%%%s", scat, syntfeats, redistributions);
	}
	
	private static String chooseValue(String auto, String man) {
		String val = "";
		if(auto.equals(man)) val = auto; else val = man;
		return val;
	}
	
	private static String emptyIfNull(String value) {
		if(value == null) return "";  else return value;
	}
	
	/**
	 * Format the manual value given a comparison with the auto value and the check for ! char which indicates a manual validation of the auto value.
	 * @param auto
	 * @param manual
	 * @return a String never null
	 */
	private static String formatAndSelectValue(String auto, String manual) {
		auto = emptyIfNull(auto); manual = emptyIfNull(manual);
		boolean verified = manual.startsWith("!");
		manual = manual.replaceFirst("!", "");
		if(auto.equals(manual)){
			if(verified) return manual;
			else return "";
		} else return manual;
	}
	
	/**
	 * Format the manual value given a comparison with the auto value and the check for ! char which indicates a manual validation of the 2 auto values (needed for variantOf).
	 * Prioritize auto over auto2 if equal
	 * @param auto
	 * @param auto2
	 * @param manual
	 * @return a String never null
	 */
	private static String formatAndSelectValue(String auto, String auto2, String manual) {
		auto = emptyIfNull(auto); auto2 = emptyIfNull(auto2); manual = emptyIfNull(manual);
		boolean verified = manual.startsWith("!");
		manual = manual.replaceFirst("!", "");
		if(auto.equals(manual) || auto2.equals(manual)) {
			if(verified)return manual;
			else return "";
		} else return manual;
	}
}