package fr.inria.profiterole.almanach.ofrlexmodifier.domain.dto;

import java.util.ArrayList;
import java.util.List;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.DBState;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.ValidationCampaign;

public class OfrlexPayload {

    private String name;
    private String sender;
    private LexiconOfrlexDto entry = new LexiconOfrlexDto();
    private List<LexiconOfrlexDto> entries = new ArrayList<LexiconOfrlexDto>();
	private Long totalEntries;
    private DBState dbs;
    private ValidationCampaign vc;
    private Type type;
    private Pagination pagination;
    public enum Type {
    	PAGE,
        LOCK,
        UPDATE_SUCCESS,
        UPDATE_LOCKED,
        UPDATE_DENIED,
        DELETE_SUCCESS,
        DELETE_LOCKED,
        ADD_SUCCESS,
        ADD_LOCKED,
        NOTIFY_OVERRIDED,
        CAMPAIGN_STARTED,
        CAMPAIGN_VIEW,
        CAMPAIGN_ENDED
    }

    public OfrlexPayload() {
    }

    public OfrlexPayload(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }  
    
    public String getSender() {
    	return sender;
    }
    
    public void setSender(String sender) {
    	this.sender = sender;
    }
    
    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
    
    public List<LexiconOfrlexDto> getEntries() {
		return entries;
	}
	public void setEntries(List<LexiconOfrlexDto> entries) {
		this.entries = entries;
	}
	public Long getTotalEntries() {
		return totalEntries;
	}
	public void setTotalEntries(Long totalEntries) {
		this.totalEntries = totalEntries;
	}

	public DBState getDbs() {
		return dbs;
	}

	public void setDbs(DBState dbs) {
		this.dbs = dbs;
	}

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}

	public ValidationCampaign getVc() {
		return vc;
	}

	public void setVc(ValidationCampaign vc) {
		this.vc = vc;
	}

	public LexiconOfrlexDto getEntry() {
		return entry;
	}

	public void setEntry(LexiconOfrlexDto entry) {
		this.entry = entry;
	}
	
	
    
}

/**
 * sub class for sub json object representing pagination infos
 * @param descending
 */
class Pagination {
	private int page;
	private int rowsPerPage;
	private String sortBy;
	private String descending;
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRowsPerPage() {
		return rowsPerPage;
	}
	public void setRowsPerPage(int rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public String getDescending() {
		return descending;
	}
	public void setDescending(String descending) {
		this.descending = descending;
	}
}