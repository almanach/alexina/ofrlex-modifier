package fr.inria.profiterole.almanach.ofrlexmodifier.controller;

import java.beans.IntrospectionException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.hibernate.envers.AuditReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.base.Joiner;
import com.google.common.base.Stopwatch;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.DBState;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconOfrlex4;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.Role;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.User;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.ValidationCampaign;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.dto.LexiconOfrlexDto;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.dto.OfrlexPayload;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.dto.TempUser;
import fr.inria.profiterole.almanach.ofrlexmodifier.exception.UserNotFoundException;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.UserRepository;
import fr.inria.profiterole.almanach.ofrlexmodifier.service.DBStateService;
import fr.inria.profiterole.almanach.ofrlexmodifier.service.FileStorageService;
import fr.inria.profiterole.almanach.ofrlexmodifier.service.LexiconOfrlex4Service;
import fr.inria.profiterole.almanach.ofrlexmodifier.service.RoleService;
import fr.inria.profiterole.almanach.ofrlexmodifier.service.UserService;
import fr.inria.profiterole.almanach.ofrlexmodifier.service.ValidationCampaignService;
import fr.inria.profiterole.almanach.ofrlexmodifier.service.VariantService;

@RestController()
@RequestMapping("/api")
public class BackendController {

	private static final Logger LOG = LoggerFactory.getLogger(BackendController.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserService us;

	@Autowired
	private LexiconOfrlex4Service ofrlexService;

	@Autowired
	private VariantService variantService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private DBStateService dbss;

	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	ValidationCampaignService vcs;
	
	
	@Autowired
    SimpUserRegistry userRegistry;
	
	@Autowired
	AuditReader auditReader;

	@GetMapping("/ping")
	public String test() throws UnsupportedEncodingException {
		return "pong";
	}
	
//	@RequestMapping(value = "/ping", method = RequestMethod.GET)
//	@PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
//	public String testRoles(Authentication auth) throws Exception {
//		System.out.println(auth.getAuthorities());
//		System.out.println(us.findByUsername(auth.getName()).getRoles().get(0).getRoleName());
//		System.out.println(auth.getAuthorities().contains(roleService.getRole("ADMIN_USER")));
//		System.out.println(auth.getName());
//		System.out.println(us.findByUsername(auth.getName()).getRoles());
//		System.out.println(us.isAdmin(auth.getName()));
//		return auth.getName();
//	}
	
	@RequestMapping(value = "/history/ofrlex", method = RequestMethod.GET)
	public String ofrlexHistory() throws JsonGenerationException, JsonMappingException, IOException {
		return ofrlexService.getHistory();
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	@PreAuthorize("hasAuthority('ADMIN_USER')")
	public List<User> getUsers() {
		return us.findAllUsers();
	}
	
	@RequestMapping(value = "/users/logged", method= RequestMethod.GET)
	public List<String> getLoggedUsers() {
		List<String> names = new ArrayList<String>();
    	for (SimpUser su : userRegistry.getUsers()) { names.add(su.getName()); }
    	return names;
	}

	@RequestMapping(value = "/users/roles")
	@PreAuthorize("hasAuthority('ADMIN_USER')")
	public List<Role> getRoles() {
		return roleService.getRoles();
	}

	@RequestMapping(value = "/user/update")
	@PreAuthorize("hasAuthority('STANDARD_USER') or hasAuthority('ADMIN_USER')")
	public @ResponseBody User updateUser(Authentication auth, @RequestBody User userInfos) {
		userInfos.setUsername(auth.getName());
		return us.updateInfos(userInfos);
	}

	@RequestMapping(value = "/user/update/{username}", method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('ADMIN_USER')")
	public @ResponseBody User updateUser(Authentication auth, @RequestBody TempUser userInfos,
			@PathVariable("username") String username) {
		userInfos.setUsername(username);
		return us.updateInfos(userInfos);
	}

	@RequestMapping(value = "/user/update/role/{username}", method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('ADMIN_USER')")
	public @ResponseBody User updateUserRole(@RequestBody List<String> roleNames,
			@PathVariable("username") String username) {
		return us.changeRoles(roleNames, username);
	}

	@RequestMapping(value = "/user/add", method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('ADMIN_USER')")
	public String creaUser(@RequestBody TempUser tempUser) throws IOException {
		if (!us.exists(tempUser.getUsername())) {
			us.creaUser(tempUser);
			return "success";
		} else {
			return "already exists";
		}
	}

	@RequestMapping(value = "/user/changepwd", method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
	public boolean changeUserPassword(@RequestBody TempUser tempUser, Authentication auth) {
		if (auth.getName().equals(tempUser.getUsername())) {
			us.changePassword(tempUser.getUsername(), tempUser.getPassword());
			return true;
		} else {
			return false;
		}
	}

	@RequestMapping(value = "/user/delete/{username}", method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('ADMIN_USER')")
	public boolean deleteUser(@PathVariable("username") String username) {
		return us.deleteUser(username);
	}

	@GetMapping(path = "/user/{id}")
	@PreAuthorize("hasAuthority('ADMIN_USER')")
	public @ResponseBody User getUserById(@PathVariable("id") String id) {

		return userRepository.findById(id).map(user -> {
			LOG.info("Reading user with id " + id + " from database.");
			return user;
		}).orElseThrow(
				() -> new UserNotFoundException("The user with the id " + id + " couldn't be found in the database."));
	}

	@GetMapping(path = "/user")
	@PreAuthorize("hasAuthority('STANDARD_USER') or hasAuthority('ADMIN_USER')")
	public @ResponseBody User getSelf(Authentication auth) {
		return userRepository.findById(auth.getName()).map(user -> {
			LOG.info("Reading user with id " + auth + " from database.");
			return user;
		}).orElseThrow(() -> new UserNotFoundException(
				"The user with the id " + auth + " couldn't be found in the database."));
	}

	@RequestMapping(path = "/secured", method = RequestMethod.GET)
	public @ResponseBody String getSecured(Authentication authentication) {
		LOG.info("{} logged in.", authentication.getName());
		return "success";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(path = "/ofrlex/{page}/{size}/{sort}/{descending}", method = RequestMethod.GET)
	@PreAuthorize("permitAll()")
//	@PreAuthorize("hasAuthority('STANDARD_USER') or hasAuthority('ADMIN_USER')")
	public @ResponseBody PageResponse getOfrlexPages(@PathVariable("page") int page, @PathVariable("size") int size,
			@PathVariable("sort") String sort, @PathVariable("descending") boolean descending)
			throws UnsupportedEncodingException {
		System.out.println("PAGE REQUESTED");
		Stopwatch stopwatch = Stopwatch.createStarted();
		PageResponse response = new PageResponse();
		Map<String, Object> res = new HashMap<String, Object>();
		if (sort.equals(""))
			res = ofrlexService.getPage(page, size);
		else
			res = ofrlexService.getSortedPage(page, size, sort, descending);
		LOG.info("getPage {}", stopwatch);
		List<LexiconOfrlexDto> entriesWithVariants = new ArrayList<LexiconOfrlexDto>();
		List<LexiconOfrlex4> entries = (List<LexiconOfrlex4>) res.get("entries");
		for (LexiconOfrlex4 entry : entries) {
//			entriesWithVariants.add(variantService.computeVariants(entry));
			LexiconOfrlexDto dto = new LexiconOfrlexDto();dto.setOfrlexValues(entry); entriesWithVariants.add( dto );
		}
		LOG.info("variants done {}", stopwatch);
		response.setEntries(entriesWithVariants);
		response.setTotalEntries((Long) res.get("totalPages"));
		LOG.info("PAGE DELIVERED {}", stopwatch);
		return response;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(path = "/ofrlex/filter", method = RequestMethod.GET)
	@PreAuthorize("permitAll()")
//	@PreAuthorize("hasAuthority('STANDARD_USER') or hasAuthority('ADMIN_USER')")
	public @ResponseBody PageResponse getOfrlexPagesFilter(@RequestParam String pattern, @RequestParam int page,
			@RequestParam int pageSize, @RequestParam String col, @RequestParam String type, Authentication authentication)
			throws UnsupportedEncodingException {
	
		PageResponse response = new PageResponse();
		Map<String, Object> filtered = ofrlexService.filter(pattern, page, pageSize, col, type);
	
		List<LexiconOfrlexDto> entriesWithVariants = new ArrayList<LexiconOfrlexDto>();
		List<LexiconOfrlex4> entries = (List<LexiconOfrlex4>) filtered.get("entries");
		if (entries.size() > 0)
			for (LexiconOfrlex4 entry : entries) {
//				entriesWithVariants.add(variantService.computeVariants(entry));
				LexiconOfrlexDto dto = new LexiconOfrlexDto();dto.setOfrlexValues(entry); entriesWithVariants.add( dto );
			}
	
		response.setEntries(entriesWithVariants);
		response.setTotalEntries((Long) filtered.get("totalPages"));
		return response;
	}

	// TODO: to clean
	@RequestMapping(path = "/ofrlex/add", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasAuthority('STANDARD_USER') or hasAuthority('ADMIN_USER')")
	public String saveEntry(@RequestBody LexiconOfrlex4 entry, Authentication auth) {
		if (dbss.getStatus("ofrlex").isLocked()) {
			if (us.isAdmin(auth.getName()))
				ofrlexService.add(entry);
			return "frozen";
		}
		ofrlexService.add(entry);
		return "success";
	}

	// TODO: to clean
	@RequestMapping(path = "/ofrlex/update", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasAuthority('STANDARD_USER') or hasAuthority('ADMIN_USER')")
	public String saveChanges(@RequestBody List<LexiconOfrlex4> entries) {
		if (dbss.getStatus("ofrlex").isLocked())
			return "frozen";
		ofrlexService.update(entries);
		return "success";
	}

	// TODO: to clean
	@RequestMapping(path = "/ofrlex/delete", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasAuthority('STANDARD_USER') or hasAuthority('ADMIN_USER')")
	public String deleteEntries(@RequestBody List<LexiconOfrlex4> entries) throws UnsupportedEncodingException {
		if (dbss.getStatus("ofrlex").isLocked())
			return "frozen";
		ofrlexService.removeMultiple(entries);
		return "success";
	}

	@RequestMapping(path = "/ofrlex/import/excel", method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('STANDARD_USER') or hasAuthority('ADMIN_USER')")
	public UploadFileResponse importExcel4Update(@RequestParam("file") MultipartFile file, Authentication auth,
			@RequestParam("override") boolean override, @RequestParam("delete") boolean delete)
			throws EncryptedDocumentException, InvalidFormatException, IOException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, IntrospectionException {
		String fileName = fileStorageService.getFileName(file);
		LOG.info("OFRLEX Import Excel file : {}, uploaded by {}, override = {}, delete = {}", fileName, auth.getName(),
				override, delete);
		if (!delete) {
//        	Map<String, String> modifs = ofrlexService.updateFromExcel(file.getInputStream(), override);
//        	LOG.info("UPDATED EXCEL : {} modifs, {} changes", modifs.get("changes"), modifs.get("adds"));
			ofrlexService.updateBatchFromExcel(file.getInputStream(), override);
		} else {
			ofrlexService.deleteFromExcel(file.getInputStream());
			LOG.info("DELETED by excel");
		}
		return new UploadFileResponse(fileName, "", file.getContentType(), file.getSize());
//        return "success";
	}

	@RequestMapping(path = "/ofrlex/ofrlex.xlsx", method = RequestMethod.GET)
	@PreAuthorize("hasAuthority('STANDARD_USER') or hasAuthority('ADMIN_USER')")
	public ResponseEntity<InputStreamResource> excelExport(Authentication authentication)
			throws IOException, NoSuchFieldException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, IntrospectionException {

		LOG.info("OFRLEX Excel requested by {}", authentication.getName());
		ByteArrayInputStream in = ofrlexService.getExcel();
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=ofrlex.xlsx");
		LOG.debug("httpheaders set");
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
	}

	@RequestMapping(path = "/ofrlex/selection_ofrlex.xlsx", method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('STANDARD_USER') or hasAuthority('ADMIN_USER')")
	public ResponseEntity<InputStreamResource> excelSelectionExport(@RequestBody List<String> ids, Authentication auth)
			throws NoSuchFieldException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
			IOException, IntrospectionException {
		LOG.info("OFRLEX Excel by selection requested by {}", auth.getName());
//    	List<String> stringIds = ids.stream().map(Object::toString).collect(Collectors.toList());
		ByteArrayInputStream in = ofrlexService.getSelectionExcel(ids);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=selection_ofrlex.xlsx");
		LOG.debug("httpheaders set");
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
	}

	@RequestMapping(path = "/ofrlex/exportcat", method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('STANDARD_USER') or hasAuthority('ADMIN_USER')")
	public ResponseEntity<InputStreamResource> excelExportCategory(@RequestBody String cat,
			Authentication authentication) throws IOException, NoSuchFieldException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		cat = cat.replaceAll("=$", "");
		LOG.info("OFRLEX {} Excel file requested by {}", cat, authentication.getName());
		ByteArrayInputStream in = ofrlexService.getCategoryExcel(cat);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=ofrlex.xlsx");
		LOG.debug("httpheaders set");
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
	}

	/**
	 * Streams data directly to response output stream
	 * 
	 * @param response
	 * @throws IOException
	 * @throws IntrospectionException
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 */
	@RequestMapping(value = "/ofrlex/export/cats/zip", produces = "application/zip")
	public void zipExportCategories(HttpServletResponse response)
			throws IOException, NoSuchFieldException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, IntrospectionException {

		List<String> cats = ofrlexService.getCategories();

		// setting headers
		response.setStatus(HttpServletResponse.SC_OK);
		response.addHeader("Content-Disposition", "attachment; filename=\"ofrlex_cats.zip\"");

		ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream());

		// create a list to add files to be zipped
		ArrayList<File> files = new ArrayList<>(2);
		files.add(new File("README.md"));

		// package files
		for (String cat : cats) {
			// new zip entry and copying inputstream with file to zipOutputStream, after all
			// closing streams
			zipOutputStream.putNextEntry(new ZipEntry(String.format("%s.xlsx", cat)));
			ByteArrayInputStream in = ofrlexService.getCategoryExcel(cat);
//            InputStream is = InputStream(in);
//            FileInputStream fileInputStream = new FileInputStream(in);

			IOUtils.copy(in, zipOutputStream);

//            fileInputStream.close();
			in.close();
			zipOutputStream.closeEntry();
		}

		zipOutputStream.close();
	}

	@RequestMapping(value="/ofrlex/export/ilex/zip", produces="application/zip")
    public void getIlexFiles(HttpServletResponse response) throws IOException, NoSuchFieldException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
//    	List<String> cats = new ArrayList<String>();
//    	cats.add("VER");
    	List<String> cats = ofrlexService.getCategories();
//    	Map<String, List<String>> orga = ofrlexService.getCoarseCategories();
    	
        response.setStatus(HttpServletResponse.SC_OK);
        response.addHeader("Content-Disposition", "attachment; filename=\"ofrlex_ilex.zip\"");

        ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream());

        // package files
//        for (String cat : cats) {
//            //new zip entry and copying inputstream with file to zipOutputStream, after all closing streams
//            zipOutputStream.putNextEntry(new ZipEntry(  String.format("ofrlex_%s.ilex", cat)   ));
//            ByteArrayInputStream in = ofrlexService.getIlex(cat);
////            InputStream is = InputStream(in);
////            FileInputStream fileInputStream = new FileInputStream(in);
//
//            IOUtils.copy(in, zipOutputStream);
//
////            fileInputStream.close();
//            in.close();
//            zipOutputStream.closeEntry();
//        }  
        
//        for ( Entry<String, List<String>> entry : orga.entrySet()) {
//        	String key = entry.getKey();
//			List<String> value = entry.getValue();
//        	
//            //new zip entry and copying inputstream with file to zipOutputStream, after all closing streams
//            zipOutputStream.putNextEntry(new ZipEntry(  String.format("%s.ilex", key)   ));
//            ByteArrayInputStream in = ofrlexService.getIlex(key, value);
////            InputStream is = InputStream(in);
////            FileInputStream fileInputStream = new FileInputStream(in);
//
//            IOUtils.copy(in, zipOutputStream);
//
////            fileInputStream.close();
//            in.close();
//            zipOutputStream.closeEntry();
//	        
//        }
        
       // get ilex contents
       Map<String, List<String>> uposMap = new HashMap<String, List<String>>();
       List<String> ilexStrings = new ArrayList<String>(); 
       for (String cat : cats) {
    	   if(cat.endsWith("_o")) continue;
           ilexStrings.add( ofrlexService.getIlexString(cat) );
       } 
       // populate map by upos lines
	   for(String ilex : ilexStrings) {
		   String[] lines = ilex.split("\n");
		   for (String line : lines) {
			   String[] cols = line.split("\t");
			   if(cols.length < 4) continue;
			   String upos = line.split("\t")[3].split(";")[4].replaceAll(",.+", "").replaceAll("upos=","");
			   if(upos.length() <= 0) upos = "__no_UPOS__";
			   List<String> uposLines = uposMap.getOrDefault(upos, new ArrayList<String>());
			   uposLines.add(line);
			   uposMap.put(upos, uposLines);
		   }
	   }
	   System.out.println(uposMap.keySet());
	   // create zip files
	   for(Entry<String, List<String>> entry : uposMap.entrySet()) {
		   zipOutputStream.putNextEntry(new ZipEntry(  String.format("%s.ilex", entry.getKey())   ));
		   String content = Joiner.on("\n").join( entry.getValue() );
		   ByteArrayInputStream in = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
		   IOUtils.copy(in, zipOutputStream);
		   in.close(); zipOutputStream.closeEntry();
	   }
	   
	   zipOutputStream.close();
    }

	@RequestMapping(value = "/ofrlex/export/tsv/zip", produces = "application/zip")
	public void getTsvFiles(HttpServletResponse response) throws IOException, NoSuchFieldException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {

		response.setStatus(HttpServletResponse.SC_OK);
		response.addHeader("Content-Disposition", "attachment; filename=\"ofrlex_tsv.zip\"");

		ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream());

//		List<String> cats = ofrlexService.getCategories();
//        for (String cat : cats) {
//            zipOutputStream.putNextEntry(new ZipEntry( String.format("%s.tsv", cat) ));
//            ByteArrayInputStream in = ofrlexService.getTsv(cat);
//            IOUtils.copy(in, zipOutputStream);
//            in.close();
//            zipOutputStream.closeEntry();
//        }  

//		Map<String, List<String>> orga = ofrlexService.getCoarseCategories();
//		for (Entry<String, List<String>> entry : orga.entrySet()) {
//			String key = entry.getKey();
//			List<String> value = entry.getValue();
//			zipOutputStream.putNextEntry(new ZipEntry(String.format("%s.tsv", key)));
//			ByteArrayInputStream in = ofrlexService.getTsv(key, value);
//			IOUtils.copy(in, zipOutputStream);
//			in.close();
//			zipOutputStream.closeEntry();
//
//		}
		
		Map<String, List<Long>> orga = new HashMap<String, List<Long>>();
		List<LexiconOfrlex4> entries = ofrlexService.getEntries();
		for (int i = 0; i < entries.size(); i++) {
		    LexiconOfrlex4 entry = entries.get(i);
			String upos = ofrlexService.catLemma2Upos(entry.getCat(), entry.getTllemma());
			if(upos.length() != 0) { //entry.setCat(upos);
				List<Long> ids = orga.getOrDefault(upos, new ArrayList<Long>());
				ids.add(entry.getId()); 
				orga.put(upos, ids);
			}
			entries.set(i, entry);
		}
		
		for (Map.Entry<String, List<Long>> entry : orga.entrySet()) {
			String key = entry.getKey();
			List<Long> ids = entry.getValue();
			zipOutputStream.putNextEntry(new ZipEntry(String.format("%s.tsv", key)));
			ByteArrayInputStream in = ofrlexService.getTsvFromIds(key, ids);
			IOUtils.copy(in, zipOutputStream);
			in.close();
			zipOutputStream.closeEntry();
		}

		zipOutputStream.close();
	}

	@RequestMapping(path = "/ofrlex/freeze/status", method = RequestMethod.GET)
	@PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
	public @ResponseBody DBState ofrlexStatus() {
		return dbss.getStatus("ofrlex");
	}
	
	
	@RequestMapping(path = "/ofrlex/vc/start", method = RequestMethod.GET)
	@PreAuthorize("hasAuthority('ADMIN_USER')")
	public @ResponseBody OfrlexPayload startValidationCampaign(List<LexiconOfrlex4> entries, SimpMessageHeaderAccessor headerAccessor, Authentication auth) {
		System.out.println("hello validation campaign");
    	LOG.info("{} has called startcampaign REST", auth.getName());
    	OfrlexPayload payload = new OfrlexPayload();
    	payload.setSender(auth.getName());
    	ValidationCampaign vc = vcs.startCampaign(auth.getName(), entries);
    	DBState dbs = dbss.startCampaign(vc, "ofrlex");
    	payload.setDbs(dbs);
    	payload.setType(OfrlexPayload.Type.CAMPAIGN_STARTED);
    	payload.setVc(vc);
    	return payload;
	}
	
	
	@RequestMapping(path = "/ofrlex/vc/view", method = RequestMethod.GET)
	@PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
	public @ResponseBody ValidationCampaign getValidationCampaign() {
		ValidationCampaign vc = vcs.getCampaign();
		List<LexiconOfrlex4> dummyList = new ArrayList<LexiconOfrlex4>();
		vc.setTargets(dummyList); // temp remove list for less data transfer on payload (it is not needed)
		return vc;
	}
	
	
	@RequestMapping(path = "/ofrlex/vc/reset", method = RequestMethod.GET)
	@PreAuthorize("hasAuthority('ADMIN_USER') ")
	public @ResponseBody DBState resetValidationCampaign() {
		vcs.removeCampaign();
		DBState dbs = dbss.resetCampaign("ofrlex");
		return dbs;
	}
	
	@RequestMapping(path = "/ofrlex/vc/validateall/{min}", method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('ADMIN_USER')")
	public @ResponseBody OfrlexPayload validateAll( Authentication auth,
			@PathVariable("min") String min)  throws UnsupportedEncodingException{
		System.out.println(String.format("validateAll with {} by {} ", min, auth.getName() ));
		OfrlexPayload payload = new OfrlexPayload();
    	payload.setSender(auth.getName());
    	int countValidated = ofrlexService.validateEntriesByMinValidators(Integer.valueOf(min), auth.getName());
    	payload.setType(OfrlexPayload.Type.CAMPAIGN_STARTED);
    	ValidationCampaign vc = vcs.getCampaign();
    	payload.setVc(vc);
    	payload.setTotalEntries( Long.valueOf(countValidated) );
		return payload;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(path = "/ofrlex/vc/entries/{page}/{size}/{sort}/{descending}", method = RequestMethod.GET)
	@PreAuthorize("permitAll()")
	public @ResponseBody PageResponse getOfrlexValidationPages(@PathVariable("page") int page, @PathVariable("size") int size,
			@PathVariable("sort") String sort, @PathVariable("descending") boolean descending)
			throws UnsupportedEncodingException {
		Stopwatch stopwatch = Stopwatch.createStarted();
		PageResponse response = new PageResponse();
		Map<String, Object> res = new HashMap<String, Object>();
		res = ofrlexService.getPageForValidation(page, size);
		LOG.info("getPage {}", stopwatch);
		List<LexiconOfrlexDto> entriesWithVariants = new ArrayList<LexiconOfrlexDto>();
		List<LexiconOfrlex4> entries = (List<LexiconOfrlex4>) res.get("entries");
		for (LexiconOfrlex4 entry : entries) {
			LexiconOfrlexDto dto = new LexiconOfrlexDto();dto.setOfrlexValues(entry); entriesWithVariants.add( dto );
		}
		LOG.info("variants done {}", stopwatch);
		response.setEntries(entriesWithVariants);
		response.setTotalEntries((Long) res.get("totalPages"));
		LOG.info("PAGE DELIVERED {}", stopwatch);
		
		us.checkGaelEmpty();
		return response;
	}
	
	
	
	
//	@RequestMapping(path = "/ofrlex/vc/validate", method = RequestMethod.POST)
//	@PreAuthorize("hasAuthority('ADMIN_USER') or  hasAuthority('STANDARD_USER') ")
//	public @ResponseBody OfrlexPayload validateEntry(LexiconOfrlex4 entry, SimpMessageHeaderAccessor headerAccessor, Authentication auth) {
//		OfrlexPayload payload = new OfrlexPayload();
//    	payload.setSender(auth.getName());
//    	if(dbss.getStatus("ofrlex").isLocked()) {
//    		payload.setType(OfrlexPayload.Type.UPDATE_LOCKED);
//    	}else {
//    		payload.setType(OfrlexPayload.Type.UPDATE_SUCCESS);
//    		List<LexiconOfrlexDto> entriesWithVariants = new ArrayList<LexiconOfrlexDto>();
//    		
//    		List<LexiconOfrlex4> entries = ofrlexService.getEntries(socketDto.getIds());
//
//    		for(LexiconOfrlex4 e : entries) {
//    			LexiconOfrlexDto dto = new LexiconOfrlexDto();
//    			dto.setOfrlexValues(e);
//    			entriesWithVariants.add(dto);
//    		}
//    		
//    		payload.setEntries(entriesWithVariants);
//    		LexiconOfrlex4 updated = ofrlexService.update(entry, auth.getName(), "validated");
//    		payload.set
//    		payload.setDbs( dbss.getStatus("ofrlex") );
//    	}
//		
//		return payload;
//	}

}

/**
 * class to map the page response into a json format (automatically with
 * jackson)
 * 
 * @author Gael Guibon
 */
class PageResponse {
	private List<LexiconOfrlexDto> entries = new ArrayList<LexiconOfrlexDto>();
	private Long totalEntries;

	public PageResponse() {
	}

	public List<LexiconOfrlexDto> getEntries() {
		return entries;
	}

	public void setEntries(List<LexiconOfrlexDto> entries) {
		this.entries = entries;
	}

	public Long getTotalEntries() {
		return totalEntries;
	}

	public void setTotalEntries(Long totalEntries) {
		this.totalEntries = totalEntries;
	}
}
