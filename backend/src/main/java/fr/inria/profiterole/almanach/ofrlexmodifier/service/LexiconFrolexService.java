package fr.inria.profiterole.almanach.ofrlexmodifier.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconFrolex;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.LexiconFrolexDao;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.LexiconFrolexRepository;


@Service
public class LexiconFrolexService {

	@Autowired
	private LexiconFrolexRepository frolexRepository;
	
	@Autowired
	private LexiconFrolexDao dao;
	
	private static final String FROLEX_EXCEL = "lexicons/Frolex_BFM.xlsx";
	
	private static final Logger log = LoggerFactory.getLogger(LexiconFrolexService.class);
	
	public LexiconFrolexService() {}
	
	@PostConstruct
	public void init() throws EncryptedDocumentException, InvalidFormatException, IOException {
		if(frolexRepository.count() < 1) {
			Workbook workbook = WorkbookFactory.create(getClass().getClassLoader().getResourceAsStream(FROLEX_EXCEL));	
	    	Sheet sheet = workbook.getSheetAt(0);
	    	Map<String, Integer> colMap = getLabelledColumns(sheet.getRow(0));
	    	sheet.removeRow(sheet.getRow(0));
	    	for (Row row: sheet )  initAndAdd(row, colMap);
	    	log.info("Frolex parsing DONE with {} entries", frolexRepository.count() );
		}else {
			log.info("Frolex data already in database. Skipping excel retrieval.");
		}
	}
	
	
	/**
	 * Add a lexicon entry to the list (or the repository if database)
	 * @param entry Object mapping non calculated external values from the lexicon
	 * @return the added entry (useful to get the generated Id from database just in case)
	 */
	public LexiconFrolex add(LexiconFrolex entry) {
		return frolexRepository.save(entry);
	}
	
	public void update(List<LexiconFrolex> entries) {
		frolexRepository.saveAll(entries);
	}
	
	public void remove(Long id) {
		frolexRepository.deleteById(id);
	}
	
	public void removeMultiple(List<LexiconFrolex> entries) {
		frolexRepository.deleteAll(entries);
	}
	
	/**
	 * Directly set the lexicon entries
	 * @param entriesRow
	 */
	public void setEntries(List<LexiconFrolex> entriesRow) {
		frolexRepository.saveAll(entriesRow);
	}
	
	/**
	 * Retrieve the lexicon entries
	 * @return
	 */
	public List<LexiconFrolex> getEntries() {
		return frolexRepository.findAll();
	}
	
	
	public List<LexiconFrolex> getByForm(String form) {
		return dao.findByForm(form);
	}
	
	
	/**
	 * Because POI does not parse column names as a dataframe, with have to parse the header and put the name/index association into a map.
	 * @param header (POI Row class)
	 * @return Map<String, Integer> of Map<labelColumn, indexColumn>
	 */
	private Map<String, Integer> getLabelledColumns(Row header) {
		Map<String, Integer> map = new HashMap<String,Integer>();
    	short minColIx = header.getFirstCellNum(); //get the first column index for a row
    	short maxColIx = header.getLastCellNum(); //get the last column index for a row
    	for(short colIx=minColIx; colIx<maxColIx; colIx++) { //loop from first to last index
    	   Cell cell = header.getCell(colIx); //get the cell
    	   map.put(cell.getStringCellValue(),cell.getColumnIndex()); //add the cell contents (name of column) and cell index to the map
    	 }
    	return map;
	}
	
	/**
	 * Initialize the entry object of the lexicon with values based on the map of Labels and index columns.
	 * @param row A row from POI
	 * @param map String (label), Integer (index column) : example "Nom" , 2
	 * @return the entry object filled with values
	 */
	private LexiconFrolex initRow(Row row, Map<String, Integer> map) {
		DataFormatter dataFormatter = new DataFormatter();
		LexiconFrolex gl = new LexiconFrolex();
		gl.setForm( dataFormatter.formatCellValue(row.getCell(map.get("form"))) );
		gl.setBfmFreq(dataFormatter.formatCellValue(row.getCell(map.get("F_bfm"))));
		gl.setMsdAfrlex(dataFormatter.formatCellValue(row.getCell(map.get("msd_afrlex"))));
		gl.setMsdBFM(dataFormatter.formatCellValue(row.getCell(map.get("msd_bfm"))));
		gl.setMsdCattexConv(dataFormatter.formatCellValue(row.getCell(map.get("msd_cattex_conv"))));
		gl.setLemma(dataFormatter.formatCellValue(row.getCell(map.get("lemma"))));
		gl.setLemmaSource(dataFormatter.formatCellValue(row.getCell(map.get("lemma_src"))));
		gl.setComment(dataFormatter.formatCellValue(row.getCell(map.get("comment"))));
		return gl;
	}
	
	/**
	 * Wrap the initRow method and the database population in order to ignore row with non integer id. Save some computation.
	 * @param row
	 * @param map
	 */
	private void initAndAdd(Row row, Map<String, Integer> map) {
		frolexRepository.save(initRow(row, map));
	}
}
