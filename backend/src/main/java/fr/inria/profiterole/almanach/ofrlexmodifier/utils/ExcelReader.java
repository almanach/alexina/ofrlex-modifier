package fr.inria.profiterole.almanach.ofrlexmodifier.utils;
import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.inria.profiterole.almanach.ofrlexmodifier.service.LexiconGodefroyService;

public class ExcelReader {
//    public static final String MODIF_EXCEL = "data/dummy_ofrmodifs.xlsx";
    public static final String OFRLEX_EXCEL = "lexicons/ofrlex4.xlsx";
    public static final String OFRLEX_VERBS_EXCEL = "lexicons/ofrlex4_verbs.xlsx";
    public static final String GODEFROY_EXCEL = "lexicons/Godefroy.xlsx";
    
    @Autowired
    LexiconGodefroyService glservice;
    
    public void read() throws IOException, InvalidFormatException {

    	// Getting the file from resources
    	File file = new File(getClass().getClassLoader().getResource(OFRLEX_VERBS_EXCEL).getFile());
    	
        // Creating a Workbook from an Excel file (.xls or .xlsx)
        Workbook workbook = WorkbookFactory.create(file);

//        // Retrieving the number of sheets in the Workbook
//        System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");

//        System.out.println("Retrieving Sheets using for-each loop");
//        for(Sheet sheet: workbook) {
//            System.out.println("=> " + sheet.getSheetName());
//        }

        // Getting the Sheet at index zero
        Sheet sheet = workbook.getSheetAt(0);
        System.out.println(sheet.getHeader());

        // Create a DataFormatter to format and get each cell's value as String
        DataFormatter dataFormatter = new DataFormatter(); 

        // 2. Or you can use a for-each loop to iterate over the rows and columns
        System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
        int i = 0;
        for (Row row: sheet) {
        	i++;
        	if(i >= 10) {break;}
            for(Cell cell: row) {
                String cellValue = dataFormatter.formatCellValue(cell);
                System.out.print(cellValue + "\t");
            }
            System.out.println();
        }

        // Closing the workbook
        workbook.close();
    }
    
    
//    public void readGodefroy() throws EncryptedDocumentException, InvalidFormatException, IOException {
//    	File file = new File(getClass().getClassLoader().getResource(OFRLEX_VERBS_EXCEL).getFile());
//    	Workbook workbook = WorkbookFactory.create(file);
//    	Sheet sheet = workbook.getSheetAt(0);
//    	DataFormatter dataFormatter = new DataFormatter();
//    	for (Row row: sheet) {
//    		LexiconGodefroy gl = new LexiconGodefroy(dataFormatter.formatCellValue(row.getCell(0)),
//    				dataFormatter.formatCellValue(row.getCell(1)), 
//    				dataFormatter.formatCellValue(row.getCell(2)), 
//    				dataFormatter.formatCellValue(row.getCell(3)),
//    				dataFormatter.formatCellValue(row.getCell(4)), 
//    				dataFormatter.formatCellValue(row.getCell(5)),
//    				dataFormatter.formatCellValue(row.getCell(6)), 
//    				dataFormatter.formatCellValue(row.getCell(7)), 
//    				dataFormatter.formatCellValue(row.getCell(8)) 
//    				);
//    		glservice.add(gl);
//        }
//    }
}
