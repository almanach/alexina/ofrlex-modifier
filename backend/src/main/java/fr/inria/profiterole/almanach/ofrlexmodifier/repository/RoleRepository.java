package fr.inria.profiterole.almanach.ofrlexmodifier.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.Role;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {
	
	Role findByRoleName(String roleName);
	List<Role> findAll();
	
}
