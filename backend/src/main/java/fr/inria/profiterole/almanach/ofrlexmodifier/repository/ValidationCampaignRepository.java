package fr.inria.profiterole.almanach.ofrlexmodifier.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconOfrlex4;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.ValidationCampaign;


@Repository
public interface ValidationCampaignRepository extends CrudRepository<ValidationCampaign, Long>{

	List<ValidationCampaign> findAll();
}
