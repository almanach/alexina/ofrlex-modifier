package fr.inria.profiterole.almanach.ofrlexmodifier.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Value("${security.signing-key}")
	private String signingKey;

   @Value("${security.encoding-strength}")
   private Integer encodingStrength;

   @Value("${security.security-realm}")
   private String securityRealm;
  
//   @Autowired
//   private AppUserDetailsService userDetailsService;
   
//   @Bean
//   public DaoAuthenticationProvider authenticationProvider() {
//       final DaoAuthenticationProvider bean = new CustomDaoAuthenticationProvider();
//       bean.setUserDetailsService(userDetailsService);
//       bean.setPasswordEncoder(passwordEncoder());
//       return bean;
//   }
   
//   @Autowired
//   private SessionRegistry sessionRegistry;
	
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//
//        http
//            .sessionManagement()
//            .sessionCreationPolicy(SessionCreationPolicy.STATELESS) // No session will be created or used by spring security
//        .and()
//            .httpBasic()
//            .realmName(securityRealm)
//        .and()
//            .authorizeRequests()
//                .antMatchers("/api/hello").permitAll()
//                .antMatchers("/api/user/**").permitAll() // allow every URI, that begins with '/api/user/'
//                .antMatchers("/api/secured").authenticated()
//                .antMatchers("/api/ofrlex/**").authenticated()
//                //.anyRequest().authenticated() // protect all other requests
//        .and()
//            .csrf().disable(); // disable cross site request forgery, as we don't use cookies - otherwise ALL PUT, POST, DELETE will get HTTP 403!
//    }
   
   @Override
   protected void configure(HttpSecurity http) throws Exception {
	   http
	       .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
	//       .sessionManagement().sessionFixation().migrateSession()
	//       .and()
	//       .httpBasic().realmName(securityRealm)
	       .and()
	//       .csrf().disable()
	       .csrf()
	           // ignore our stomp endpoints since they are protected using Stomp headers
	           .ignoringAntMatchers("/app/**", "/topic", "/topic/**")
	           .and()
	       .headers()
	           // allow same origin to frame our site to support iframe SockJS
	           .frameOptions().sameOrigin()
	        .and()
	       .authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/oauth/token").permitAll()
	       .antMatchers("/app/**", "/topic", "/topic/**").permitAll()
	    ;
	        
//      http.cors().configurationSource(request -> new CorsConfiguration().applyPermitDefaultValues());

//      http.sessionManagement().maximumSessions(1).maxSessionsPreventsLogin(true).sessionRegistry(sessionRegistry);
   }
   
//   @Override
//   public void configure(WebSecurity web) throws Exception {
//     web.ignoring()
//     	.antMatchers("/ws/**");
////       .antMatchers(HttpMethod.OPTIONS);
//   }
   
//   @Override
//   protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
//       auth.authenticationProvider(authenticationProvider());
//   }
   
//   @Bean
//	public static SessionRegistry sessionRegistry() {
//		return new SessionRegistryImpl();
//	}

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
       return super.authenticationManager();
    }
    
//    @Bean
//    public ServletListenerRegistrationBean<HttpSessionEventPublisher> httpSessionEventPublisher() {
//        return new ServletListenerRegistrationBean<HttpSessionEventPublisher>(new HttpSessionEventPublisher());
//    }
    
    @Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
    }
    
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//       auth.userDetailsService(userDetailsService)
//               .passwordEncoder(new BCryptPasswordEncoder(encodingStrength));
//    }
    
//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
//    }
    
    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
       JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
       converter.setSigningKey(signingKey);
       return converter;
    }

    @Bean
    public TokenStore tokenStore() {
       return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    @Primary //Making this primary to avoid any accidental duplication with another token service instance of the same name
    public DefaultTokenServices tokenServices() {
       DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
       defaultTokenServices.setTokenStore(tokenStore());
       defaultTokenServices.setSupportRefreshToken(true);
       return defaultTokenServices;
    }

    

    
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("foo").password("{noop}bar").roles("USER");
//    }
}
