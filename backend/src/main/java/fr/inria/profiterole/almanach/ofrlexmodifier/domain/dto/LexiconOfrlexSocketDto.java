package fr.inria.profiterole.almanach.ofrlexmodifier.domain.dto;

import java.util.ArrayList;
import java.util.List;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconOfrlex4;

public class LexiconOfrlexSocketDto {

	List<Long> ids = new ArrayList<Long>();
	LexiconOfrlex4 entry = new LexiconOfrlex4();
	
	public LexiconOfrlexSocketDto() {}
		
	public LexiconOfrlexSocketDto(List<Long> ids, LexiconOfrlex4 entry) {
		super();
		this.ids = ids;
		this.entry = entry;
	}

	public List<Long> getIds() {
		return ids;
	}
	public void setIds(List<Long> ids) {
		this.ids = ids;
	}
	public LexiconOfrlex4 getEntry() {
		return entry;
	}
	public void setEntry(LexiconOfrlex4 entry) {
		this.entry = entry;
	}
	
	
	
}
