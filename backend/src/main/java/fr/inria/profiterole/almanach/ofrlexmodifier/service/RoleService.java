package fr.inria.profiterole.almanach.ofrlexmodifier.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.Role;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.RoleRepository;

@Service
public class RoleService {

	@Autowired
	RoleRepository rr;
	
	@PostConstruct
    public void init() {
		if(rr.findByRoleName("GUEST_USER") == null) {
			Role anon = new Role();
	    	anon.setRoleName("GUEST_USER"); anon.setDescription("Guest User - Has no admin rights and cannot do any modification");
	    	rr.save(anon);
		}
		if(rr.count() < 1) {
			Role r = new Role();
	    	r.setRoleName("STANDARD_USER"); r.setDescription("Standard User - Has no admin rights");
	    	rr.save(r);
	    	Role admin = new Role();
	    	admin.setRoleName("ADMIN_USER"); admin.setDescription("Admin User - Has permission to perform admin tasks");
	    	rr.save(admin);
		}
	}
	
	public Role getRole(String roleName) {
		return rr.findByRoleName(roleName);
	}
	
	public List<Role> getRoles() {
		return rr.findAll();
	}
	
}
