package fr.inria.profiterole.almanach.ofrlexmodifier.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconFrolex;


@Repository
public interface LexiconFrolexRepository extends CrudRepository<LexiconFrolex, Long>{
	public List<LexiconFrolex> findAll();
}
