package fr.inria.profiterole.almanach.ofrlexmodifier.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconDect;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.LexiconDectDao;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.LexiconDectRepository;


@Service
public class LexiconDectService {

	@Autowired
	private LexiconDectRepository dectRepository;
	
	@Autowired
	private LexiconDectDao dao;
	
	// dect informations are inside these
	private static final String OFRLEX4_EXCEL = "lexicons/ofrlex4.xlsx";
	private static final String OFRLEX4_VERBS_EXCEL = "lexicons/ofrlex4_verbs.xlsx";
	
	private static final Logger log = LoggerFactory.getLogger(LexiconDectService.class);
	
	public LexiconDectService() {}
	
	@PostConstruct
	public void init() throws EncryptedDocumentException, InvalidFormatException, IOException {
		
		if(dectRepository.count() < 1) {
			Workbook workbook = WorkbookFactory.create(getClass().getClassLoader().getResourceAsStream(OFRLEX4_EXCEL));	
	    	Sheet sheet = workbook.getSheetAt(0);
	    	Map<String, Integer> colMap = getLabelledColumns(sheet.getRow(0));
	    	sheet.removeRow(sheet.getRow(0));
	    	for (Row row: sheet )  initAndAdd(row, colMap);
	    	log.info("DECT parsing DONE with {} entries", dectRepository.count() );
	    	
	    	
	    	workbook = WorkbookFactory.create(getClass().getClassLoader().getResourceAsStream(OFRLEX4_VERBS_EXCEL));
	    	sheet = workbook.getSheetAt(0);
	    	colMap = getLabelledColumns(sheet.getRow(0));
	    	sheet.removeRow(sheet.getRow(0));	
	    	for (Row row: sheet ) initAndAdd(row, colMap);
	    	log.info("DECT: Ofrlex and OfrlexVerbs for DECT parsing SUCCESS");
		}else {
			log.info("DECT data already in database. Skipping excel retrieval.");
		}
	}
	
	
	/**
	 * Add a lexicon entry to the list (or the repository if database)
	 * @param entry Object mapping non calculated external values from the lexicon
	 * @return the added entry (useful to get the generated Id from database just in case)
	 */
	public LexiconDect add(LexiconDect entry) {
		return dectRepository.save(entry);
	}
	
	public void update(List<LexiconDect> entries) {
		dectRepository.saveAll(entries);
	}
	
	public void remove(Long id) {
		dectRepository.deleteById(id);
	}
	
	public void removeMultiple(List<LexiconDect> entries) {
		dectRepository.deleteAll(entries);
	}
	
	/**
	 * Directly set the lexicon entries
	 * @param entriesRow
	 */
	public void setEntries(List<LexiconDect> entriesRow) {
		dectRepository.saveAll(entriesRow);
	}
	
	
	public Optional<LexiconDect> getById(Long id) {
		return dectRepository.findById(id);
//		return Optional.of(dao.findById(id));
	}
	
	/**
	 * Retrieve the lexicon entries
	 * @return
	 */
	public List<LexiconDect> getEntries() {
		return dectRepository.findAll();
	}
	
	
	
	/**
	 * Because POI does not parse column names as a dataframe, with have to parse the header and put the name/index association into a map.
	 * @param header (POI Row class)
	 * @return Map<String, Integer> of Map<labelColumn, indexColumn>
	 */
	private Map<String, Integer> getLabelledColumns(Row header) {
		Map<String, Integer> map = new HashMap<String,Integer>();
    	short minColIx = header.getFirstCellNum(); //get the first column index for a row
    	short maxColIx = header.getLastCellNum(); //get the last column index for a row
    	for(short colIx=minColIx; colIx<maxColIx; colIx++) { //loop from first to last index
    	   Cell cell = header.getCell(colIx); //get the cell
    	   map.put(cell.getStringCellValue(),cell.getColumnIndex()); //add the cell contents (name of column) and cell index to the map
    	 }
    	return map;
	}
	
	/**
	 * Initialize the entry object of the lexicon with values based on the map of Labels and index columns.
	 * @param row A row from POI
	 * @param map String (label), Integer (index column) : example "Nom" , 2
	 * @return the entry object filled with values
	 */
	private LexiconDect initRow(Row row, Map<String, Integer> map) {
		// __id__	GD (from DECT)	GD page (from DECT)	TL (from DECT)	TL from DECT is hw	DECT entry	DECT page	DECT cat.ms	DECT scat	
		// DECT lemma	DECT freq	F-B (from DECT)	FEW etymon (from DECT)	FEW ref (from DECT)	TLF (from DECT)	AND (from DECT)	DMF (from DECT)	DECT def	
		DataFormatter dataFormatter = new DataFormatter();
		LexiconDect gl = new LexiconDect();
		Long id;
		try { id = Long.parseLong( dataFormatter.formatCellValue(row.getCell(map.get("__id__")))); gl.setId( id ) ;
		}catch(Exception e) {log.error("id is not an integer! \nTrace: {}", e);}
		
		if(dataFormatter.formatCellValue(row.getCell(map.get("GD (from DECT)"))) != null) gl.setGodefroy(dataFormatter.formatCellValue(row.getCell(map.get("GD (from DECT)"))));
		if(dataFormatter.formatCellValue(row.getCell(map.get("GD page (from DECT)"))) != null) gl.setGodefroyPage(dataFormatter.formatCellValue(row.getCell(map.get("GD page (from DECT)"))));
		if(dataFormatter.formatCellValue(row.getCell(map.get("TL (from DECT)"))) != null) gl.setTobler(dataFormatter.formatCellValue(row.getCell(map.get("TL (from DECT)"))));
		if(dataFormatter.formatCellValue(row.getCell(map.get("TL from DECT is hw"))) != null) gl.setToblerHeadword(dataFormatter.formatCellValue(row.getCell(map.get("TL from DECT is hw"))));
		if(dataFormatter.formatCellValue(row.getCell(map.get("DECT entry"))) != null) gl.setEntry(dataFormatter.formatCellValue(row.getCell(map.get("DECT entry"))));
		if(dataFormatter.formatCellValue(row.getCell(map.get("DECT page"))) != null) gl.setPage(dataFormatter.formatCellValue(row.getCell(map.get("DECT page"))));
		if(dataFormatter.formatCellValue(row.getCell(map.get("DECT cat.ms"))) != null) gl.setCategoryMorphoSyntactic(dataFormatter.formatCellValue(row.getCell(map.get("DECT cat.ms"))));
		if(dataFormatter.formatCellValue(row.getCell(map.get("DECT scat"))) != null) gl.setSubcat(dataFormatter.formatCellValue(row.getCell(map.get("DECT scat"))));
		if(dataFormatter.formatCellValue(row.getCell(map.get("DECT lemma"))) != null) gl.setLemma(dataFormatter.formatCellValue(row.getCell(map.get("DECT lemma"))));
		if(dataFormatter.formatCellValue(row.getCell(map.get("DECT freq"))) != null) gl.setFreq(dataFormatter.formatCellValue(row.getCell(map.get("DECT freq"))));
		if(dataFormatter.formatCellValue(row.getCell(map.get("F-B (from DECT)"))) != null) gl.setFB(dataFormatter.formatCellValue(row.getCell(map.get("F-B (from DECT)"))));
		if(dataFormatter.formatCellValue(row.getCell(map.get("FEW ref (from DECT)"))) != null) gl.setFewRef(dataFormatter.formatCellValue(row.getCell(map.get("FEW ref (from DECT)"))));
		if(dataFormatter.formatCellValue(row.getCell(map.get("FEW etymon (from DECT)"))) != null) gl.setFewRef(dataFormatter.formatCellValue(row.getCell(map.get("FEW etymon (from DECT)"))));
		if(dataFormatter.formatCellValue(row.getCell(map.get("TLF (from DECT)"))) != null) gl.setTLF(dataFormatter.formatCellValue(row.getCell(map.get("TLF (from DECT)"))));
		if(dataFormatter.formatCellValue(row.getCell(map.get("AND (from DECT)"))) != null) gl.setDectAND(dataFormatter.formatCellValue(row.getCell(map.get("AND (from DECT)"))));
		if(dataFormatter.formatCellValue(row.getCell(map.get("DMF (from DECT)"))) != null) gl.setDMF(dataFormatter.formatCellValue(row.getCell(map.get("DMF (from DECT)"))));
		if(dataFormatter.formatCellValue(row.getCell(map.get("DECT def"))) != null) gl.setDef(dataFormatter.formatCellValue(row.getCell(map.get("DECT def"))));
//		log.info("DECT ID {}", gl.getId());
		return gl;
	}
	
	/**
	 * Wrap the initRow method and the database population in order to ignore row with non integer id. Save some computation.
	 * @param row
	 * @param map
	 */
	private void initAndAdd(Row row, Map<String, Integer> map) {
		DataFormatter dataFormatter = new DataFormatter();
		boolean correctId = true;
		boolean entryNotNull = true;
		try {
			Integer.parseInt( dataFormatter.formatCellValue(row.getCell(map.get("__id__"))) );
			if(dataFormatter.formatCellValue(row.getCell(map.get("DECT entry"))) == null) entryNotNull = false;
		}catch(Exception e) {
			correctId = false;
			log.debug("id is not an integer! : {} \n", dataFormatter.formatCellValue(row.getCell(map.get("__id__"))));
		}
		if(correctId && entryNotNull) if(!dataFormatter.formatCellValue(row.getCell(map.get("DECT entry"))).isEmpty()) dectRepository.save(initRow(row, map));
	}
}
