package fr.inria.profiterole.almanach.ofrlexmodifier.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="tobler")
public class LexiconTobler {

	@Id
	@GenericGenerator(name = "UseIdOrGenerate", strategy = "fr.inria.profiterole.almanach.ofrlexmodifier.repository.UseIdOrGenerate")
	@GeneratedValue(generator = "UseIdOrGenerate")
	@Column(unique = true, nullable = false)
	private Long id;
	private String headword;
	private String lemma;
	private String headwordMs;
	private String ref;
	private String headwordSubcat;
	private String variantOf;
	private String descendent;
	
	public LexiconTobler() {}
	
	public LexiconTobler(Long id, String headword, String lemma, String headwordMs, String ref, String headwordSubcat,
			String variantOf, String descendent) {
		super();
		this.id = id;
		this.headword = headword;
		this.lemma = lemma;
		this.headwordMs = headwordMs;
		this.ref = ref;
		this.headwordSubcat = headwordSubcat;
		this.variantOf = variantOf;
		this.descendent = descendent;
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getHeadword() {
		return headword;
	}


	public void setHeadword(String headword) {
		this.headword = headword;
	}


	public String getLemma() {
		return lemma;
	}


	public void setLemma(String lemma) {
		this.lemma = lemma;
	}


	public String getHeadwordMs() {
		return headwordMs;
	}


	public void setHeadwordMs(String headwordMs) {
		this.headwordMs = headwordMs;
	}


	public String getRef() {
		return ref;
	}


	public void setRef(String ref) {
		this.ref = ref;
	}


	public String getHeadwordSubcat() {
		return headwordSubcat;
	}


	public void setHeadwordSubcat(String headwordSubcat) {
		this.headwordSubcat = headwordSubcat;
	}


	public String getVariantOf() {
		return variantOf;
	}


	public void setVariantOf(String variantOf) {
		this.variantOf = variantOf;
	}


	public String getDescendent() {
		return descendent;
	}


	public void setDescendent(String descendent) {
		this.descendent = descendent;
	}

	@Override
	public String toString() {
		return "LexiconTobler [id=" + id + ", headword=" + headword + ", lemma=" + lemma + ", headwordMs=" + headwordMs
				+ ", ref=" + ref + ", headwordSubcat=" + headwordSubcat + ", variantOf=" + variantOf + ", descendent="
				+ descendent + "]";
	}
	
	
}
