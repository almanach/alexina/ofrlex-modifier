package fr.inria.profiterole.almanach.ofrlexmodifier.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.User;


@Repository
public interface UserRepository extends CrudRepository<User, String>{
	User findByUsername(String username);
}
