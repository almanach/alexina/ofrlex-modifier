package fr.inria.profiterole.almanach.ofrlexmodifier.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconGodefroy;

@Transactional
@Repository
public class LexiconGodefroyDaoImpl implements LexiconGodefroyDao{

	@Autowired
	JdbcTemplate jt;
	
	@Override
	public List<LexiconGodefroy> findByForm(String form) {
		RowMapper<LexiconGodefroy> rowMapper = new BeanPropertyRowMapper<LexiconGodefroy>(LexiconGodefroy.class);
		return jt.query("SELECT * FROM godefroy WHERE form = ?", rowMapper, form);
	}

}
