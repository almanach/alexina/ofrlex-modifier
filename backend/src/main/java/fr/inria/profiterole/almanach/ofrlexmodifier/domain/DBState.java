package fr.inria.profiterole.almanach.ofrlexmodifier.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "db_state")
public class DBState {

//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private Long id;
	private boolean locked;
	@Id
	private String target;
	private String author;
	private String date;
	private Long campaign;
	private boolean campaignOngoing;
	private Long total;
	private Long progress;
	
	public DBState() {}

	public DBState(boolean locked, String target, String author, String date, Long campaign, boolean campaignOngoing, Long total, Long progress) {
		super();
		this.locked = locked;
		this.target = target;
		this.author = author;
		this.date = date;
		this.campaign = campaign;
		this.campaignOngoing = campaignOngoing;
		this.total = total;
		this.progress = progress;
	}
	
	public DBState(boolean locked, String target, String author, String date, Long total, Long progress) {
		super();
		this.locked = locked;
		this.target = target;
		this.author = author;
		this.date = date;
		this.total = total;
		this.progress = progress;
	}
	
	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Long getCampaign() {
		return campaign;
	}

	public void setCampaign(Long campaign) {
		this.campaign = campaign;
	}

	public boolean isCampaignOngoing() {
		return campaignOngoing;
	}

	public void setCampaignOngoing(boolean campaignOngoing) {
		this.campaignOngoing = campaignOngoing;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Long getProgress() {
		return progress;
	}

	public void setProgress(Long progress) {
		this.progress = progress;
	}

	
	
	
	
}
