package fr.inria.profiterole.almanach.ofrlexmodifier.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Stopwatch;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconDect;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconOfrlex4;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.dto.LexiconOfrlexDto;

@Service
public class VariantService {
	
	private static final Logger LOG = LoggerFactory.getLogger(VariantService.class);
	
	@Autowired
	LexiconDectService dectService;
	
	@Autowired
	LexiconGodefroyService godefroyService;
	
	@Autowired
	LexiconOfrlex4Service ofrlexService;
	
	@Autowired
	LexiconFrolexService frolexService;

    /**
     * Compute variants to add them into the payload as LexiconOfrlexDto. Database untouched only requested.
     * @param ofrlex
     * @return
     */
    public LexiconOfrlexDto computeVariants(LexiconOfrlex4 ofrlex) {
    	Stopwatch stopwatch = Stopwatch.createStarted();
    	LexiconOfrlexDto dto = new LexiconOfrlexDto();
		dto.setOfrlexValues(ofrlex);
		String target = "";
		if(ofrlex.getVariantOf() != null) if(!ofrlex.getVariantOf().isEmpty()) target = ofrlex.getVariantOf();
		if(ofrlex.getTlvariantOf() != null) if(!ofrlex.getTlvariantOf().isEmpty()) target = ofrlex.getTlvariantOf();		
		if(target.isEmpty()) target = ofrlex.getOfrlexHeadword();
		List<LexiconOfrlex4> variants = ofrlexService.getVariantOf(target);
		LOG.info("prepadone {}", stopwatch);
		for(LexiconOfrlex4 variant : variants) {
			Stopwatch sw = Stopwatch.createStarted();
			// get complementary info from dect. need to see if different and populate DTO
			Optional<LexiconDect> dect = dectService.getById(variant.getId());
			if(dect.isPresent()) dto.setvDect(dect.get());
			// get infos from godefroy based on headword x form (entry)
			 dto.setvGodefroyList( godefroyService.getByForm(variant.getOfrlexHeadword()) );
			 dto.setvFrolexList( frolexService.getByForm(variant.getOfrlexHeadword()) );
			 LOG.info("inner loop {}", sw);
		}
		LOG.info("compute variants end {}", stopwatch);
		return dto;
    }
    
    
    public LexiconOfrlexDto computeVariants(LexiconOfrlexDto dto) {
		String target = "";
		if(!dto.getVariantOf().isEmpty()) target = dto.getVariantOf();
		if(!dto.getTlvariantOf().isEmpty()) target = dto.getTlvariantOf();		
		if(target.isEmpty()) target = dto.getOfrlexHeadword();
		List<LexiconOfrlex4> variants = ofrlexService.getVariantOf(target); 
		for(LexiconOfrlex4 variant : variants) {
			// get complementary info from dect. need to see if different and populate DTO
			Optional<LexiconDect> dect = dectService.getById(variant.getId());
			if(dect.isPresent()) dto.setvDect(dect.get());
			// get infos from godefroy based on headword x form (entry)
			dto.setvGodefroyList( godefroyService.getByForm(variant.getOfrlexHeadword()) );
			dto.setvFrolexList( frolexService.getByForm(variant.getOfrlexHeadword()) );
		}
		return dto;
    }
	
	
}
