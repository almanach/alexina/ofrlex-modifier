package fr.inria.profiterole.almanach.ofrlexmodifier.repository;

import java.util.List;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconOfrlex4;

public interface LexiconOfrlexDao {
	List<LexiconOfrlex4> findByVariantOfOrTlvariantOf(String variantOf);

	List<String> getDifferentCats();

	List<Long> getValidatorsIndexes();
	
}
