package fr.inria.profiterole.almanach.ofrlexmodifier.repository;

import java.util.List;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconFrolex;

public interface LexiconFrolexDao {
	List<LexiconFrolex> findByForm( String form );
}
