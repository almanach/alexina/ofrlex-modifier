package fr.inria.profiterole.almanach.ofrlexmodifier.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconTobler;

@Repository
public interface LexiconToblerRepository extends CrudRepository<LexiconTobler, Long>{
	public List<LexiconTobler> findAll();
}
