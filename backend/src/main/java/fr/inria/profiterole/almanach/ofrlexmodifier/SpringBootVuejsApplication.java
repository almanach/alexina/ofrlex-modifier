package fr.inria.profiterole.almanach.ofrlexmodifier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import fr.inria.profiterole.almanach.ofrlexmodifier.controller.BackendController;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.FileStorageProperties;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.LexiconDectRepository;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.LexiconGodefroyRepository;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.LexiconOfrlexRepository;
import fr.inria.profiterole.almanach.ofrlexmodifier.service.LexiconDectService;
import fr.inria.profiterole.almanach.ofrlexmodifier.service.LexiconGodefroyService;
import fr.inria.profiterole.almanach.ofrlexmodifier.service.LexiconOfrlex4Service;
import fr.inria.profiterole.almanach.ofrlexmodifier.service.VariantService;

@SpringBootApplication
@EnableConfigurationProperties({
	FileStorageProperties.class
})
public class SpringBootVuejsApplication {//implements CommandLineRunner {
	

    @SuppressWarnings("unused")
	private static final Logger LOG = LoggerFactory.getLogger(SpringBootVuejsApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringBootVuejsApplication.class, args);
	}
	
	
	@Autowired
	LexiconGodefroyService glService;
	
	@Autowired
	LexiconOfrlex4Service ofrlexService;
	
	@Autowired
	LexiconOfrlexRepository or;
	
	@Autowired
	LexiconDectService ds;
	
	@Autowired
	LexiconDectRepository dr;
	
	@Autowired
	LexiconGodefroyRepository gr;
	
	@Autowired
	BackendController bc;
	
	@Autowired
	VariantService variantService;
	
//	@Bean
//    public ServletWebServerFactory servletContainer() {
//        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory() {
//            @Override
//            protected void postProcessContext(Context context) {
//                SecurityConstraint securityConstraint = new SecurityConstraint();
//                securityConstraint.setUserConstraint("CONFIDENTIAL");
//                SecurityCollection collection = new SecurityCollection();
//                collection.addPattern("/*");
//                securityConstraint.addCollection(collection);
//                context.addConstraint(securityConstraint);
//            }
//        };
//        tomcat.addAdditionalTomcatConnectors(redirectConnector());
//        return tomcat;
//    }
//    
//    private Connector redirectConnector() {
//        Connector connector = new Connector(TomcatServletWebServerFactory.DEFAULT_PROTOCOL);
//        connector.setScheme("http");
//        connector.setPort(80);
//        connector.setSecure(false);
//        connector.setRedirectPort(443);
//        return connector;
//    }
	
	
//	@Override
//	public void run(String... args) throws Exception {
//		Stopwatch stopwatch = Stopwatch.createStarted();
////		
////		String formatted = String.format("%03d", 10);
////		System.out.println(formatted);
////		
//////		System.out.println(glService.getEntries().size());
//////		System.out.println(glService.getEntries().get(0).toString());
//////		System.out.println(ofrlexService.getEntries().size());
//////		System.out.println(ofrlexService.getEntries().get(0).toString());
//
//		
////		LexiconOfrlexDto payload = new LexiconOfrlexDto();
////		LexiconOfrlex4 amertonde = or.findByOfrlexHeadword("amertonde");
////		payload.setOfrlexValues(amertonde);
////		String target = "";
////		if(!amertonde.getVariantOf().isEmpty()) target = amertonde.getVariantOf();
////		if(!amertonde.getTlvariantOf().isEmpty()) target = amertonde.getTlvariantOf();		
////		if(target.isEmpty()) target = amertonde.getOfrlexHeadword();
////		List<LexiconOfrlex4> variants = ofrlexService.getVariantOf(target); 
////		for(LexiconOfrlex4 variant : variants) {
////			System.out.println(variant.toString());
////			// get complementary info from dect. need to see if different and populate DTO
////			Optional<LexiconDect> dect = dr.findById(variant.getId());
////			if(dect.isPresent()) {
////				System.out.println(dect.get().toString());
////				payload.setvDect(dect.get());
////			}
////			// get infos from godefroy based on headword x form (entry)
////			Optional<LexiconGodefroy> godefroy = gr.findByForm(variant.getOfrlexHeadword());
////			if(godefroy.isPresent()) {
////				System.out.println(godefroy.get().toString());
////				payload.setvGodefroy(godefroy.get());
////			}
////		}
////		System.out.println(String.format("payload DTO : %s", payload.toString() ));
////		// get info from DECT by id
//		
//		
//		
////		System.out.println(bc.getOfrlexPages(0, 5, "id", false));
//
//		   
//		   
////		List<LexiconOfrlex4> entries = (List<LexiconOfrlex4>) ofrlexService.getSortedPage(0, 15, "id", false).get("entries");
////		LOG.info("entries done {}", stopwatch);
////		List<LexiconOfrlexDto> dtos = new ArrayList<LexiconOfrlexDto>();
////		for(LexiconOfrlex4 entry : entries) variantService.computeVariants(entry);
////		
////		LOG.info("variants done {}", stopwatch);
//		
//		// lexeme embeddings
////		try {
////
////			String jsonStr = "";
////			ClassPathResource cpr = new ClassPathResource("pseudosyns4java.json");
////			byte[] bdata = FileCopyUtils.copyToByteArray(cpr.getInputStream());
////			jsonStr = new String(bdata, StandardCharsets.UTF_8);
////
////			ObjectMapper mapper = new ObjectMapper();
////			List<PseudosynSourceDto> pseudosyns;
////			pseudosyns = Arrays.asList(mapper.readValue(jsonStr, PseudosynSourceDto[].class));
////			LOG.info("Pseudosyns : {}", pseudosyns.get(0).toString());
//////			for (Scenario scenar : scenarii)
//////				this.addScenario(scenar);
////		} catch (IOException e) {
////			LOG.warn(e.getMessage());
////		}
//		
//		stopwatch.stop();
//		System.exit(0);
//		
//	}
}
