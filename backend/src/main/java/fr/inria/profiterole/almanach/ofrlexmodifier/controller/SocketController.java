package fr.inria.profiterole.almanach.ofrlexmodifier.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.DBState;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconOfrlex4;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.User;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.ValidationCampaign;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.dto.LexiconOfrlexDto;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.dto.LexiconOfrlexSocketDto;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.dto.OfrlexPayload;
import fr.inria.profiterole.almanach.ofrlexmodifier.service.DBStateService;
import fr.inria.profiterole.almanach.ofrlexmodifier.service.FileStorageService;
import fr.inria.profiterole.almanach.ofrlexmodifier.service.LexiconOfrlex4Service;
import fr.inria.profiterole.almanach.ofrlexmodifier.service.UserService;
import fr.inria.profiterole.almanach.ofrlexmodifier.service.ValidationCampaignService;
import fr.inria.profiterole.almanach.ofrlexmodifier.service.VariantService;

@Controller
public class SocketController {

		@Autowired
		DBStateService dbss;
		
		@Autowired
		LexiconOfrlex4Service ofrlexService;
		
		@Autowired
		VariantService variantService;
		
		@Autowired
		UserService us;
		
		@Autowired
		FileStorageService fileStorageService;
		
		@Autowired
		ValidationCampaignService vcs;
		
		@Autowired
	    SimpUserRegistry userRegistry;
		
		@Autowired
	    private SimpMessagingTemplate template;
		
		@SuppressWarnings("unused")
		private static final Logger LOG = LoggerFactory.getLogger(SocketController.class);

		
		@MessageMapping("/ping")
		@SendTo("/topic/public")
		public String ping(String ping, Principal principal) throws Exception {
			return "pong";
		}
		
	    @MessageMapping("/hello")
	    @SendTo("/topic/public")
	    public OfrlexPayload greeting(OfrlexPayload message, Principal principal) throws Exception {
	    	System.out.println(String.format("hello by %s", principal.getName() ));
//	        return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
	    	message.setName(String.format("hello by %s", principal.getName() ));
	    	return message;
	    }
	   
	    @MessageMapping("/users")
	    @SendTo("/topic/public")
	    public List<String> findUsers() {
	    	List<String> names = new ArrayList<String>();
	    	for (SimpUser su : userRegistry.getUsers()) { names.add(su.getName()); System.out.println(su.getName()); }
	    	return names;
	    }

	    @Scheduled(fixedDelay=300)
	    public void publishUpdates(){
	    	List<String> names = new ArrayList<String>();
	    	for (SimpUser su : userRegistry.getUsers()) { names.add(su.getName()); }
	        template.convertAndSend("/topic/public", names);
	    }

	    
	    @MessageMapping("/ofrlex.freezetoggle")
	    @SendTo("/topic/ofrlex")
	    @PreAuthorize("hasAuthority('ADMIN_USER')")
	    public OfrlexPayload toggleFreezeOfrlex( SimpMessageHeaderAccessor headerAccessor,
	    		Authentication auth) {
	    	OfrlexPayload payload = new OfrlexPayload();
	    	DBState dbs = dbss.toggle(auth.getName(), "ofrlex");
	    	payload.setType(OfrlexPayload.Type.LOCK);
	    	payload.setSender(auth.getName());
	    	payload.setDbs(dbs);
	    	return payload;
	    }
	    
	    
	    @MessageMapping("/ofrlex.add")
	    @SendTo("/topic/ofrlex")
	    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
	    public OfrlexPayload addEntry(@Payload LexiconOfrlex4 entry, SimpMessageHeaderAccessor headerAccessor,
	    		Authentication auth) throws JsonProcessingException {
	    	OfrlexPayload payload = new OfrlexPayload();
	    	payload.setSender(auth.getName());
	    	if(dbss.getStatus("ofrlex").isLocked() && !us.isAdmin(auth.getName())) {
	    		payload.setType(OfrlexPayload.Type.ADD_LOCKED);
	    	}else {
	    		payload.setType(OfrlexPayload.Type.ADD_SUCCESS);
	    		LexiconOfrlex4 saved = ofrlexService.add(entry, auth.getName());
//	    		LexiconOfrlexDto dto = variantService.computeVariants(ofrlexService.add(entry)); // commented for compiutation saving (faster feedback)
	    		LexiconOfrlexDto dto = new LexiconOfrlexDto();
	    		dto.setOfrlexValues(saved);
	    		payload.setEntries(Arrays.asList(dto));
	    	}
	    	return payload;
	    }
	    
//	    @MessageMapping("/ofrlex.update")
//	    @SendTo("/topic/ofrlex")
//	    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
//	    public OfrlexPayload updateEntries( List<LexiconOfrlex4> entries, SimpMessageHeaderAccessor headerAccessor,
//	    		Authentication auth) throws JsonProcessingException {
//	    	OfrlexPayload payload = new OfrlexPayload();
//	    	payload.setSender(auth.getName());
//	    	if(dbss.getStatus("ofrlex").isLocked()) {
//	    		payload.setType(OfrlexPayload.Type.UPDATE_LOCKED);
//	    	}else {
//	    		System.out.println(entries.toString());
//	    		ofrlexService.update(entries);
//	    		payload.setType(OfrlexPayload.Type.UPDATE_SUCCESS);
//	    		List<LexiconOfrlexDto> entriesWithVariants = new ArrayList<LexiconOfrlexDto>();
//	        	for(LexiconOfrlex4 dto : entries) entriesWithVariants.add(variantService.computeVariants(dto));
//	    		payload.setEntries(entriesWithVariants);
//	    	}
//	    	return payload;
//	    }
	    
	    @MessageMapping("/ofrlex.update")
	    @SendTo("/topic/ofrlex")
	    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
	    public OfrlexPayload updateEntries( LexiconOfrlexSocketDto socketDto, SimpMessageHeaderAccessor headerAccessor,
	    		Authentication auth) throws JsonProcessingException {
	    	OfrlexPayload payload = new OfrlexPayload();
	    	payload.setSender(auth.getName());
	    	if(dbss.getStatus("ofrlex").isLocked()) {
	    		payload.setType(OfrlexPayload.Type.UPDATE_LOCKED);
	    	}else {
	    		
	    		// check admin only for validated field
	    		if(socketDto.getEntry().getModifiedField() == "validated") {
	    			if(us.isAdmin(auth.getName()) != true) {
	    				payload.setType(OfrlexPayload.Type.UPDATE_DENIED);
	    				return payload;
	    			}
	    		}
	    		
	    		// update users based on username for security and to retrieve passwords and so on from db
	    		List<User> validatorsUpdated = new ArrayList<User>();
	    		for(User u : socketDto.getEntry().getValidators()) validatorsUpdated.add( us.findByUsername(u.getUsername()) );
	    		LexiconOfrlex4 lo4 = socketDto.getEntry(); lo4.setValidators(validatorsUpdated); socketDto.setEntry(lo4);

	    		ofrlexService.update(socketDto.getEntry(), auth.getName(), socketDto.getEntry().getModifiedField());
	    		payload.setType(OfrlexPayload.Type.UPDATE_SUCCESS);
	    		List<LexiconOfrlexDto> entriesWithVariants = new ArrayList<LexiconOfrlexDto>();
	    		
	    		List<LexiconOfrlex4> entries = ofrlexService.getEntries(socketDto.getIds());

	    		for(LexiconOfrlex4 e : entries) {
	    			LexiconOfrlexDto dto = new LexiconOfrlexDto();
	    			dto.setOfrlexValues(e);
	    			entriesWithVariants.add(dto);
	    		}
	    		
	    		payload.setEntries(entriesWithVariants);
	    	}
	    	return payload;
	    }
	    
	    
	    @MessageMapping("/ofrlex.delete")
	    @SendTo("/topic/ofrlex")
	    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
	    public OfrlexPayload deleteEntries(List<LexiconOfrlex4> entries, SimpMessageHeaderAccessor headerAccessor,
	    		Authentication auth) throws JsonProcessingException {
	    	OfrlexPayload payload = new OfrlexPayload();
	    	payload.setSender(auth.getName());
	    	if(dbss.getStatus("ofrlex").isLocked()) {
	    		payload.setType(OfrlexPayload.Type.DELETE_LOCKED);
	    	}else {
	    		ofrlexService.removeMultiple(entries, auth.getName());
	    		payload.setType(OfrlexPayload.Type.DELETE_SUCCESS);
	    		List<LexiconOfrlexDto> entriesWithVariants = new ArrayList<LexiconOfrlexDto>();
	    		for(LexiconOfrlex4 e : entries) {
	    			LexiconOfrlexDto dto = new LexiconOfrlexDto();
	    			dto.setOfrlexValues(e);
	    			entriesWithVariants.add(dto);
	    		}
//	        	for(LexiconOfrlex4 dto : entries) entriesWithVariants.add(variantService.computeVariants(dto)); // commented for computation saving
	    		payload.setEntries(entriesWithVariants);
	    	}
	    	return payload;
	    }
	    
	    @MessageMapping("/ofrlex.startcampaign")
	    @SendTo("/topic/ofrlex")
	    @PreAuthorize("hasAuthority('ADMIN_USER')")
	    public OfrlexPayload startValidationCampaign(List<LexiconOfrlex4> entries, SimpMessageHeaderAccessor headerAccessor, Authentication auth) {
	    	System.out.println("hello validation campaign");
	    	LOG.info("{} has called startcampaign", auth.getName());
	    	OfrlexPayload payload = new OfrlexPayload();
	    	payload.setSender(auth.getName());
	    	ValidationCampaign vc = vcs.startCampaign(auth.getName(), entries);
//	    	for (LexiconOfrlex4 entry : entries) entry.setValidationCampaign(vc);
	    	DBState dbs = dbss.startCampaign(vc, "ofrlex");
	    	payload.setDbs(dbs);
	    	payload.setType(OfrlexPayload.Type.CAMPAIGN_STARTED);
	    	payload.setVc(vc);
	    	return payload;
	    }
	    
	    
	    @MessageMapping("/ofrlex.viewcampaign")
	    @SendTo("/topic/ofrlex")
	    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
	    public OfrlexPayload viewValidationCampaign(List<LexiconOfrlex4> entries, SimpMessageHeaderAccessor headerAccessor, Authentication auth) {
	    	System.out.println("view validation campaign");
	    	LOG.info("{} called viewcampaign", auth.getName());
	    	OfrlexPayload payload = new OfrlexPayload();
	    	payload.setSender(auth.getName());
	    	ValidationCampaign vc = vcs.getCampaign();
	    	payload.setVc(vc);
	    	payload.setType(OfrlexPayload.Type.CAMPAIGN_VIEW);
	    	return payload;
	    }
	    
	    @MessageMapping("/ofrlex.endcampaign")
	    @SendTo("/topic/ofrlex")
	    @PreAuthorize("hasAuthority('ADMIN_USER')")
	    public OfrlexPayload endValidationCampaign( SimpMessageHeaderAccessor headerAccessor, Authentication auth) {
	    	System.out.println(String.format("%s has called endcampaign", auth.getName()));
	    	LOG.info("{} has called endcampaign", auth.getName());
	    	OfrlexPayload payload = new OfrlexPayload();
	    	payload.setSender(auth.getName());
	    	ValidationCampaign vc = vcs.getCampaign();
	    	DBState dbs = dbss.endCampaign("ofrlex"); // end the current campaign
	    	payload.setDbs(dbs);
	    	payload.setType(OfrlexPayload.Type.CAMPAIGN_ENDED);
	    	payload.setVc(vc);
	    	return payload;
	    }
	    
	    @MessageMapping("/ofrlex.notify")
	    @SendTo("/topic/ofrlex")
	    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
	    public OfrlexPayload notify(@Payload OfrlexPayload payload, Authentication auth) {
	    	payload.setType(OfrlexPayload.Type.NOTIFY_OVERRIDED);
	    	payload.setSender(auth.getName());
	    	return payload;
	    }
	    
	
	
}
