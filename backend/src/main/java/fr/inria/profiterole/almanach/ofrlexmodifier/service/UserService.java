package fr.inria.profiterole.almanach.ofrlexmodifier.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.Role;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.User;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.dto.TempUser;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.RoleRepository;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.UserRepository;
import fr.inria.profiterole.almanach.ofrlexmodifier.utils.Tools;

@Service
public class UserService {
	
	@Autowired
    UserRepository userRepository;
	
	@Autowired
	RoleRepository rr;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	
	private static String urlDeploy = "https://profiterole-almanach-ui.paris.inria.fr:8888"; // "https://localhost:8888"
//	private static String urlDeploy = "https://profiterole-almanach-ui:8888";
//	private static String urlDeploy = "https://localhost:8888";
	
	@PostConstruct
    public void init() {
		if(rr.count() < 1) {
			Role r = new Role();
	    	r.setRoleName("STANDARD_USER"); r.setDescription("Standard User - Has no admin rights");
	    	rr.save(r);
	    	Role admin = new Role();
	    	admin.setRoleName("ADMIN_USER"); admin.setDescription("Admin User - Has permission to perform admin tasks");
	    	rr.save(admin);
		}
		if(!userRepository.existsById("admin")) {
			User admin = new User();
			admin.setUsername("admin"); admin.setPassword(passwordEncoder.encode("admin"));
			admin.setStatus("Administrator"); admin.setAvatar(String.format("%s/downloadFile/admin-avatar.jpg", urlDeploy) );
			admin.setRoles(Arrays.asList( rr.findByRoleName("ADMIN_USER")));
			userRepository.save(admin);
		}
	}

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public List<User> findAllUsers() {
        return (List<User>)userRepository.findAll();
    }
    
    public User creaUser(String username, String pwd) {
    	User user = new User();
    	user.setUsername(username);
    	user.setPassword(passwordEncoder.encode(pwd));
    	user.setRoles(Arrays.asList(rr.findByRoleName("STANDARD_USER")));
    	User savedUser = userRepository.save(user);
    	return savedUser;
    }
    
    public User creaUser(TempUser tempUser) throws IOException {
    	List<String> roleNames = tempUser.getRoles();
    	User user = new User();
    	user.setAge(tempUser.getAge()); user.setEmail(tempUser.getEmail()); user.setFirstName(tempUser.getFirstName()); user.setLastName(tempUser.getLastName()); 
    	user.setGender(tempUser.getGender()); user.setOrganization(tempUser.getOrganization()); user.setStatus(tempUser.getStatus()); user.setUsername(tempUser.getUsername());
    	user.setPassword(passwordEncoder.encode( tempUser.getPassword() ));
//    	user.setAvatar(String.format("https://localhost:8098/downloadFile/%s.jpg", String.valueOf(Tools.randInt(0, 5))));
    	
    	File source = new File(String.format("./uploads/%s.jpg", String.valueOf(Tools.randInt(0, 5))));
    	File destination = new File(String.format("./uploads/%s-avatar.jpg", tempUser.getUsername()));
    	FileUtils.copyFile(source, destination);
    	user.setAvatar(String.format("%s/downloadFile/%s-avatar.jpg", urlDeploy, tempUser.getUsername()));
    	
    	List<Role> roles = new ArrayList<Role>();
    	for(String name : roleNames) 
    		if(rr.findByRoleName(name) != null) roles.add( rr.findByRoleName(name) );
    	user.setRoles(roles);
    	
    	return userRepository.save(user);
    }
    
    public User updateInfos(User user) {
    	User userDB = userRepository.findByUsername(user.getUsername());
    	userDB.setAge(user.getAge());
    	userDB.setEmail(user.getEmail());
    	userDB.setFirstName(user.getFirstName());
    	userDB.setLastName(user.getLastName());
    	userDB.setGender(user.getGender());
    	userDB.setOrganization(user.getOrganization());
    	userDB.setStatus(user.getStatus());
    	return userRepository.save(userDB);
    }
    
    public User updateInfos(TempUser user) {
    	User userDB = userRepository.findByUsername(user.getUsername());
    	userDB.setAge(user.getAge());
    	userDB.setEmail(user.getEmail());
    	userDB.setFirstName(user.getFirstName());
    	userDB.setLastName(user.getLastName());
    	userDB.setGender(user.getGender());
    	userDB.setOrganization(user.getOrganization());
    	userDB.setStatus(user.getStatus());
    	return userRepository.save(userDB);
    }
    
    /**
     * Change password based on username (id). New password is encrypted
     * @param username
     * @param password - String password 
     * @return
     */
    public User changePassword(String username, String password) {
    	User user = userRepository.findByUsername(username);
    	user.setPassword(passwordEncoder.encode( password ));
    	return userRepository.save(user);
    }
    
    /**
     * Change roles using the user id (username) and a list of role names. 
     * @param roleNames
     * @param username
     * @return
     */
    public User changeRoles(List<String> roleNames, String username) {
    	User user = userRepository.findByUsername(username);
    	List<Role> roles = new ArrayList<Role>();
    	for(String name : roleNames) 
    		if(rr.findByRoleName(name) != null) roles.add( rr.findByRoleName(name) );
    	user.setRoles(roles);
    	return userRepository.save(user);
    }
    
    public User changeAvatar(String path, String username) {
    	User user = userRepository.findByUsername(username);
    	user.setAvatar(path);
    	return userRepository.save(user);
    }
    
    public boolean exists(String username) {
    	return userRepository.existsById(username);
    }
    
    public boolean deleteUser(String username) {
    	userRepository.deleteById(username);
    	return true;
    }
    
    public boolean isAdmin(String username) {
    	List<Role> roles = userRepository.findByUsername(username).getRoles();
    	for(Role role : roles) if(role.getRoleName().equals("ADMIN_USER")) return true;
    	return false;
    }
    
    /**
     * for debugging
     * @return
     */
    public boolean checkGaelEmpty() {
    	User gael = this.findByUsername("gael");
    	boolean empty = false;
    	if(gael.getPassword().isEmpty() || gael.getPassword().length()<7) empty = true;
		if(empty) System.out.println("View ENtry gael empty pwd");
		return gael.getPassword().isEmpty();
    }

}
