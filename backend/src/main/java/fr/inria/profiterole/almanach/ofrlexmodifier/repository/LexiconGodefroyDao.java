package fr.inria.profiterole.almanach.ofrlexmodifier.repository;

import java.util.List;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconGodefroy;

public interface LexiconGodefroyDao {
	List<LexiconGodefroy> findByForm( String form );
}
