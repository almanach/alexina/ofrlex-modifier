package fr.inria.profiterole.almanach.ofrlexmodifier.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;

public class ShellTools {

	
	private static final Logger log = LoggerFactory.getLogger(ShellTools.class);
	
    public static void main(String[] args) {

//        String res = generateIlex();
//        if(res != null) Tools.ecrire("VERB.ilex", res);//System.out.println(res);
//        else System.out.println("null");

    }
    
    public static String generateIlex(String cat) {
    	ProcessBuilder processBuilder = new ProcessBuilder();
    	
    	String command = String.format("cd creaIlex ; cat ofrlex_%s.tsv | perl create_ilex2.pl", cat);
        processBuilder.command("bash", "-c", command);

//        processBuilder.command("bash", "-c", "cd /Users/gguibon/eclipse-workspace/ofrlexexceltoilex ; perl -w test.pl temp.ilex"); //linux
//    	System.out.println("cd /Users/gguibon/eclipse-workspace/ofrlexexceltoilex ; cat temp | perl -w create_ilex.pl -l \"/Users/gguibon/eclipse-workspace/lefff/{v_new,prep,nom,adj,adv}.ilex\"");
//        processBuilder.command("bash", "-c", "cd /Users/gguibon/eclipse-workspace/ofrlexexceltoilex ; cat temp | perl create_ilex2.pl");
//        processBuilder.command("cmd.exe", "/c", "ping -n 3 google.com"); //windows

        try {

            Process process = processBuilder.start();

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            List<String> linesRes = new ArrayList<String>();
            
            String line;
            while ((line = reader.readLine()) != null) {
            	linesRes.add(line);
            }
            int exitCode = process.waitFor();
            log.info("shell: {}",exitCode);
            return Joiner.on("\n").join(linesRes);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) { e.printStackTrace(); }
		return null;
    }
    
    public static void test() {
    	ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("bash", "-c", "cd /Users/gguibon/eclipse-workspace/ofrlexexceltoilex ; ls "); //linux
//        processBuilder.command("cmd.exe", "/c", "ping -n 3 google.com"); //windows

        try {

            Process process = processBuilder.start();

            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

            int exitCode = process.waitFor();
            System.out.println("\nExited with error code : " + exitCode);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}

