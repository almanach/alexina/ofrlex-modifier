package fr.inria.profiterole.almanach.ofrlexmodifier.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="frolex")
public class LexiconFrolex {

	@Id
	@GenericGenerator(name = "UseIdOrGenerate", strategy = "fr.inria.profiterole.almanach.ofrlexmodifier.repository.UseIdOrGenerate")
	@GeneratedValue(generator = "UseIdOrGenerate")
	@Column(unique = true, nullable = false)
	private Long id;
	private String form;
	private String bfmFreq;
	private String msdAfrlex;
	private String msdBFM;
	private String msdCattexConv;
	private String lemma;
	private String lemmaSource;
	private String comment;
	
	public LexiconFrolex() {}
	
	
	public LexiconFrolex(Long id, String form, String bfmFreq, String msdAfrlex, String msdBFM, String msdCattexConv,
			String lemma, String lemmaSource, String comment) {
		super();
		this.id = id;
		this.form = form;
		this.bfmFreq = bfmFreq;
		this.msdAfrlex = msdAfrlex;
		this.msdBFM = msdBFM;
		this.msdCattexConv = msdCattexConv;
		this.lemma = lemma;
		this.lemmaSource = lemmaSource;
		this.comment = comment;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getForm() {
		return form;
	}
	public void setForm(String form) {
		this.form = form;
	}
	public String getBfmFreq() {
		return bfmFreq;
	}
	public void setBfmFreq(String bfmFreq) {
		this.bfmFreq = bfmFreq;
	}
	public String getMsdAfrlex() {
		return msdAfrlex;
	}
	public void setMsdAfrlex(String msdAfrlex) {
		this.msdAfrlex = msdAfrlex;
	}
	public String getMsdBFM() {
		return msdBFM;
	}
	public void setMsdBFM(String msdBFM) {
		this.msdBFM = msdBFM;
	}
	public String getMsdCattexConv() {
		return msdCattexConv;
	}
	public void setMsdCattexConv(String msdCattexConv) {
		this.msdCattexConv = msdCattexConv;
	}
	public String getLemma() {
		return lemma;
	}
	public void setLemma(String lemma) {
		this.lemma = lemma;
	}
	public String getLemmaSource() {
		return lemmaSource;
	}
	public void setLemmaSource(String lemmaSource) {
		this.lemmaSource = lemmaSource;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
	@Override
	public String toString() {
		return "LexiconFrolex [id=" + id + ", form=" + form + ", bfmFreq=" + bfmFreq + ", msdAfrlex=" + msdAfrlex
				+ ", msdBFM=" + msdBFM + ", msdCattexConv=" + msdCattexConv + ", lemma=" + lemma + ", lemmaSource="
				+ lemmaSource + ", comment=" + comment + "]";
	}
	
	
	
}
