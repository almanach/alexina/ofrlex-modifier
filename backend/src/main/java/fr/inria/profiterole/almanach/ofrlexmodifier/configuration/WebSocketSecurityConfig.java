package fr.inria.profiterole.almanach.ofrlexmodifier.configuration;

import static org.springframework.messaging.simp.SimpMessageType.CONNECT;
import static org.springframework.messaging.simp.SimpMessageType.DISCONNECT;
import static org.springframework.messaging.simp.SimpMessageType.HEARTBEAT;
import static org.springframework.messaging.simp.SimpMessageType.UNSUBSCRIBE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.support.ChannelInterceptorAdapter;
import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry;
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;
import org.springframework.security.messaging.web.csrf.CsrfChannelInterceptor;

//import org.springframework.security.messaging.web.csrf.CsrfChannelInterceptor;
//import org.springframework.security.messaging.web.socket.server.CsrfTokenHandshakeInterceptor;

@Configuration
public class WebSocketSecurityConfig extends AbstractSecurityWebSocketMessageBrokerConfigurer {

//	@Autowired
//    private TokenSecurityChannelInterceptor tokenSecurityChannelInterceptor;


	
  @Override
  protected void configureInbound(MessageSecurityMetadataSourceRegistry messages) {
    messages.simpTypeMatchers(CONNECT, UNSUBSCRIBE, DISCONNECT, HEARTBEAT).permitAll()
    .simpDestMatchers("/app/**", "/topic/**").authenticated().simpSubscribeDestMatchers("/topic/**").authenticated()
        .anyMessage().denyAll();
  }

  @Override
  protected boolean sameOriginDisabled() {
    return true;
  }
  
  @Bean
  public CsrfChannelInterceptor csrfChannelInterceptor() {
      return new CsrfChannelInterceptor();
  }
  
//  @Bean
//  public ChannelInterceptorAdapter securityContextChannelInterceptor() {
//      return tokenSecurityChannelInterceptor;
//  }
}
