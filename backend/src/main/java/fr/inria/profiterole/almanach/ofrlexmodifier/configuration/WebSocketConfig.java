package fr.inria.profiterole.almanach.ofrlexmodifier.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;


@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
//	public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {
	
	private static final Logger LOG = LoggerFactory.getLogger(WebSocketConfig.class);
	
//	@Autowired
//	private TokenStore tokenStore;
	
//	@Autowired
//	private CustomHttpSessionHandshakeInterceptor customHttpSessionHandshakeInterceptor;
	
	@Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint("/ws").setAllowedOrigins("*").withSockJS();
//        registry.addEndpoint("/ws")
////        .addInterceptors(customHttpSessionHandshakeInterceptor)
////        .setHandshakeHandler(new DefaultHandshakeHandler() {
////            @Override
////            protected Principal determineUser(ServerHttpRequest request, WebSocketHandler wsHandler, Map<String, Object> attributes) {
////                Principal principal = request.getPrincipal();
////                LOG.info("YOOOOOOOOO");
//////                if (principal == null) {
//////                	log.debug("Anonymous access");
//////                    Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
//////                    authorities.add(new SimpleGrantedAuthority(AuthoritiesConstants.ANONYMOUS));
//////                    principal = new AnonymousAuthenticationToken("WebsocketConfiguration", "anonymous", authorities);
//////                }
////                return principal;
////            }
////        })
//        .setAllowedOrigins("*").withSockJS();//.setInterceptors(customHttpSessionHandshakeInterceptor);    	    
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes("/app");
        registry.enableSimpleBroker("/topic");
//        registry.enableSimpleBroker("/topic", "/queue", "/user");   // Enables a simple in-memory broker
//        registry.setUserDestinationPrefix("/user");  
    }
    
    
//    @Override
//    public void configureClientInboundChannel(ChannelRegistration registration) {
////    	registration.interceptors(new FilterChannelInterceptor());
//        registration.interceptors(new ChannelInterceptor() {
//        	
//            @Override
//            public Message<?> preSend(Message<?> message, MessageChannel channel) {
//            	LOG.debug("???hhh");
//            	System.out.println("HEEEEY");
//                StompHeaderAccessor accessor =  MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
//                System.out.println("toKEN" + accessor.getFirstNativeHeader("access_token"));
//                if (StompCommand.CONNECT.equals(accessor.getCommand())) {
//                    Authentication user = tokenStore.readAuthentication(accessor.getFirstNativeHeader("access_token")) ; // access authentication header(s)
//                    accessor.setUser(user);
//                }
////            	Message<?> newMessage = MessageBuilder.createMessage(modifyMessage(message),  accessor.getMessageHeaders());
//                return message;
//            }
//        });
//    }
    
    
//    @SuppressWarnings("deprecation")
//	@Override
//	public void configureClientInboundChannel(ChannelRegistration registration) {
//		registration.setInterceptors(new ChannelInterceptorAdapter() {
//
//			@Override
//			public Message<?> preSend(Message<?> message, MessageChannel channel) {
//				System.out.println("Oiii");
//				StompHeaderAccessor accessor =
//						MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
//
//				if (StompCommand.CONNECT.equals(accessor.getCommand())) {
//					String user = accessor.getFirstNativeHeader("user");
//					if (!StringUtils.isEmpty(user)) {
//						List<GrantedAuthority> authorities = new ArrayList<>();
//						authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
//						Authentication auth = new UsernamePasswordAuthenticationToken(user, user, authorities);
//						SecurityContextHolder.getContext().setAuthentication(auth);
//						accessor.setUser(auth);
//					}
//				}
//
//				return message;
//			}
//		});
//	}
    
//    protected void configureInbound(MessageSecurityMetadataSourceRegistry messages) {
//        messages.simpTypeMatchers(
//                        SimpMessageType.CONNECT,
//                        SimpMessageType.MESSAGE,
//                        SimpMessageType.SUBSCRIBE).authenticated()
//                .simpTypeMatchers(
//                        SimpMessageType.UNSUBSCRIBE,
//                        SimpMessageType.DISCONNECT).permitAll()
//                //.nullDestMatcher().authenticated()
//                .anyMessage().denyAll();
//    }
    
    
//    @SuppressWarnings("deprecation")
//	@Override
//	public void configureClientInboundChannel(ChannelRegistration registration) {
//		registration.setInterceptors(new ChannelInterceptorAdapter() {
//
//			@Override
//			public Message<?> preSend(Message<?> message, MessageChannel channel) {
//
//				StompHeaderAccessor accessor =
//						MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
//
//				if (StompCommand.CONNECT.equals(accessor.getCommand())) {
//					String user = accessor.getFirstNativeHeader("user");
//					if (!StringUtils.isEmpty(user)) {
//						List<GrantedAuthority> authorities = new ArrayList<>();
//						authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
//						Authentication auth = new UsernamePasswordAuthenticationToken(user, user, authorities);
//						SecurityContextHolder.getContext().setAuthentication(auth);
//						accessor.setUser(auth);
//						sr.registerNewSession(user, auth);
//					}
//				}
//
//				return message;
//			}
//		});
//	}
    
    
//    @Bean
//    public CustomHttpSessionHandshakeInterceptor 
//          customHttpSessionHandshakeInterceptor() {
//          return new CustomHttpSessionHandshakeInterceptor();
//
//    }
    
   
}
