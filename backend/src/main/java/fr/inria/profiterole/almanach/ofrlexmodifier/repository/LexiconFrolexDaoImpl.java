package fr.inria.profiterole.almanach.ofrlexmodifier.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconFrolex;

@Transactional
@Repository
public class LexiconFrolexDaoImpl implements LexiconFrolexDao{

	@Autowired
	JdbcTemplate jt;
	
	@Override
	public List<LexiconFrolex> findByForm(String form) {
		RowMapper<LexiconFrolex> rowMapper = new BeanPropertyRowMapper<LexiconFrolex>(LexiconFrolex.class);
		return jt.query("SELECT * FROM frolex WHERE form = ?", rowMapper, form);
	}

}
