package fr.inria.profiterole.almanach.ofrlexmodifier.configuration;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.google.common.base.Joiner;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SimpleCorsFilter implements Filter {

    public SimpleCorsFilter() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        HttpServletRequest request = (HttpServletRequest) req;
        
        System.out.println(Joiner.on(" ; ").join(Collections.list(request.getHeaderNames())) );
        System.out.println(String.format("Origin : %s", request.getHeader("Origin")));
//        host ; user-agent ; accept ; accept-language ; accept-encoding ; connection ; referer ; cache-control
//        Accept-Encoding	
//        gzip, deflate, br
//        Accept-Language	
//        fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3
//        Access-Control-Request-Headers	
//        authorization
//        Access-Control-Request-Method	
//        POST
//        Connection	
//        keep-alive
//        Host	
//        www.profiterole-almanach-ui:8888
//        Origin	
//        https://profiterole-almanach-ui.paris.inria.fr:8888
//        Referer	
//        https://profiterole-almanach-ui.paris.inria.fr:8888/
//        User-Agent	
//        Mozilla/5.0 (Macintosh; Intel …) Gecko/20100101 Firefox/69.0
        
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with, Authorization, Content-Type, Accept");
        
//        response.setHeader("Access-Control-Allow-Origin", "https://localhost:8080");
	    response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        
//	    response.setHeader("Access-Control-Allow-Origin", request.getHeader("referer"));
//	    if(!request.getHeader("referer").isEmpty())
	    
	        
        if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
        	System.out.println("OPTION");
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
        	System.out.println("NO OPTIONS");
            chain.doFilter(req, response);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }
}
