package fr.inria.profiterole.almanach.ofrlexmodifier.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.DBState;

@Repository
public interface DBStateRepository extends CrudRepository<DBState, String>{
	
}
