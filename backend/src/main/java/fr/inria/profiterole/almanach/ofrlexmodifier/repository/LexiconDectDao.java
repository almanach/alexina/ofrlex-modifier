package fr.inria.profiterole.almanach.ofrlexmodifier.repository;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconDect;

public interface LexiconDectDao {
	LexiconDect findById( Long id );
}
