package fr.inria.profiterole.almanach.ofrlexmodifier.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconOfrlex4;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.ValidationCampaign;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.ValidationCampaignRepository;

@Service
public class ValidationCampaignService {
	
	private static final Logger LOG = LoggerFactory.getLogger(ValidationCampaignService.class);
	
	@Autowired
	ValidationCampaignRepository vcr;
	
	@Autowired
	DBStateService dbss;
	
	
	/**
	 * Starts a validation campaign for ofrlex entries
	 * @param username
	 * @param entries
	 * @return the created validation campaign from database
	 */
	public ValidationCampaign startCampaign(String username, List<LexiconOfrlex4> entries) {
		vcr.deleteAll();
		ValidationCampaign vc = new ValidationCampaign();
		vc.setAuthor(username);
		vc.setStartDate(new Date());
		vc.setTargets(entries);
//		for (LexiconOfrlex4 entry : entries) entry.setValidationCampaign(vc);
		ValidationCampaign vcSaved = vcr.save(vc);		
		return vcSaved;
	}
	
	/**
	 * Get the current validation campaign (supposed to be only one)
	 * @return a ValidationCampaign instance
	 */
	public ValidationCampaign getCampaign() {
		List<ValidationCampaign> vcList = vcr.findAll();
		ValidationCampaign vc = new ValidationCampaign();
		if(!vcList.isEmpty()) vc = vcList.get(0);
		return vc;
	}
	
	/**
	 * Remomve the uniq validation campaign (don't forget to update DBstate)
	 */
	public void removeCampaign() {
		List<ValidationCampaign> vcList = vcr.findAll();
		ValidationCampaign vc = new ValidationCampaign();
		if(!vcList.isEmpty()) vc = vcList.get(0);
		vcr.delete(vc);
	}
	
	
	/**
	 * Get the list of Long ids from the list of targeted entries by the current validation campaign
	 * @return List of Long
	 */
	public List<Long> getTargetIds() {
		List<ValidationCampaign> vcList = vcr.findAll();
		ValidationCampaign vc = new ValidationCampaign();
		if(!vcList.isEmpty()) vc = vcList.get(0);
		List<Long> ids = new ArrayList<Long>();
		for( LexiconOfrlex4 o : vc.getTargets() ) ids.add(o.getId());
		return ids;
	}
	
	
	
}
