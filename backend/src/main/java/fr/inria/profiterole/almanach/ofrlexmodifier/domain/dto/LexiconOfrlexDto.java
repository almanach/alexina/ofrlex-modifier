package fr.inria.profiterole.almanach.ofrlexmodifier.domain.dto;

import java.util.ArrayList;
import java.util.List;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconDect;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconFrolex;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconGodefroy;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconOfrlex4;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.Pseudosyn;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.User;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.ValidationCampaign;

public class LexiconOfrlexDto {

	private Long id;
	private String ofrlexHeadword;
	private String scat;
	private String tlhw;
	private String tllemma;
	private String tlhwms;
	private String tlref;
	private String tlhwscat;
	private String inflclass;
	private String headwordscat;
	private String cat;
	private String subCat;
	private String syntInfo;
	private String bfmfreq;
	private String glossFr;
	private String descendent;
	private String tlvariantOf;
	private String variantOf;
	private String tldescendent;
	private String pseudoGlosses;
	private String redistributions;
	private String manualCat;
	private String manualSubCat;
	private String manualVariantOf;
	private String manualSyntfeats;
	private String manualRedistributions;
	private String manualGlossFr;
	private String manualPseudoGlosses;
	private String manualInflclass;
	private String manualDescendent;
	
	private List<Pseudosyn> pseudosyns = new ArrayList<Pseudosyn>();
	
	// additionnal infos for payload
	private LexiconDect vDect ;
	private List<LexiconGodefroy> vGodefroyList = new ArrayList<LexiconGodefroy>() ;
	private List<LexiconFrolex> vFrolexList = new ArrayList<LexiconFrolex>();
	
	private String editor = "";
	private String modifiedField = "";
	private ValidationCampaign validationCampaign;
	private List<User> validators = new ArrayList<User>();
	private boolean validated = false;
	
	
	public LexiconOfrlexDto() {}	


	public LexiconOfrlexDto(Long id, String ofrlexHeadword, String scat, String tlhw, String tllemma, String tlhwms, String tlref,
			String tlhwscat, String inflclass, String headwordscat, String cat, String subCat, String syntInfo,
			String bfmfreq, String glossFr, String descendent, String tlvariantOf, String variantOf,
			String tldescendent, String pseudoGlosses, String redistributions, String manualCat, String manualSubCat,
			String manualVariantOf, String manualSyntfeats, String manualRedistributions, String manualGlossFr,
			String manualPseudoGlosses, String manualInflclass, String manualDescendent, LexiconDect vDect, List<Pseudosyn> pseudosyns,
			List<LexiconGodefroy> vGodefroyList, List<LexiconFrolex> vFrolexList, String editor, 
			String modifiedField, ValidationCampaign validationCampaign, 
			List<User> validators,
			boolean validated) {
		super();
		this.id = id;
		this.ofrlexHeadword = ofrlexHeadword;
		this.scat = scat;
		this.tlhw = tlhw;
		this.tllemma = tllemma;
		this.tlhwms = tlhwms;
		this.tlref = tlref;
		this.tlhwscat = tlhwscat;
		this.inflclass = inflclass;
		this.headwordscat = headwordscat;
		this.cat = cat;
		this.subCat = subCat;
		this.syntInfo = syntInfo;
		this.bfmfreq = bfmfreq;
		this.glossFr = glossFr;
		this.descendent = descendent;
		this.tlvariantOf = tlvariantOf;
		this.variantOf = variantOf;
		this.tldescendent = tldescendent;
		this.pseudoGlosses = pseudoGlosses;
		this.redistributions = redistributions;
		this.manualCat = manualCat;
		this.manualSubCat = manualSubCat;
		this.manualVariantOf = manualVariantOf;
		this.manualSyntfeats = manualSyntfeats;
		this.manualRedistributions = manualRedistributions;
		this.manualGlossFr = manualGlossFr;
		this.manualPseudoGlosses = manualPseudoGlosses;
		this.manualInflclass = manualInflclass;
		this.manualDescendent = manualDescendent;
		this.pseudosyns = pseudosyns;
		this.vDect = vDect;
		this.vGodefroyList = vGodefroyList;
		this.vFrolexList = vFrolexList;
		this.editor = editor;
		this.modifiedField = modifiedField;
		this.validationCampaign = validationCampaign;
		this.validators = validators;
		this.validated = validated;
	}


	public void setOfrlexValues(LexiconOfrlex4 o) {
		this.id = o.getId();
		this.ofrlexHeadword = o.getOfrlexHeadword();
		this.headwordscat = o.getScat();
		this.tlhw = o.getTlhw();
		this.tllemma = o.getTllemma();
		this.tlhwms = o.getTlhwms();
		this.tlref = o.getTlref();
		this.tlhwscat = o.getTlhwscat();
		this.inflclass = o.getInflclass();
		this.headwordscat = o.getHeadwordscat();
		this.cat = o.getCat();
		this.subCat = o.getSubCat();
		this.syntInfo = o.getSyntInfo();
		this.bfmfreq = o.getBfmfreq();
		this.glossFr = o.getGlossFr();
		this.descendent = o.getDescendent();
		this.tlvariantOf = o.getTlvariantOf();
		this.variantOf = o.getVariantOf();
		this.tldescendent = o.getTldescendent();
		this.pseudoGlosses = o.getPseudoGlosses();
		this.redistributions = o.getRedistributions();
		this.manualCat = o.getManualCat();
		this.manualSyntfeats = o.getManualSyntfeats();
		this.manualRedistributions = o.getManualRedistributions();
		this.manualVariantOf = o.getManualVariantOf();
		this.manualSubCat = o.getManualSubCat();
		this.manualGlossFr = o.getManualGlossFr();
		this.manualPseudoGlosses = o.getManualPseudoGlosses();
		this.manualInflclass = o.getManualInflclass();
		this.manualDescendent = o.getManualDescendent();
		this.pseudosyns = o.getPseudosyns();
		this.editor = o.getEditor();
		this.modifiedField = o.getModifiedField();
//		this.validationCampaign = o.getValidationCampaign();
		this.validators = o.getValidators();
		this.validated = o.isValidated();
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOfrlexHeadword() {
		return ofrlexHeadword;
	}

	public void setOfrlexHeadword(String ofrlexHeadword) {
		this.ofrlexHeadword = ofrlexHeadword;
	}
	
	public String getScat() {
		return scat;
	}

	public void setScat(String scat) {
		this.scat = scat;
	}

	public String getTlhw() {
		return tlhw;
	}

	public void setTlhw(String tlhw) {
		this.tlhw = tlhw;
	}

	public String getTllemma() {
		return tllemma;
	}

	public void setTllemma(String tllemma) {
		this.tllemma = tllemma;
	}

	public String getTlhwms() {
		return tlhwms;
	}

	public void setTlhwms(String tlhwms) {
		this.tlhwms = tlhwms;
	}

	public String getTlref() {
		return tlref;
	}

	public void setTlref(String tlref) {
		this.tlref = tlref;
	}

	public String getTlhwscat() {
		return tlhwscat;
	}

	public void setTlhwscat(String tlhwscat) {
		this.tlhwscat = tlhwscat;
	}

	public String getInflclass() {
		return inflclass;
	}

	public void setInflclass(String inflclass) {
		this.inflclass = inflclass;
	}

	public String getHeadwordscat() {
		return headwordscat;
	}

	public void setHeadwordscat(String headwordscat) {
		this.headwordscat = headwordscat;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public String getSubCat() {
		return subCat;
	}

	public void setSubCat(String subCat) {
		this.subCat = subCat;
	}

	public String getSyntInfo() {
		return syntInfo;
	}

	public void setSyntInfo(String syntInfo) {
		this.syntInfo = syntInfo;
	}

	public String getBfmfreq() {
		return bfmfreq;
	}

	public void setBfmfreq(String bfmfreq) {
		this.bfmfreq = bfmfreq;
	}

	public String getGlossFr() {
		return glossFr;
	}

	public void setGlossFr(String glossFr) {
		this.glossFr = glossFr;
	}

	public String getDescendent() {
		return descendent;
	}

	public void setDescendent(String descendent) {
		this.descendent = descendent;
	}

	public String getTlvariantOf() {
		return tlvariantOf;
	}

	public void setTlvariantOf(String tlvariantOf) {
		this.tlvariantOf = tlvariantOf;
	}

	public String getVariantOf() {
		return variantOf;
	}

	public void setVariantOf(String variantOf) {
		this.variantOf = variantOf;
	}

	public String getTldescendent() {
		return tldescendent;
	}

	public void setTldescendent(String tldescendent) {
		this.tldescendent = tldescendent;
	}

	public String getPseudoGlosses() {
		return pseudoGlosses;
	}

	public void setPseudoGlosses(String pseudoGlosses) {
		this.pseudoGlosses = pseudoGlosses;
	}

	public String getRedistributions() {
		return redistributions;
	}

	public void setRedistributions(String redistributions) {
		this.redistributions = redistributions;
	}

	public String getManualSyntfeats() {
		return manualSyntfeats;
	}

	public void setManualSyntfeats(String manualSyntfeats) {
		this.manualSyntfeats = manualSyntfeats;
	}

	public String getManualRedistributions() {
		return manualRedistributions;
	}

	public void setManualRedistributions(String manualRedistributions) {
		this.manualRedistributions = manualRedistributions;
	}

	
	
	public String getManualCat() {
		return manualCat;
	}

	public void setManualCat(String manualCat) {
		this.manualCat = manualCat;
	}

	public String getManualSubCat() {
		return manualSubCat;
	}


	public void setManualSubCat(String manualSubCat) {
		this.manualSubCat = manualSubCat;
	}


	public List<Pseudosyn> getPseudosyns() {
		return pseudosyns;
	}


	public void setPseudosyns(List<Pseudosyn> pseudosyns) {
		this.pseudosyns = pseudosyns;
	}


	public LexiconDect getvDect() {
		return vDect;
	}

	public void setvDect(LexiconDect vDectList) {
		this.vDect = vDectList;
	}

	public List<LexiconGodefroy> getvGodefroyList() {
		return vGodefroyList;
	}

	public void setvGodefroyList(List<LexiconGodefroy> vGodefroyList) {
		this.vGodefroyList = vGodefroyList;
	}


	public String getManualVariantOf() {
		return manualVariantOf;
	}


	public void setManualVariantOf(String manualVariantOf) {
		this.manualVariantOf = manualVariantOf;
	}

	public List<LexiconFrolex> getvFrolexList() {
		return vFrolexList;
	}

	public void setvFrolexList(List<LexiconFrolex> vFrolexList) {
		this.vFrolexList = vFrolexList;
	}


	public String getManualGlossFr() {
		return manualGlossFr;
	}


	public void setManualGlossFr(String manualGlossFr) {
		this.manualGlossFr = manualGlossFr;
	}


	public String getManualPseudoGlosses() {
		return manualPseudoGlosses;
	}


	public void setManualPseudoGlosses(String manualPseudoGlosses) {
		this.manualPseudoGlosses = manualPseudoGlosses;
	}


	public String getManualInflclass() {
		return manualInflclass;
	}


	public void setManualInflclass(String manualInflclass) {
		this.manualInflclass = manualInflclass;
	}


	public String getManualDescendent() {
		return manualDescendent;
	}


	public void setManualDescendent(String manualDescendent) {
		this.manualDescendent = manualDescendent;
	}
	
	
	public String getEditor() {
		return editor;
	}


	public void setEditor(String editor) {
		this.editor = editor;
	}


	public String getModifiedField() {
		return modifiedField;
	}


	public void setModifiedField(String modifiedField) {
		this.modifiedField = modifiedField;
	}


	public ValidationCampaign getValidationCampaign() {
		return validationCampaign;
	}


	public void setValidationCampaign(ValidationCampaign validationCampaign) {
		this.validationCampaign = validationCampaign;
	}


	public List<User> getValidators() {
		return validators;
	}


	public void setValidators(List<User> validators) {
		this.validators = validators;
	}


	public boolean isValidated() {
		return validated;
	}


	public void setValidated(boolean validated) {
		this.validated = validated;
	}


	@Override
	public String toString() {
		return "LexiconOfrlexDto [id=" + id + ", ofrlexHeadword=" + ofrlexHeadword + ", scat=" + scat + ", tlhw=" + tlhw
				+ ", tllemma=" + tllemma + ", tlhwms=" + tlhwms + ", tlref=" + tlref + ", tlhwscat=" + tlhwscat
				+ ", inflclass=" + inflclass + ", headwordscat=" + headwordscat + ", cat=" + cat + ", subCat=" + subCat
				+ ", syntInfo=" + syntInfo + ", bfmfreq=" + bfmfreq + ", glossFr=" + glossFr + ", descendent="
				+ descendent + ", tlvariantOf=" + tlvariantOf + ", variantOf=" + variantOf + ", tldescendent="
				+ tldescendent + ", pseudoGlosses=" + pseudoGlosses + ", redistributions=" + redistributions
				+ ", manualCat=" + manualCat + ", manualSubCat=" + manualSubCat + ", manualVariantOf=" + manualVariantOf
				+ ", manualSyntfeats=" + manualSyntfeats + ", manualRedistributions=" + manualRedistributions
				+ ", manualGlossFr=" + manualGlossFr + ", manualPseudoGlosses=" + manualPseudoGlosses
				+ ", manualInflclass=" + manualInflclass + ", manualDescendent=" + manualDescendent + ", pseudosyns="
				+ pseudosyns + ", vDect=" + vDect + ", vGodefroyList=" + vGodefroyList + ", vFrolexList=" + vFrolexList
				+ ", editor=" + editor + ", modifiedField=" + modifiedField + "]";
	}


	
}
