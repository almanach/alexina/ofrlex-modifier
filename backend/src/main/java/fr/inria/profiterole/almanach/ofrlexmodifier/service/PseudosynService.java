package fr.inria.profiterole.almanach.ofrlexmodifier.service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.inria.profiterole.almanach.ofrlexmodifier.domain.LexiconOfrlex4;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.Pseudosyn;
import fr.inria.profiterole.almanach.ofrlexmodifier.domain.dto.PseudosynSourceDto;
import fr.inria.profiterole.almanach.ofrlexmodifier.repository.PseudosynRepository;

@Service
public class PseudosynService {

	private static final Logger log = LoggerFactory.getLogger(PseudosynService.class);
	
	@Autowired
	PseudosynRepository pr;
	
	@Autowired
	LexiconOfrlex4Service os;
	
	private static final String JSON_SOURCE = "pseudosyns4java.json";
	
	@PostConstruct
    public void init() throws JsonParseException, JsonMappingException, IOException {

//		pr.deleteAll();
		
		if(pr.count() < 1) {
			String jsonStr = "";
			ClassPathResource cpr = new ClassPathResource(JSON_SOURCE);
			byte[] bdata = FileCopyUtils.copyToByteArray(cpr.getInputStream());
			jsonStr = new String(bdata, StandardCharsets.UTF_8);
	
			ObjectMapper mapper = new ObjectMapper();
			List<PseudosynSourceDto> pseudosyns;
			pseudosyns = Arrays.asList(mapper.readValue(jsonStr, PseudosynSourceDto[].class));
			log.info("Pseudosyns : {}", pseudosyns.get(0).toString());
	//		this.add(pseudosyns.get(0));
			for(PseudosynSourceDto psd : pseudosyns) this.add(psd);
		}
		
	}
	
	/**
	 * add a pseudosyn from pseudosynsource (for json initial mapping)
	 * @param psd
	 * @return
	 */
	public Pseudosyn add(PseudosynSourceDto psd) {
		Pseudosyn ps = new Pseudosyn();
		ps.setId(psd.getId()); ps.setDistance(psd.getDistance()); ps.setForm(psd.getForm()); ps.setPos(psd.getPos());
		try {
			List<LexiconOfrlex4> entries = os.getByHeadwordAndPos(psd.getSourceLexeme(), psd.getSourcePos());
			ps.setLexeme(entries.get(0));		
			return pr.save(ps);
		}catch(Exception e) { log.error("error for {} \n {}", psd.toString(), e.toString() ); }  
		return ps;
	}
	
	
}
