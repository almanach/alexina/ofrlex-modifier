// Configuration for your app
// https://quasar.dev/quasar-cli/quasar-conf-js

// ALTER TABLE ofrlex
// ADD validated BOOLEAN DEFAULT FALSE NOT NULL; 

// ALTER TABLE db_state
// ADD campaign_ongoing BOOLEAN DEFAULT FALSE NOT NULL; 

// const source = "profiterole-almanach-ui.paris.inria.fr:8888";
const source = "localhost:8888";

module.exports = function (ctx) {
  return {
    // app boot file (/src/boot)
    // --> boot files are part of "main.js"
    boot: [
      'i18n',
      'axios'
    ],

    css: [
      'app.styl'
    ],

    extras: [
      // 'ionicons-v4',
      // 'mdi-v3',
      'fontawesome-v5',
      // 'eva-icons',
      // 'themify',
      // 'roboto-font-latin-ext', // this or either 'roboto-font', NEVER both!

      'roboto-font', // optional, you are not bound to it
      'material-icons' // optional, you are not bound to it
    ],

    framework: {
      // iconSet: 'ionicons-v4',
      // lang: 'de', // Quasar language

      // all: true, // --- includes everything; for dev only!

      components: [
        'QLayout',
        'QHeader',
        'QDrawer',
        'QPageContainer',
        'QPage',
        'QToolbar',
        'QToolbarTitle',
        'QBtn', 'QBtnDropdown', 'QBtnGroup',
        'QIcon',
        'QList',
        'QItem',
        'QItemSection',
        'QItemLabel',
        'QSpace',
        'QBadge',
        'QTooltip',
        'QAvatar',
        'QCard', 'QCardSection', 'QCardActions', 'QSeparator',
        'QParallax',
        'QInput', 'QSelect', 'QField', 'QForm', 'QRadio', 'QCheckbox',
        'QAvatar', 'QChip',
        'QTable', 'QTr', 'QTd', 'QPopupEdit', 'QMenu', 'QToggle', 'QMarkupTable', 'QTh',
        'QBreadcrumbs',
        'QBreadcrumbsEl',
        'QDialog',
        'QUploader',
        'QBar', 'QBanner', 'QImg',
        'QTab', 'QTabs', 'QTabPanels', 'QTabPanel', 'QExpansionItem',
        'QFab', 'QFabAction', 'QBtnToggle', 'QLinearProgress',
        'QVirtualScroll'
      ],

      directives: [
        'Ripple', 'ClosePopup'
      ],

      // Quasar plugins
      plugins: [
        'Notify',
        'LoadingBar',
        'Loading',
        'AppVisibility',
        'AppFullscreen'
      ],
      config: {
        loadingBar: { 
          color: 'yellow',
          size: '6px',
          position: 'top',
          skipHijack: true
        },
        loading: { /* Loading defaults */ }
      }
    },

    supportIE: true,

    build: {
      scopeHoisting: true,
      // vueRouterMode: 'history',
      // vueCompiler: true,
      // gzip: true,
      // analyze: true,
      // extractCSS: false,
      extendWebpack (cfg) {
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /node_modules/,
          options: {
            formatter: require('eslint').CLIEngine.getFormatter('stylish')
          }
        })
      },
      distDir: 'target/dist'
    },

    // devServer: {
    //   // https: true,
    //   // port: 8080,
    //   open: true // opens browser window automatically
    // },

    // proxy all webpack dev-server requests starting with /api
    // to our Spring Boot backend (localhost:8088) using http-proxy-middleware
    // see https://cli.vuejs.org/config/#devserver-proxy
    devServer: {
      https: true,
      open: false,
      // port: 8081,
      proxy: {
        '/api': {
          target: source, // this configuration needs to correspond to the Spring Boot backends' application.properties server.port
          ws: true,
          changeOrigin: true
        },
        '/oauth': { target: source,
          ws: true,
          changeOrigin: true
        },
        '/ws': {
          target: source,
          ws: true,
          changeOrigin: true
        }
      }
    },

    // Change build paths to make them Maven compatible
    // see https://cli.vuejs.org/config/
    outputDir: 'target/dist',
    assetsDir: 'static',

    // animations: 'all', // --- includes all animations
    animations: [],

    ssr: {
      pwa: false
    },

    pwa: {
      // workboxPluginMode: 'InjectManifest',
      // workboxOptions: {}, // only for NON InjectManifest
      manifest: {
        // name: 'ofrlex-modifier',
        // short_name: 'ofrlex-modifier',
        // description: 'application for different lexicon manipulation tools',
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#027be3',
        icons: [
          {
            'src': 'statics/icons/profiterole-logo-128x128.png',
            'sizes': '128x128',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/profiterole-logo-192x192.png',
            'sizes': '192x192',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/profiterole-logo-256x256.png',
            'sizes': '256x256',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/profiterole-logo-384x384.png',
            'sizes': '384x384',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/profiterole-logo-512x512.png',
            'sizes': '512x512',
            'type': 'image/png'
          }
        ]
      }
    },

    cordova: {
      // id: 'fr.inria.profiterole.almanach.ofrlexmodifier',
      // noIosLegacyBuildFlag: true, // uncomment only if you know what you are doing
    },

    electron: {
      // bundler: 'builder', // or 'packager'

      extendWebpack (cfg) {
        // do something with Electron main process Webpack cfg
        // chainWebpack also available besides this extendWebpack
      },

      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options

        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Windows only
        // win32metadata: { ... }
      },

      builder: {
        // https://www.electron.build/configuration/configuration

        // appId: 'profiterole'
      }
    }
  }
}
