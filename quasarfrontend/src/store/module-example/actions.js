
export function login({commit}, {user, password}) {
    return new Promise((resolve, reject) => {
        api.getSecured(user, password)
            .then(response => {
                if(response.status == 200) {
                    console.log("Login successful: ", user);
                    // place the loginSuccess state into our vuex store
                    commit('login_success', {
                        userName: user,
                        userPass: password
                    });
                }
                resolve(response)
            })
            .catch(error => {
                console.log("Error: " + error);
                // place the loginError state into our vuex store
                commit('login_error', {
                    userName: user
                });
                reject("Invalid credentials!")
            })
    })
}

export function logout({commit}, {user}) {
    return new Promise((resolve, reject) => {
        console.log("logging out user: " + user);
        commit('logout_success');
        resolve({status: 'disconnected'});
    });
}

export function changeDesiredUrl({commit}, {value}) {
    state.desiredUrl = payload.value;
    commit('desiredurl_modified', {value: value});
}

export function accessFailed({commit}, {value}) {
    commit('access_failed');
}