export default {
  loginSuccess: false,
  loginError: false,
  userName: null,
  userPass: null,
  desiredUrl: null,
  failedAccess: false
}
