
export function login_success(state, payload){
    state.loginSuccess = true;
    state.userName = payload.userName;
    state.userPass = payload.userPass;
}

export function login_error(state, payload){
    state.loginError = true;
    state.userName = payload.userName;
}

export function logout_success(state){
    state.loginSuccess = false;
    state.userName = null;
    state.userPass = null;
}

export function desiredurl_modified(state, payload) {
    state.desiredUrl = payload.value;
}

export function access_failed(state, value){
    state.failedAccess = value;
}
