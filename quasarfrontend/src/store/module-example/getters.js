export function someGetter (/* state */) {
}


export function isLoggedIn(state){ return state.loginSuccess}
export function hasLoginErrored(state){ return state.loginError}
export function getUserName(state){ return state.userName}
export function getUserPass(state){ return state.userPass}
export function getDesiredUrl(state){ return state.desiredUrl}
export function getFailedAccess(state){ return state.failedAccess}
