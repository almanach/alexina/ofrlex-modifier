import Vue from 'vue'
import Vuex from 'vuex'
import api from '../boot/backend-api'


Vue.use(Vuex);

import VueCookies from 'vue-cookies';
VueCookies.config('7d');
Vue.use(VueCookies);

import SockJS from "sockjs-client";
import Stomp from "webstomp-client";

export default new Vuex.Store({
    state: {
        appVersion: "0.9.1 (20200910)",
        // appVersion: "0.9.0 (20200319)",
        // appVersion: "0.8.90 (20200203)",
        // appVersion: "0.8.88 (20191210)",
        // source: "https://profiterole-almanach-ui.paris.inria.fr:8888",
        source: "https://localhost:8888",
        loginSuccess: false,
        loginError: false,
        user: {  username: '',firstName: '',lastName: '', age: null, gender: '', email: '', organization: '', status: '', avatar: '', roles: [] },
        userName: null,
        userPass: null,
        desiredUrl: null,
        failedAccess: false,
        avatarKey: 0,
        connected: false,
        socket: null,
        stompClient: null,
        loggedUsers: [],
        validationCampaign: { id:null, author:null, targets: [], startDate: null, endDate:null  }
    },
    mutations: {
        change_source(state, payload){
            state.source = payload;
        },
        login_success(state, payload){
            state.loginSuccess = true;
            state.userName = payload.userName;
            // state.userPass = payload.userPass;
        },
        login_error(state, payload){
            state.loginError = true;
            state.userName = payload.userName;
        },
        logout_success(state){
            state.loginSuccess = false;
            state.userName = null;
            state.userPass = null;
        },
        desiredurl_modified(state, payload) {
            state.desiredUrl = payload.value;
        },
        access_failed(state, value){
            state.failedAccess = value;
        },
        update_user(state, payload){
            state.user = payload.user;
        },
        increment_avatar_key(state){
            state.avatarKey += 1;
        },
        connect_socket(state){
            state.socket = new SockJS(state.source+"/ws?access_token="+VueCookies.get('axesstoken'));
            state.stompClient = Stomp.over(state.socket);
            state.stompClient.connect({heartbeat: false}, frame  => {
                state.connected = true;
            });
            state.stompClient.debug = function(str) {};
        },
        update_logged_users(state, payload){
            state.loggedUsers = payload.users;
        },
        set_validation_campaign(state, payload){
            state.validationCampaign = payload.vc;
        }
    },
    actions: {
        login({commit}, {user, password}){
            return new Promise((resolve, object) => {
                api.requestAccessToken(user, password)
                    .then(response => {
                        if(response.access_token) {
                            VueCookies.set('axesstoken', response.access_token, response.expires_in, null, null, true);
                            commit('login_success', { userName: user });
                            api.getPersonalInfos(user).then( resp => { 
                                commit('update_user', {user : resp.data} );
                            })
                        }
                        resolve(response);
                    })
                    .catch(error => {
                        console.log("Error: " + error);
                        commit('login_error', { userName: user });
                        Promise.reject("Invalid credentials!");
                    })
            });
        },
        logout({commit}, {user}) {
            return new Promise((resolve, reject) => {
                console.log("logging out user: " + user);
                VueCookies.remove('axesstoken');
                commit('logout_success');
                resolve({status: 'disconnected'});
            });
        },
        changeDesiredUrl({commit}, {value}) {
            state.desiredUrl = payload.value;
            commit('desiredurl_modified', {value: value});
        },
        accessFailed({commit}, {value}) {
            commit('access_failed');
        },
        updateUser({commit}, {user}) {
            commit('update_user', { user: user });
        },
        connectSocket({commit}) {
            commit('connect_socket');
            // state.socket = new SockJS("https://profiterole-almanach-ui.paris.inria.fr:8888/ws?access_token="+VueCookies.get('axesstoken'));
            // state.stompClient = Stomp.over(state.socket);
            // state.connected = true;
        },
        setValidationCampaign({commit}, {vc}) {
            console.log('vc store', vc);
            commit('set_validation_campaign', {vc: vc});
        }

    },
    getters: {
        getSource: state => state.source,
        isLoggedIn: state => state.loginSuccess,
        hasLoginErrored: state => state.loginError,
        getUserName: state => state.userName,
        getUserPass: state => state.userPass,
        getDesiredUrl: state => state.desiredUrl,
        getFailedAccess: state => state.failedAccess,
        getUserInfos: state => state.user,
        getAvatarKey: state => state.avatarKey,
        getStompClient: state => state.stompClient,
        isConnected: state => state.connected,
        getAppVersion: state => state.appVersion,
        getLoggedUsers: state => state.loggedUsers,
        getValidationCampaign: state => state.validationCampaign
    }
})