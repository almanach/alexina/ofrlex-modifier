## Documentation
This table is made to display source informations before they are used to create the intentional lexicons (.ilex). Once a lexeme is modified, these modifications will impact all the following lexicon files with flexions (.lex).
This is why only some modifications are allowed: 
- Morphological Category (**cat**)
- Valency (**SubCat**)
- **Syntactic Info**
- **Redistributions**
- Variants (**VariantOf**) 

### Sources
1. FROLEX: 
  > - POS tags have been converted to CATTEX tag set with gender and number extension when necessary. 
2. Wiktionary: 
  > - Etymology, gloses and fleional informations where retrieved from it.
  > - Extraction process: structure infos into an XML file before using Alexina wiktionary gramar to obtain detailed flexional infos.
3. *Altfranzösisches Wörterbuch* by Tobler (**TL**): 
  > - List of lemmas (published by Achim Stein) with variants and secondary entries
  > - Entries etracted from the OCR version using a custom extractor and some manual fixes
4. *Lexique de l'ancien français* (**Godefroy**) from Wikisource filtered using *Base des mots fantômes*
  > - Definitions, citation form and page indexes where retrieved from it.
5. *Dictionnaire Électronique de Chrétien de Troyes* (**DECT**):
  > - Converted from PDF to text
  > - Entries extracted using simple rules
  > - Links with TL and Godefroy retrieved from it

### Columns
- **Headword**
An headword is a lemma chosen arbitrarily among multiple possible lemmas in order to have a reference form. You can have a glimpse of different lemmas if you take a look at the variants for each lexeme.
- **Lemma**
Lemmas were essentially obtained from the TL. 
- **Macro Cat**
Macro category is automatically computed from the CATTEX category in the **cat** column.
- **Cat**
The CATTEX morphological category with some added values about gender and number.
- **SubCat**
Valency informations. They come from *Lefff* verbs that were considered syntactically similar. Neede correction and/or addition.
To link an *Ofrlex* verb with a *Lefff* one, 5 informations were analyzed in order:
  1. Pseudo-gloss manually added if similar syntactic properties
  2. Contemporary French gloss from one of the sources or manually added 
  3. Godefroy or DECT definition when this definition is made of only one word
  4. Contemporary French descendent extracted from Wiktionary or manually
  5. *Lefff* entry with equal citation form in the *Ofrlex*
- **Flexion**
Flexional informations from Godefroy, DECT or TL. Possible values: adj, inv, 0 (transitive), conjuct, inv-e, inv, noun-m, noun-g, noun-f, noun-unkgdr (unknown gender), 
v-er (verb first group regular flexion, infinitive suffix = er), v-re , v-ier, v-unkconj (unknown conjunction), v-ir, v-[verb] (reference to the verb)
- **Syntactic Info**
Other or genereic (category) syntactic informations.
- **Redistributions**
Possible values: actif (active), passif (passive), actif_impersonnel
- **VariantOf**
Variants mainly from TL and manual insertions. You can refer to other Ofrlex headwords, it will compute the variants infos.
- **Pseudo Glosses > French Gloss > Descendent**
Hierarchical visualization of 3 columns. 
  - A *Pseudo gloss* (PG) is computed by finding similar syntactic properties
  - A *French gloss* (FG) is computed from one of the sources or manually added. It is from contemporary french
  - A *Descendent* (D) is a Contemporary French descendent extracted from Wiktionary or manually

### Variants
Variants initially come from the TL variants or manually added. 
However, variants informations are also computed from the DECT, the Godefroy and the FROLEX in order to take some missing informations fomr the variants and dispatch them to the linked headwords in a two-way fashion. 
Hence some informations visible here can come from variants.
You can quickly access variants main infos from FROLEX and Godefroy by expanding the row using the orange arrow on Headword. If there is no orange arrow, then there is nothing to see.

### Pseudosyns
Pseudosyns are pseudo synonyms obtained trough word vectorisation in context (embeddings), clustering and limited to the same Part-of-Speech tags. 
They contain three informations:
> - cosine distance with the associated entry (lexeme): the number in the left part
> - form 
> - PoS tag (in a rounded outlined square)
> ![Pseudosyn](../statics/pseudosyn.png)

You can click on it to validate or invalidate a pseudosyn. Once validated a pseudo synonym become green and its informations will be shared with the target entry (the row). The information sharing will be done by batch later on the proejct members' decision. Hence, you still can undo it before it's too late.

> A pseudosyn: ![Pseudosyn](../statics/pseudosyn.png)
> A validated pseudosyn: ![Pseudosyn](../statics/pseudosyn-validated.png)

For more information about how these pseudosyns are automatically obtained please refer to the related paper (to be published).