const routes = [
  { 
    path: '/', 
    component: () => import('layouts/AppLayout'),
    // Now we define the sub-routes.
    // These are getting injected into
    // layout (from above) automatically
    // by using <router-view> placeholder
    // (need to specify it in layout)
    children: [
      {
        path: '',
        component: () => import('pages/Hub')
      },
      {
        path: 'modifier',
        component: () => import('pages/Modifier'),
        meta: {
            requiresAuth: true
        }
      },
      {
        path: 'settings',
        component: () => import('pages/Settings'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'admin',
        component: () => import('pages/Admin'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'infos',
        component: () => import('pages/Infos'),
        meta: {
          requiresAuth: false
        }
      }
    ]
  },
  // otherwise redirect to home
  { path: '*', redirect: '/' }

]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404')
  })
}

export default routes
