import axios from 'axios'

export default async ({ Vue }) => {
  const AXIOS = axios.create({
    baseURL: `/api`,
    timeout: 10000
  });
  
  function hello() {
    return AXIOS.get(`/hello`);
  }
  function getUser(userId) {
    return AXIOS.get(`/user/` + userId);
  }
  function createUser(firstName, lastName) {
    return AXIOS.post(`/user/` + firstName + '/' + lastName);
  }
function getSecured(user, password) {
    return AXIOS.get(`/secured/`,{
        auth: {
            username: user,
            password: password
        }});
}
function getOfrlexPage(user, password, page, size) {
    return AXIOS.get('/ofrlex/' + page + '/' + size , {
        auth: {
            username: user,
            password: password
        }
    })
}
function updateOfrlex(user, password, entries) {
    return AXIOS.post('/ofrlex/update', entries, { auth: { username: user, password: password } });
}
function filterOfrlex(user, password, page, size, pattern) {
    return AXIOS.get('/ofrlex/filter?pattern='+pattern+'&pageSize='+size+'&page='+page, { auth: {username: user, password: password } });
}
function exportOfrlexExcel(user, password) {
    return AXIOS.get('/ofrlex/ofrlex.xlsx', { auth: {username: user, password: password }, responseType: 'blob', timeout: 20000 });
}
  
  Vue.prototype.$axios = axios

}
