import axios from 'axios'
import VueCookies from 'vue-cookies';
import { cpus } from 'os';
// set default config
VueCookies.config('7d');

// const source = "https://profiterole-almanach-ui.paris.inria.fr:8888";
const source = "https://localhost:8888"; 


const AXIOS = axios.create({
  baseURL: source+`/api`,
  timeout: 40000
});

const AUTH = axios.create({
    // unused quasar proxy to prevent issues in https CORS
    baseURL: source+`/oauth`,
    timeout: 10000,
    withCredentials: true
});

function fetchPost(url, data, typeStr){
    var headers = {};
    if(VueCookies.get('axesstoken')){
        headers = {'Accept': 'application/json', 'Content-Type': "application/json", 'Authorization': 'Bearer '+VueCookies.get('axesstoken') }; 
    }else{ headers = {'Accept': 'application/json', 'Content-Type': "application/json", "Access-Control-Allow-Headers": "x-requested-with"} }
    if(typeStr){
        return fetch(source+'/api'+url, { mode: 'cors', method: 'POST', body: data, headers: headers }).then((res) => res);
    }else{
        return fetch(source+'/api'+url, { mode: 'cors', method: 'POST', body: data, headers: headers }).then((res) => res.json());
    }
}

export default {
    ping(){ return AXIOS.get('/ping'); },
    testRoles(){
        return AXIOS.get('/ping', { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} } );
    },
    testFetch(){
        let data = {};
        return fetch(source+'/test', { mode: 'cors', method: 'GET',
        headers: {'Accept': 'application/json', 'Content-Type': "application/x-www-form-urlencoded", 'Authorization': 'Basic '+btoa('servuser:servpwd') }
        }).then((res) => res.json())
    },
    getUser(userId) {
        return AXIOS.get(`/user/` + userId);
    },
    getLoggedUsers(){
        return AXIOS.get('/users/logged');
    },
    getPersonalInfos(){
        return AXIOS.get(`/user`, { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} } );
    },
    updatePersonalInfos(user) {
        return AXIOS.post('/user/update', user, { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} } );
    },
    updateUserInfos(user, username) {
        return AXIOS.post('/user/update/'+username, user, { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
    },
    updateUserRoles(username, roles) {
        return AXIOS.post('/user/update/role/'+username, roles, { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
    },
    createUser(tempUser) {
        return AXIOS.post('/user/add', tempUser, { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
    },
    changePwd(tempUser) {
        return AXIOS.post('/user/changepwd', tempUser, { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
    },
    deleteUser(username) {
        return AXIOS.post('/user/delete/'+username, {} , { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
    },
    getSecured(user, password) {
        return AXIOS.get(`/secured/`,{
            auth: {
                username: user,
                password: password
            }});
    },
    getOfrlexPage(page, size, sort, descending) {
        if(sort != ''){
            return AXIOS.get('/ofrlex/' + page + '/' + size + '/' + sort + '/'+ descending, { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
        }else {
            return AXIOS.get('/ofrlex/' + page + '/' + size , { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
        }
    },
    addEntryOfrlex(entry) {
        return AXIOS.post('/ofrlex/add', entry, { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
    },
    updateOfrlex(entries) {
        return AXIOS.post('/ofrlex/update', entries, { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
    },
    filterOfrlex(page, size, pattern, sort, descending, col, type) {
        if(sort != ''){
            return AXIOS.get('/ofrlex/filter?pattern='+pattern+'&pageSize='+size+'&page='+page+'&sort='+sort+'&descending='+descending+'&col='+col+'&type='+type,
            { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
        }else {
            return AXIOS.get('/ofrlex/filter?pattern='+pattern+'&pageSize='+size+'&page='+page+'&col='+col+'&type='+type,
            { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
        }
    },
    exportOfrlexSelectionExcel(ids) {
        return AXIOS.post('/ofrlex/selection_ofrlex.xlsx', ids,  
            { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')}, responseType: 'blob', timeout: 60000 });
    },
    exportOfrlexExcel() {
        return AXIOS.get('/ofrlex/ofrlex.xlsx', 
            { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')}, responseType: 'blob', timeout: 60000 });
    },
    exportOfrlexCatExcel(cat) {
        return AXIOS.post('/ofrlex/exportcat', cat,
            { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')}, responseType: 'blob', timeout: 60000 });
    },
    exportOfrlexCatsZip() {
        return AXIOS.get('/ofrlex/export/cats/zip',{ headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')}, responseType: 'blob', timeout: 60000 });
    },
    exportOfrlexIlexZip() {
        return AXIOS.get('/ofrlex/export/ilex/zip',{ headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')}, responseType: 'blob', timeout: 180000 });
    },
    exportOfrlexTsvZip() {
        return AXIOS.get('/ofrlex/export/tsv/zip',{ headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')}, responseType: 'blob', timeout: 90000 });
    },
    deleteOfrlexRows(entries) {
        return AXIOS.post('/ofrlex/delete', entries, { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
    },
    freezeOfrlex(){
        return AXIOS.post('/ofrlex/freeze/toggle', {}, { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
    },
    getOfrlexStatus(){
        return AXIOS.get('/ofrlex/freeze/status', { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
    },
    startCampaign(){
        return AXIOS.get('/ofrlex/vc/start', { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
    },
    getCampaign(){
        return AXIOS.get('/ofrlex/vc/view', { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
    },
    resetCampaign(){
        return AXIOS.get('/ofrlex/vc/reset', { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
    },
    getCampaignEntries(page, size, sort, descending) {
        return AXIOS.get('/ofrlex/vc/entries/' + page + '/' + size + '/' + sort + '/'+ descending , { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
    },
    validateAll(min){
        return AXIOS.post('/ofrlex/vc/validateall/'+min, {}, { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
    },
    getHistory(){
        return AXIOS.get('/history/ofrlex', { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
    },
    requestAccessToken(user, pwd) {
        var data = { "grant_type": "password", "username":user, "password":pwd };
        // using fetch instead of axios for better cors handling (Xhttp vs request)
        return fetch(source+'/oauth/token', { mode: 'cors', method: 'POST', body: new URLSearchParams(data).toString(),
            headers: {'Accept': 'application/json', 'Content-Type': "application/x-www-form-urlencoded", 'Authorization': 'Basic '+btoa('servuser:servpwd'),
            'Access-Control-Request-Method': 'POST', 'Access-Control-Request-Headers': 'Authorization, Content-Type, Accept'  }
        }).then((res) => res.json()) // .then((dat) =>  console.log(dat))  // .catch((err)=>console.error(err))
    },
    requestUsers() {
        return AXIOS.get('/users', { headers: { Authorization : "Bearer "+VueCookies.get('axesstoken')} });
    },
    parseJwt(token) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
        return JSON.parse(jsonPayload);
    }
}


