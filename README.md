# ofrlex-modifier

User Interface that serves as a hub of tools for modifying and manipulating ofrlex.

## Live access

The application is still under developpement. But [you can access the current version here](https://profiterole-almanach-ui.paris.inria.fr:8888)

## Usage

**Releases** are [visible here](https://gitlab.inria.fr/gguibon/ofrlex-modifier/-/tags).

**Executables** JAR files can be [downloaded here](https://mybox.inria.fr/d/52796d58af/).

## Compilation

Shell is not available for download, in case of issues compiling please do:
```mvn -U package```

Final package can be found in ofrlex-modifier/backend/target/ and started with :
```java -Xmx4g -jar backend/target/NAME_OF_JAR.jar```

Your operating system's default browser will open on localhost:8888. You can also manually open this url : [https://localhost:8888](https://localhost:8888).

## Deploy

- ``localhost:8888`` in frontend files and in backend UserService urlDeploy variable need to be change to ``<DOMAIN>:8888`` corresponding to the url name of the domain to deploy to. Otherwise, the frontend will not be able to communicate with backend correctly.

## Notes

- lexicons entries with __id__ = "*" were not handled.