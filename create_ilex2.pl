#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;
use Data::Dumper;

# Usage: perl -w xlsx2txt.pl ofrlex4_verbs.xlsx  | perl -w create_ilex2.pl -l "<chemin_vers_le_dossier_source_lefff>/{v_new,prep,nom,adj,adv}.ilex" 
# Usage : cat tsvfile.tsv | perl -w create_ilex2.pl

my $verbose = 0;
# my $leffffiles = "/Users/sagot/Documents/alexina/lefff/{v_new,prep,nom,adj,adv}.ilex";
my $leffffiles = "/Users/gguibon/eclipse-workspace/lefff/{v_new,prep,nom,adj,adv}.ilex";
my $lefffformsfile;
my $udtrainfile;

while (1) {
  $_=shift;
  if (/^$/) {last;}
#   elsif (/^-l$/) {$leffffiles = shift;}
  elsif (/^-v$/) {$verbose = 1;}
  elsif (/^-f$/) {$lefffformsfile = shift;}
  elsif (/^-ud$/) {$udtrainfile = shift;}
  else {die "Unrecognized option: $_"}
}

my %lefff;
if ($leffffiles ne "") {
  for (glob qq("${leffffiles}")) {
    #print STDERR "  Reading $_\n";
    open LEX, "<$_" || die "Could not open $_: $!";
    binmode LEX, ":utf8";
    while (<LEX>) {
      chomp;
      next if /^#/ || /^\s*$/ || /^_error/;
      /^((?:[^\t_]|_[^\t_])+)(?:___?([^\t]+))?\t([^\t]+)\t(\d+);([^\t;]*);([^\t;]*);([^\t;]*);([^\t;]*);([^\t;]*)(\t|$)/ || die "Invalid line: $_";
      my ($l, $sid, $inflc, $w, $semid, $cat, $subcat, $syntfeats, $redistr) = ($1, $2, $3, $4, $5, $6, $7, $8, $9);
      my ($prontype, $pronflag, $syntflag);
      if ($cat eq "v") {
	my $pronflag = "N";
	if ($semid =~ /^s[e']/) {
	  $prontype = 1;
	  $pronflag = "S"
	} elsif ($subcat =~ /\bse(?:r[ée](?:fl|c))/) {
	  $prontype = 2;
	  $pronflag = "R";
	}
	$syntflag = "I";
	if ($subcat =~ /Obj:/) {
	  if ($subcat =~ /Objà:/) {
	    $syntflag = "2";
	  } else {
	    $syntflag = "T";
	  }
	} elsif ($redistr =~ /impersonnel/) {
	  $syntflag = "1";
	}
      } else {
      }
      my $id;
      if ($syntflag ne "" && $pronflag ne "") {
	$id = $#{$lefff{$l}{$cat}{$pronflag.$syntflag}}+1;
	$lefff{$l}{$cat}{$pronflag.$syntflag}[$id]{sid} = $sid;
	$lefff{$l}{$cat}{$pronflag.$syntflag}[$id]{inflc} = $inflc;
	$lefff{$l}{$cat}{$pronflag.$syntflag}[$id]{w} = $w;
	$lefff{$l}{$cat}{$pronflag.$syntflag}[$id]{cat} = $cat;
	$lefff{$l}{$cat}{$pronflag.$syntflag}[$id]{subcat} = $subcat;
	$lefff{$l}{$cat}{$pronflag.$syntflag}[$id]{syntfeats} = $syntfeats;
	$lefff{$l}{$cat}{$pronflag.$syntflag}[$id]{redistr} = $redistr;
	$lefff{$l}{$cat}{$pronflag.$syntflag}[$id]{is_pron} = $prontype > 0 ? 1 : 0;
      }
      if ($pronflag ne "") {
	$id = $#{$lefff{$l}{$cat}{$pronflag}}+1;
	$lefff{$l}{$cat}{$pronflag}[$id]{sid} = $sid;
	$lefff{$l}{$cat}{$pronflag}[$id]{inflc} = $inflc;
	$lefff{$l}{$cat}{$pronflag}[$id]{w} = $w;
	$lefff{$l}{$cat}{$pronflag}[$id]{cat} = $cat;
	$lefff{$l}{$cat}{$pronflag}[$id]{subcat} = $subcat;
	$lefff{$l}{$cat}{$pronflag}[$id]{syntfeats} = $syntfeats;
	$lefff{$l}{$cat}{$pronflag}[$id]{redistr} = $redistr;
	$lefff{$l}{$cat}{$pronflag}[$id]{is_pron} = $prontype > 0 ? 1 : 0;
      }
      $id = $#{$lefff{$l}{$cat}{""}}+1;
      $lefff{$l}{$cat}{""}[$id]{sid} = $sid;
      $lefff{$l}{$cat}{""}[$id]{inflc} = $inflc;
      $lefff{$l}{$cat}{""}[$id]{w} = $w;
      $lefff{$l}{$cat}{""}[$id]{cat} = $cat;
      $lefff{$l}{$cat}{""}[$id]{subcat} = $subcat;
      $lefff{$l}{$cat}{""}[$id]{syntfeats} = $syntfeats;
      $lefff{$l}{$cat}{""}[$id]{redistr} = $redistr;
      $lefff{$l}{$cat}{""}[$id]{is_pron} = $prontype > 0 ? 1 : 0;
      $lefff{$l}{$cat}{""}[$id]{pronflag} = $pronflag;
    }
    close LEX;
  }
}

my %cols = (
	    "id" => 0,
	    "hw" => 1,
	    "scat" => 2,
	    "TLhw" => 3,
	    "TLlemma" => 4,
	    "TLhw_ms" => 5,
	    "TLref" => 6,
	    "TLhw_scat" => 7,
	    "inflclass" => 8,
	    "hw__scat" => 9,
	    "cat" => 10,
	    "syntinfo" => 11,
	    "Gkey" => 12,
	    "Gcatkey" => 13,
	    "Ghw" => 14,
	    "Gpcat" => 15,
	    "Gdef" => 16,
	    "Gref" => 17,
	    "GDhw_DECT" => 18,
	    "GDref_DECT" => 19,
	    "TLentry_DECT" => 20,
	    "TLentry_DECT_ishw" => 21,
	    "DECThw" => 22,
	    "DECTpage" => 23,
	    "DECTcat_ms" => 24,
	    "DECTscat" => 25,
	    "DECTlemma" => 26,
	    "DECTfreq" => 27,
	    "FBhw_DECT" => 28,
	    "FEWetymon_DECT" => 29,
	    "FEWref_DECT" => 30,
	    "TLFhw_DECT" => 31,
	    "ANDhw_DECT" => 32,
	    "DMFhw_DECT" => 33,
	    "DECTdef" => 34,
	    "BFMfreq" => 35,
	    "gloss" => 36,
	    "desc" => 37,
	    "TLvariantof" => 38,
	    "variantof" => 39,
	    "TLdesc" => 40,
	    "variants" => 41,
	    "pseudogloss" => 42,
	    "mansubcat" => 43,
	    "mansyntfeats" => 44,
	    "manredistr" => 45,
	);


my %tl_defs;
my ($cat, $id, $TLhw, $hasstar);
my @line;
open TL, "<TL_extraction.tsv" || die "Could not open 'TL_extraction.tsv': $!";
binmode TL, ":utf8";
while (<TL>) {
  chomp;
  @line = split /\t/, $_;
  $TLhw = $line[0];
  $cat = $line[1];
  $cat =~ s/\..*//;
  $hasstar = ($TLhw =~ s/^\*//);
  if (defined($tl_defs{$TLhw}) && defined($tl_defs{$TLhw}{$cat})) {
    die $TLhw;
  }
  $tl_defs{$TLhw}{$cat}{hasstar} = $hasstar;
  $tl_defs{$TLhw}{$cat}{def} = $line[3];
  $tl_defs{$TLhw}{$cat}{fullcat} = $line[1];
  $tl_defs{$TLhw}{$cat}{origcat} = $line[1];
}

my ($hw, $inflclass, $cat);
my ($TLsourcehw, $GDhw, $GDhw_from_DECT);
my (%ilex, %tlhw_hw2id, %tlsourcehw_hw2id, %tlhw_hw2cat, %tlsourcehw_hw2cat);
my $counterFile;
while (<>) {
#   my ($currentFileName, $currentFileNumber) = @ARGV;
#   print "file named : $ARGV , number $counterFile\n";
#   print "line $. : ", $_;
  chomp;
  next if /^x\t/i;
  next if /^__id__\t/i;
  die unless /^\d+\t/;
#   next if $_ eq "";
  s/&lt;/</g;
  s/&gt;/>/g;
  s/&quot;/"/g;
  s/&apos;/'/g;
  s/&amp;/&/g;
  @line = split /\t/, $_;
  $hw = $line[$cols{hw}];
  $inflclass = $line[$cols{inflclass}];
  $cat = $line[$cols{cat}];
  die "Empty cat for headword '$hw' , line = '$_'" if $cat eq "";
  $TLhw = $line[$cols{TLhw}];
  if (defined($ilex{$hw}) && defined($ilex{$hw}{$cat})) {
    $id = $#{$ilex{$hw}{$cat}} + 1;
  } else {
    $id = 1;
  }
  # DO NOT FILL %ilex before the above lines, which determine the correct $id (or this creates a ghost entry in %ilex, which screws everything up).
  $ilex{$hw}{$cat}[$id]{id} = $line[$cols{id}]; #gg
  if ($line[$cols{DECThw}] ne "") {
    # VERT	1145	ADJ	a.	vert	17	vert	viridis	XIV, 507a	vert
    # DECT entry	DECT page	DECT cat.ms	DECT scat	DECT lemma	DECT freq	F-B (from DECT)	FEW etymon (from DECT)	FEW ref (from DECT)	TLF (from DECT)	AND (from DECT)	DMF (from DECT)
    $ilex{$hw}{$cat}[$id]{dectentry} = $line[$cols{DECThw}];
    $ilex{$hw}{$cat}[$id]{dectpage} = $line[$cols{DECTpage}];
    $ilex{$hw}{$cat}[$id]{dectcat} = $line[$cols{DECTscat}];
    $ilex{$hw}{$cat}[$id]{dectlemma} = $line[$cols{DECTlemma}];
    $ilex{$hw}{$cat}[$id]{dectfreq} = $line[$cols{DECTfreq}];
    $ilex{$hw}{$cat}[$id]{dectfb} = $line[$cols{FBhw_DECT}] if $line[$cols{FBhw_DECT}] ne "";
    $ilex{$hw}{$cat}[$id]{dectfew} = $line[$cols{FEWetymon_DECT}] if $line[$cols{FEWetymon_DECT}] ne "";
    $ilex{$hw}{$cat}[$id]{dectfewref} = $line[$cols{FEWref_DECT}] if $line[$cols{FEWref_DECT}] ne "";
    $ilex{$hw}{$cat}[$id]{decttlf} = $line[$cols{TLFhw_DECT}] if $line[$cols{TLFhw_DECT}] ne "";
    $ilex{$hw}{$cat}[$id]{dectand} = $line[$cols{ANDhw_DECT}] if $line[$cols{ANDhw_DECT}] ne "";
    $ilex{$hw}{$cat}[$id]{dectdmf} = $line[$cols{DMFhw_DECT}] if $line[$cols{DMFhw_DECT}] ne "";
    $ilex{$hw}{$cat}[$id]{dectdef} = $line[$cols{DECTdef}] if $line[$cols{DECTdef}] ne "";
  }
  $ilex{$hw}{$cat}[$id]{tlms} = $line[$cols{TLhw_ms}];
  $ilex{$hw}{$cat}[$id]{tlloc} = "TL:".$line[$cols{TLref}];
  $ilex{$hw}{$cat}[$id]{inflclass} = $line[$cols{inflclass}];
  if ($line[$cols{syntinfo}] ne "") {
    $line[$cols{syntinfo}] =~ /^([^;]*);([^;]*);([^;]*)$/ || die "Incorrect syntactic information for headword '$hw': '$line[$cols{syntinfo}]'";
  }
  $ilex{$hw}{$cat}[$id]{g} = $line[$cols{gloss}];
  $ilex{$hw}{$cat}[$id]{desc} = $line[$cols{desc}];
  $ilex{$hw}{$cat}[$id]{pg} = $line[$cols{pseudogloss}];
  $ilex{$hw}{$cat}[$id]{scatman} = $line[$cols{mansubcat}];
  if ($TLhw ne "") {
    $ilex{$hw}{$cat}[$id]{tlhw} = $TLhw;
    $tlhw_hw2id{$TLhw}{$hw} = $id;
    $tlhw_hw2cat{$TLhw}{$hw} = $cat;
    $TLsourcehw = "";
    if (defined($tl_defs{$TLhw}) && defined($tl_defs{$TLhw}{$cat})) {
      $TLsourcehw = $TLhw;
    } elsif (defined($tl_defs{lc($TLhw)}) && defined($tl_defs{lc($TLhw)}{$cat})) {
      $TLsourcehw = lc($TLhw);
    } else {
      for (1..9) {
	if (defined($tl_defs{$TLhw.$_}) && defined($tl_defs{$TLhw.$_}{$cat})) {
	  $TLsourcehw = $TLhw.$_;
	  last;
	} elsif (defined($tl_defs{lc($TLhw.$_)}) && defined($tl_defs{lc($TLhw.$_)}{$cat})) {
	  $TLsourcehw = lc($TLhw.$_);
	  last;
	}
      }
    }
    if ($TLsourcehw eq "") {
      #print STDERR "TL entry for '$TLhw' not found\n" if $verbose;
    } else {
      if ($TLsourcehw ne $TLhw) {
	#print STDERR "TL entry for '$TLhw' not found, using '$TLsourcehw' instead\n";
      }
      $ilex{$hw}{$cat}[$id]{tlsourcehw} = $TLsourcehw;
      $tlsourcehw_hw2id{$TLsourcehw}{$hw} = $id;
      $tlsourcehw_hw2cat{$TLsourcehw}{$hw} = $cat;
    }
  }
  $GDhw = $line[$cols{Gkey}];
  $GDhw_from_DECT = $line[$cols{GDhw_DECT}];
  if ($GDhw ne "") {
    $GDhw_from_DECT =~ s/ \/ .*//;
    if ($line[$cols{id}] > 0 && $GDhw !~ /_C$/) {
      $GDhw =~ /^(.+?)(?: \(s[e']\))?[1-9]?$/;
      if ($GDhw_from_DECT ne "" && $1 ne $GDhw_from_DECT && !(
				     $1 eq "baucenc" && $GDhw_from_DECT eq "baucent"
				     || $1 eq "adoler" && $GDhw_from_DECT eq "adouler"
				     || $1 eq "aquerre" && $GDhw_from_DECT eq "aquerir"
				     || $1 eq "demincier" && $GDhw_from_DECT eq "demincer"
				     || $1 eq "desavenir" && $GDhw_from_DECT eq "desavenant"
				     || $1 eq "desprendre" && $GDhw_from_DECT eq "despris"
				     || $1 eq "enhaïr" && $GDhw_from_DECT eq "enhair"
				     || $1 eq "maleïr" && $GDhw_from_DECT eq "maleir"
				     || $1 eq "morir" && $GDhw_from_DECT eq "mort"
				     || $1 eq "raancler" && $GDhw_from_DECT eq "draoncler"
				     || $1 eq "nuire" && $GDhw_from_DECT eq "nuisant"
				     || $1 eq "ocire" && $GDhw_from_DECT eq "ocirre"
				     || $1 eq "recerceler" && $GDhw_from_DECT eq "recercelé"
				     || $1 eq "respitier" && $GDhw_from_DECT eq "respitié"
				     || $1 eq "reprover" && $GDhw_from_DECT eq "resprouver"
				     || $1 eq "sembler" && $GDhw_from_DECT eq "semblant"
				     || $1 eq "soudre" && $GDhw_from_DECT eq "soldre"
				     || $1 eq "tolir" && $GDhw_from_DECT eq "toldre"
				     || $1 eq "trespasser" && $GDhw_from_DECT eq "trespassant"
				     || $1 eq "trespenser" && $GDhw_from_DECT eq "trespensé"
				     || $1 eq "valoir" && $GDhw_from_DECT eq "vaillant"
				     || $1 eq "crieme" && $GDhw_from_DECT eq "creme"
				     || $1 eq "erraument" && $GDhw_from_DECT eq "eriaument"
				     || $1 eq "estandart" && $GDhw_from_DECT eq "estandard"
				     || $1 eq "estour" && $GDhw_from_DECT eq "estor"
				     || $1 eq "feintié" && $GDhw_from_DECT eq "faintié"
				     || $1 eq "florete" && $GDhw_from_DECT eq "florette"
				     || $1 eq "geme" && $GDhw_from_DECT eq "gemme"
				     || $1 eq "glas" && $GDhw_from_DECT eq "clas"
				     || $1 eq "igal" && $GDhw_from_DECT eq "ivel"
				     || $1 eq "lait" && $GDhw_from_DECT eq "laid"
				     || $1 eq "maisiere" && $GDhw_from_DECT eq "meisiere"
				     || $1 eq "maissele" && $GDhw_from_DECT eq "maiscele"
				     || $1 eq "nasel" && $GDhw_from_DECT eq "nasal"
				     || $1 eq "oïe" && $GDhw_from_DECT eq "oie"
				     || $1 eq "orteil" && $GDhw_from_DECT eq "ortel"
				     || $1 eq "petit" && $GDhw_from_DECT eq "menor"
				     || $1 eq "piu" && $GDhw_from_DECT eq "pif"
				     || $1 eq "poitral" && $GDhw_from_DECT eq "poitrail"
				     || $1 eq "ro" && $GDhw_from_DECT eq "rau"
				     || $1 eq "soutain" && $GDhw_from_DECT eq "soltain"
				     || $1 eq "voir" && $GDhw_from_DECT eq "voire"
				     || $1 eq "vuit" && $GDhw_from_DECT eq "vuide"
				    )) {
#	die "$line[$cols{id}]: $GDhw does not match $GDhw_from_DECT\n";
      } else {
	$ilex{$hw}{$cat}[$id]{godefroyentry} = $line[$cols{Ghw}];
	$ilex{$hw}{$cat}[$id]{godefroyloc} = $line[$cols{Gref}];
	$ilex{$hw}{$cat}[$id]{godefroyms} = $line[$cols{Gpcat}];
	$ilex{$hw}{$cat}[$id]{godefroydef} = $line[$cols{Gdef}];
      }
    }
  }
  $ilex{$hw}{$cat}[$id]{manvariantof} = $line[$cols{variantof}] if $line[$cols{variantof}] ne "";
  $ilex{$hw}{$cat}[$id]{tlvariantof} = $line[$cols{TLvariantof}] if $line[$cols{TLvariantof}] ne "";
#   print "line $. : ", $_;
#   close ARGV if eof;
#   close();
  $counterFile++ if eof;
}

# take infos from variants
for $hw (sort {remove_diacritics($a) cmp remove_diacritics($b)} keys %ilex) {
	# print Dumper( $hw ), Dumper( $ilex{$hw}{$cat}[$id]{manvariantof} ), Dumper( $ilex{$hw} );
	die "ERROR: '$hw' is an empty headword" if $hw eq "";
	for $cat (sort {$a cmp $b} keys %{$ilex{$hw}}) {
		for $id (1..$#{$ilex{$hw}{$cat}}) {
			my ($variant, $varianthw, $variantcat, $variantid);
			if (defined($ilex{$hw}{$cat}[$id]{manvariantof})) {
				if (defined $ilex{$ilex{$hw}{$cat}[$id]{manvariantof}} && defined $ilex{$ilex{$hw}{$cat}[$id]{manvariantof}}{$cat}) {
					die "Underspecified manual variant '$ilex{$hw}{$cat}[$id]{manvariantof}' for entry $hw ($id) [".$#{$ilex{$ilex{$hw}{$cat}[$id]{manvariantof}}{$cat}}." possible reference entries]" if $#{$ilex{$ilex{$hw}{$cat}[$id]{manvariantof}}{$cat}} > 1;
					$varianthw = $ilex{$hw}{$cat}[$id]{manvariantof};
					$variantcat = $cat;
					$variantid = 1;
				} elsif (defined($tlhw_hw2id{$ilex{$hw}{$cat}[$id]{manvariantof}}) && defined($tl_defs{$ilex{$hw}{$cat}[$id]{manvariantof}})) {
					my $TLvariant = $ilex{$hw}{$cat}[$id]{manvariantof};
					die "Manual, apparently TL variant '$TLvariant' not associated with an entry (entry $hw)" unless defined $tlhw_hw2id{$TLvariant};
					$TLvariant =~ /^(.*?)[0-9]*$/;
					my $modTLvariant = $1;
					$modTLvariant =~ s/^\(s\)/s/g;
					$modTLvariant =~ s/([^ ])\(.\)/$1/g;
					$modTLvariant =~ s/\!$//g;
					$modTLvariant =~ s/^(.*) \(([^,]+)(?:, .*)?\)$/$2 $1/g;
					$modTLvariant =~ s/-$/-_/g;
					if (scalar keys %{ $tlhw_hw2id{$TLvariant} } == 1) {
						$variant = join "", map {$_.$tlhw_hw2id{$TLvariant}{$_}} keys %{$tlhw_hw2id{$TLvariant}};
						$variant =~ /^(.*?)([0-9]*)$/;
						$varianthw = $1;
						$variantcat = $tlhw_hw2cat{$TLvariant}{$1};
						die if $variantcat eq "1";
						$variantid = $2;
						die unless defined $ilex{$varianthw} && defined $ilex{$varianthw}{$variantcat} && defined $ilex{$varianthw}{$variantcat}[$variantid];
					} elsif (defined($tlhw_hw2id{$TLvariant}{$modTLvariant})) {
						$variant = $modTLvariant;
						$varianthw = $variant;
						$variantcat = $tlhw_hw2cat{$TLvariant}{$modTLvariant};
						die if $variantcat eq "1";
						$variantid = $tlhw_hw2id{$TLvariant}{$modTLvariant};
					} else {
						die "Underspecified TL variant '$TLvariant' (modified: $modTLvariant) (entries: ".(join ", ", sort keys %{ $tlhw_hw2id{$TLvariant} }).")";
					}
				} else {
					warn "Unknown manual variant '$ilex{$hw}{$cat}[$id]{manvariantof}' (cat $cat) for entry $hw ($id) (".defined($tlhw_hw2id{$ilex{$hw}{$cat}[$id]{manvariantof}})." && ".defined($tl_defs{$ilex{$hw}{$cat}[$id]{manvariantof}}).") for file named ".$ARGV[0]." and file number ".$ARGV[1];
				}
			}
			if ($variant eq "") {
				if (defined($ilex{$hw}{$cat}[$id]{tlvariantof})) {
					my $TLvariant = $ilex{$hw}{$cat}[$id]{tlvariantof};
					die "TL variant '$TLvariant' not associated with an entry (entry $hw)" unless defined $tlhw_hw2id{$TLvariant};
					$TLvariant =~ /^(.*?)[0-9]*$/;
					my $modTLvariant = $1;
					$modTLvariant =~ s/^\(s\)/s/g;
					$modTLvariant =~ s/([^ ])\(.\)/$1/g;
					$modTLvariant =~ s/\!$//g;
					$modTLvariant =~ s/^(.*) \(([^,]+)(?:, .*)?\)$/$2 $1/g;
					$modTLvariant =~ s/-$/-_/g;
					if (scalar keys %{ $tlhw_hw2id{$TLvariant} } == 1) {
						$variant = join "", map {$_.$tlhw_hw2id{$TLvariant}{$_}} keys %{$tlhw_hw2id{$TLvariant}};
						$variant =~ /^(.*?)([0-9]*)$/;
						$varianthw = $1;
						$variantcat = $tlhw_hw2cat{$TLvariant}{$1};
						die if $variantcat eq "1";
						$variantid = $2;
						die unless defined $ilex{$varianthw} && defined $ilex{$varianthw}{$variantcat} && defined $ilex{$varianthw}{$variantcat}[$variantid];
					} elsif (defined($tlhw_hw2id{$TLvariant}{$modTLvariant})) {
						$variant = $modTLvariant;
						$varianthw = $variant;
						$variantcat = $tlhw_hw2cat{$TLvariant}{$modTLvariant};
						die if $variantcat eq "1";
						$variantid = $tlhw_hw2id{$TLvariant}{$modTLvariant};
					} else {
						die "Underspecified TL variant '$TLvariant' (modified: $modTLvariant) (entries: ".(join ", ", sort keys %{ $tlhw_hw2id{$TLvariant} }).")";
					}
				}
			}
			if ($varianthw ne "") {
			$ilex{$hw}{$cat}[$id]{varianthw} = $varianthw;
			$ilex{$hw}{$cat}[$id]{variantcat} = $variantcat;
			$ilex{$hw}{$cat}[$id]{variantid} = $variantid;
			my $i = $#{$ilex{$varianthw}{$variantcat}[$variantid]{variants}} + 1;
			$ilex{$varianthw}{$variantcat}[$variantid]{variants}[$i]{hw} = $hw;
			$ilex{$varianthw}{$variantcat}[$variantid]{variants}[$i]{cat} = $cat;
			$ilex{$varianthw}{$variantcat}[$variantid]{variants}[$i]{id} = $id;
			}
		}
	}
}

my $didsomething = 1;
while ($didsomething) {
  $didsomething = 0;
  for $hw (sort {remove_diacritics($a) cmp remove_diacritics($b)} keys %ilex) {
    for $cat (sort {$a cmp $b} keys %{$ilex{$hw}}) {
      for $id (1..$#{$ilex{$hw}{$cat}}) {
	if (defined($ilex{$hw}{$cat}[$id]{varianthw})) {
	  my $varianthw = $ilex{$hw}{$cat}[$id]{varianthw};
	  my $variantcat = $ilex{$hw}{$cat}[$id]{variantcat};
	  my $variantid = $ilex{$hw}{$cat}[$id]{variantid};
	  die "Non-existing variant associated with entry $hw/$cat/$id: $varianthw/$variantcat/$variantid" unless defined($ilex{$varianthw}) && defined($ilex{$varianthw}{$variantcat}) && $#{$ilex{$varianthw}{$variantcat}} >= $variantid;
	  if ($ilex{$hw}{$cat}[$id]{varianthw} eq $ilex{$varianthw}{$variantcat}[$variantid]{varianthw} && $ilex{$hw}{$cat}[$id]{variantcat} eq $ilex{$varianthw}{$variantcat}[$variantid]{variantcat} && $ilex{$hw}{$cat}[$id]{variantid} == $ilex{$varianthw}{$variantcat}[$variantid]{variantid}) {
#	    #print STDERR "!!! Deleting \$ilex{$hw}{$cat}[$id]{varianthw}\n";
	    delete($ilex{$hw}{$cat}[$id]{varianthw});
	    delete($ilex{$hw}{$cat}[$id]{variantcat});
	    delete($ilex{$hw}{$cat}[$id]{variantid});
	  } else {
	    if (defined($ilex{$varianthw}{$variantcat}[$variantid]{varianthw})) {
	      for ("varianthw", "variantcat", "variantid") {
#		#print STDERR "\$ilex{$hw}{$cat}[$id]{$_} ($ilex{$hw}{$cat}[$id]{$_}) rewritten as $ilex{$varianthw}{$variantcat}[$variantid]{$_}\n";
		$ilex{$hw}{$cat}[$id]{$_} = $ilex{$varianthw}{$variantcat}[$variantid]{$_};
	      }
	      $didsomething = 1;
	    }
	  }
	}
      }
    }
  }
}


my @words;
my $line;
my %form_lemma;
my %possible_scats;

# if (0) {
#   if ($lefffformsfile ne "") {
#     # creating a form->lemma association table
#     open VERB_F, "<$lefffformsfile" || die $!;
#     binmode VERB_F, ":utf8";
#     while (<VERB_F>) {
#       chomp;
#       next if /^_error\t/;
#       /^([^\t]+)\t([^\t_]+)/ || die "|$_|";
#       my ($f, $l) = ($1, $2);
#       #    #print STDERR "$l\t".defined($ilex{$l})." && ".defined($ilex{$l}{VER})." && ".$#{$ilex{$l}{VER}}." && ".defined($ilex{$l}{VER}[1]{varianthw})."\n" if $l =~ /^[oö][iï]r$/;
#       if (defined($ilex{$l}) && defined($ilex{$l}{VER}) && $#{$ilex{$l}{VER}} == 1 && defined($ilex{$l}{VER}[1]{varianthw})) {
# 	# if the lemma has only one entry and is the variant of another one, we use said variant instead (in order to reduce spurious ambiguities)
# 	$form_lemma{$f}{$ilex{$l}{VER}[1]{varianthw}} = 1;
#       } else {
# 	$form_lemma{$f}{$l} = 1;
#       }		     
#     }
#     close VERB_F;
#   }

#   if ($udtrainfile ne "") {
#     open UDTRAIN, "<$udtrainfile" || die $!;
#     while (<UDTRAIN>) {
#       chomp;
#       $line++;
#       if (/^$/) {
# 	process_last_udsentence();
# 	@words = ();
#       } elsif (/^(\d+)\t([^\t]+)\t([^\t]+)\t([^\t]+)\t[^\t]+\t[^\t]+\t([^\t]+)\t([^\t]+)\t.*/) {
# 	my ($i, $f, $l, $upos, $sg, $sl) = ($1, $2, $3, $4, $5, $6);
# 	for (split /:/, $sl) {
# 	  $words[$i]{form} = $f;
# 	  $words[$i]{lemma} = $l unless $l eq "_";
# 	  $words[$i]{upos} = $upos;
# 	  $words[$i]{sgov} = $sg;
# 	  $words[$i]{slabel} = $_;
# 	  $words[$i]{line} = $line;
# 	}
#       }
#     }
#     close UDTRAIN;
#   }
#   for my $lemma (sort keys %possible_scats) {
#     for (sort keys %{$possible_scats{$lemma}}) {
#       #print STDERR "$lemma\t$_\t$possible_scats{$lemma}{$_}\n";
#     }
#   }
# }


# create subcat based on temp scat
for $hw (sort {remove_diacritics($a) cmp remove_diacritics($b)} keys %ilex) {
  die "ERROR: '$hw' is an empty headword" if $hw eq "";
  for $cat (sort {$a cmp $b} keys %{$ilex{$hw}}) {
    for $id (1..$#{$ilex{$hw}{$cat}}) {
      my ($selected_linestart, $selected_hw, $selected_type, $selected_syntnb, $selected_ispron);
      die if $hw eq "";
      my $tlsourcehw = get_info_from_self_or_variants($hw,$cat,$id,"tlsourcehw",0);
      $ilex{$hw}{$cat}[$id]{tldef} = $tl_defs{$tlsourcehw}{$tlsourcehw_hw2cat{$tlsourcehw}{$hw}}{def};
      my $lefffcat;

      if ($cat eq "VER") {
	$lefffcat = "v";
	for my $infotype ("mansubcat", "pg", "g", "godefroydef", "tldef", "desc", "selfdmf", "self") {
	  my (@e_hw, @e_type, @e_syntnb, @e_ispron, @e_infotype);
	  my $globallypushedsomething;
	  die if $hw eq "";
	  my $info = get_info_from_self_or_variants($hw,$cat,$id,$infotype,0);
	  for my $r (split /\s*\|\|\s*/, $info) {
	    next if $r eq "";
	    my $syntflag = "";
	    my $syntbracket = "";
	    if ($r =~ s/^\[(.*?)\] *//) {
	      $syntbracket = $1;
	    }
	    next if $syntbracket =~ /übertr\./;
	    my $understood_syntbracket = 0;
	    if ($syntbracket eq "v. a." || $syntbracket =~ /trans\.|mit\s+\S+\s+obj\./i) {
	      $syntflag = "T";
	      $understood_syntbracket = 1;
	    } elsif ($syntbracket eq "v. n." || $syntbracket =~ /^intr\.(?: mit|$)/ || $syntbracket eq "intr. & refl." || $syntbracket eq "abs.") {
	      $syntflag = "I";
	      $understood_syntbracket = 1;
	    } elsif ($syntbracket eq "intr. subjektlos") {
	      $syntflag = "i";
	      $understood_syntbracket = 1;
	    } elsif ($syntbracket eq "v. impers.") {
	      $syntflag = "1";
	      $understood_syntbracket = 1;
	    }
	    my $pron = 0;
	    if ($syntbracket eq "v. réfl." || $syntbracket =~ /^refl\.(?: mit|$)/) {
	      $pron = 2;
	      $understood_syntbracket = 1;
	    }
	    #print STDERR "### |$syntbracket|\n" unless $understood_syntbracket || $syntbracket eq "";
	    my $pushedsomething = 0;
	    for (split /[,;] ?/, $r) {
	      my $originfo = $_;
	      $pron = 0 if $pron == 1;
	      $pron = 1 if s/^\(pron\.?\) *//;
	      s/^\(.*?\) *//;
	      if (s/^s(?:'|e )(.*)$/\1/) {
		if (defined $lefff{$_} && defined $lefff{$_}{$lefffcat}) {
		  if (defined($lefff{$_}{$lefffcat}{S})) {
		    push @e_hw, $_;
		    push @e_type, "S";
		    push @e_syntnb, $#{$lefff{$_}{$lefffcat}{S}}+1;
		    push @e_ispron, ($pron > 0 ? 1 : 0);
		    push @e_infotype, $infotype;
		    $pushedsomething = 1;
		  } elsif (defined($lefff{$_}{$lefffcat}{R})) {
		    push @e_hw, $_;
		    push @e_type, "R";
		    push @e_syntnb, $#{$lefff{$_}{$lefffcat}{R}}+1;
		    push @e_ispron, ($pron > 0 ? 1 : 0);
		    push @e_infotype, $infotype;
		    $pushedsomething = 1;
		  }
		}
	      } elsif ($_ ne "_") {
		if (defined $lefff{$_} && defined $lefff{$_}{$lefffcat}) {
		  if ($syntflag ne "" && (defined $lefff{$_}{$lefffcat}{"N".$syntflag} || defined $lefff{$_}{$lefffcat}{"R".$syntflag})) {
		    push @e_hw, $_;
		    push @e_type, "N".$syntflag.","."R".$syntflag;
		    push @e_syntnb, (defined $lefff{$_}{$lefffcat}{"N".$syntflag} ? $#{$lefff{$_}{$lefffcat}{"N".$syntflag}}+1 : 0) + (defined $lefff{$_}{$lefffcat}{"R".$syntflag} ? $#{$lefff{$_}{$lefffcat}{"R".$syntflag}}+1 : 0);
		    push @e_ispron, ($pron > 0 ? 1 : 0);
		    push @e_infotype, $infotype;
		    $pushedsomething = 1;
		  } elsif (defined $lefff{$_}{$lefffcat}{"N"} || defined $lefff{$_}{$lefffcat}{"R"}) {
		    push @e_hw, $_;
		    push @e_type, "N,R";
		    push @e_syntnb, (defined $lefff{$_}{$lefffcat}{"N"} ? $#{$lefff{$_}{$lefffcat}{"N"}}+1 : 0) + (defined $lefff{$_}{$lefffcat}{"R"} ? $#{$lefff{$_}{$lefffcat}{"R"}}+1 : 0);
		    push @e_ispron, ($pron > 0 ? 1 : 0);
		    push @e_infotype, $infotype;
		    $pushedsomething = 1;
		  }
		}
	      }
	    }
	    unless ($pushedsomething) {
	      if ($syntflag ne "" && !/ /) {
		push @e_hw, "";
		push @e_type, $syntflag;
		push @e_syntnb, 1;
		push @e_ispron, ($pron > 0 ? 1 : 0);
		push @e_infotype, $infotype;
		$pushedsomething = 1;
	      } elsif ($infotype eq "self") {
		push @e_hw, "";
		push @e_type, "NT";
		push @e_syntnb, 1;
		push @e_ispron, ($pron > 0 ? 1 : 0);
		push @e_infotype, "";
		$pushedsomething = 1;
	      }
	    }
	    $globallypushedsomething = 1 if $pushedsomething;
	  }
	  if ($globallypushedsomething) {
	    for my $lid (sort {length($e_type[$b]) <=> length($e_type[$a]) || $e_syntnb[$a] <=> $e_syntnb[$b]} 0..$#e_hw) {
	      if ($e_ispron[$lid] > 0) {
		if ($hw =~ /^[aeiouâêîôûyäëïöüÿ]/i) {
		  $ilex{$hw}{$cat}[$id]{semid} = "s'Lemma";
		} else {
		  $ilex{$hw}{$cat}[$id]{semid} = "se Lemma";
		}
	      } else {
		$ilex{$hw}{$cat}[$id]{semid} = "Lemma";
	      }
	      if ($e_hw[$lid] ne "") { # verification
		my $ok = 0;
		for (split /,/, $e_type[$lid]) {
		  if (defined $lefff{$e_hw[$lid]}{$lefffcat} && defined $lefff{$e_hw[$lid]}{$lefffcat}{$_}) {
		    $ok = 1;
		    last;
		  }
		}
		die "\$lefff{$e_hw[$lid]}{$e_type[$lid]} undefined" if $ok == 0;
	      }
	      if ($e_hw[$lid] ne "") { # actually doing the job
		for my $f (split /,/, $e_type[$lid]) {
		  if (defined $lefff{$e_hw[$lid]}{$lefffcat} && defined $lefff{$e_hw[$lid]}{$lefffcat}{$f}) {
		    for (0..$#{$lefff{$e_hw[$lid]}{$lefffcat}{$f}}) {
		      push @{$ilex{$hw}{$cat}[$id]{subcat}}, $lefff{$e_hw[$lid]}{$lefffcat}{$f}[$_]{subcat};
		      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, $lefff{$e_hw[$lid]}{$lefffcat}{$f}[$_]{syntfeats};
		      push @{$ilex{$hw}{$cat}[$id]{redistr}}, $lefff{$e_hw[$lid]}{$lefffcat}{$f}[$_]{redistr};
		      push @{$ilex{$hw}{$cat}[$id]{leffflemma}}, $e_hw[$lid];
		      push @{$ilex{$hw}{$cat}[$id]{synttype}}, $e_type[$lid];
		      push @{$ilex{$hw}{$cat}[$id]{infotype}}, ($e_infotype[$lid] eq "" ? "(default)" : $e_infotype[$lid]);
		    }
		  }
		}
	      } else {
		if ($e_ispron[$lid] == 0) {
		  if ($e_type[$lid] eq "N" || $e_type[$lid] eq "NT" || $e_type[$lid] eq "T") {
		    push @{$ilex{$hw}{$cat}[$id]{subcat}}, "<Suj:cln|sn,Obj:(cla|sn)>";
		    push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "cat=v";
		    push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%actif,\%passif,\%ppp_employé_comme_adj";
		    push @{$ilex{$hw}{$cat}[$id]{synttype}}, $e_type[$lid];
		    push @{$ilex{$hw}{$cat}[$id]{infotype}}, ($e_infotype[$lid] eq "" ? "(default)" : $e_infotype[$lid]);
		  } elsif ($e_type[$lid] eq "I" || $e_type[$lid] eq "NI") {
		    push @{$ilex{$hw}{$cat}[$id]{subcat}}, "<Suj:cln|sn>";
		    push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "cat=v";
		    push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%actif";
		    push @{$ilex{$hw}{$cat}[$id]{synttype}}, $e_type[$lid];
		    push @{$ilex{$hw}{$cat}[$id]{infotype}}, ($e_infotype[$lid] eq "" ? "(default)" : $e_infotype[$lid]);
		  } elsif ($e_type[$lid] eq "i" || $e_type[$lid] eq "Ni") {
		    push @{$ilex{$hw}{$cat}[$id]{subcat}}, "<>";
		    push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "cat=v";
		    push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%actif";
		    push @{$ilex{$hw}{$cat}[$id]{synttype}}, $e_type[$lid];
		    push @{$ilex{$hw}{$cat}[$id]{infotype}}, ($e_infotype[$lid] eq "" ? "(default)" : $e_infotype[$lid]);
		  } elsif ($e_type[$lid] eq "1") {
		    push @{$ilex{$hw}{$cat}[$id]{subcat}}, "<Suj:cln|sn>";
		    push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "cat=v";
		    push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%actif,\%actif_impersonnel";
		    push @{$ilex{$hw}{$cat}[$id]{synttype}}, $e_type[$lid];
		    push @{$ilex{$hw}{$cat}[$id]{infotype}}, ($e_infotype[$lid] eq "" ? "(default)" : $e_infotype[$lid]);
		  } else {
		    die "Cases where \$e_type[\$lid] eq \"$e_type[$lid]\" are not handled yet";
		  }
		} else {
		  die "Cases where \$e_ispron[\$lid] != 0 are not handled yet";
		}
	      }
	      last;
	    }
	    last;
	  }
	}
	#print "$selected_linestart\t$selected_hw/$selected_type:$selected_syntnb\n";
      } else { # other than verbs
	for my $infotype ("mansubcat", "pg", "g", "desc", "selfdmf", "self") {
	  my (@e_hw, @e_type, @e_syntnb, @e_infotype);
	  my $globallypushedsomething;
	  die if $hw eq "";
	  for my $r (split /\s*\|\|\s*/, get_info_from_self_or_variants($hw,$cat,$id,$infotype,0)) {
	    next if $r eq "";
	    my $pushedsomething = 0;
	    #print STDERR "<$r>\n" if $r eq "abaissement";
	    for (split /[,;] ?/, $r) {
	      #print STDERR "$hw:$infotype: $cat / $_ ($r)\n" if $r eq "abaissement";
	      die "ERROR: '$hw/$cat/$id' has an empty cat" if $cat eq "";
	      $lefffcat = cat_lemma2lefffcat($cat,$_);
	      my $originfo = $_;
	      s/^\(.*?\) *//;
	      if (defined $lefff{$_} && defined $lefff{$_}{$lefffcat}) {
		push @e_hw, $_;
		push @e_type, "";
		push @e_syntnb, (defined $lefff{$_}{$lefffcat}{""} ? $#{$lefff{$_}{$lefffcat}{""}}+1 : 0);
		push @e_infotype, $infotype;
		$pushedsomething = 1;
	      }
	    }
	    $globallypushedsomething = 1 if $pushedsomething;
	  }
	  if ($globallypushedsomething) {
	    for my $lid (sort {length($e_type[$b]) <=> length($e_type[$a]) || $e_syntnb[$a] <=> $e_syntnb[$b]} 0..$#e_hw) {
	      $ilex{$hw}{$cat}[$id]{semid} = "Lemma";
	      die "ERROR: '$hw/$cat/$id' has an empty cat" if $cat eq "";
	      $lefffcat = cat_lemma2lefffcat($cat,$_);
	      #print STDERR "<$hw>$lid!!!\n" if $hw eq "abaissement";	      
	      if ($e_hw[$lid] ne "") { # verification
		my $ok = 0;
		if ($e_type[$lid] eq "") {
		  if (defined $lefff{$e_hw[$lid]}{$lefffcat}{""}) {
		    $ok = 1;
		  }
		} else {
		  for (split /,/, $e_type[$lid]) {
		    if (defined $lefff{$e_hw[$lid]}{$lefffcat}{$_}) {
		      $ok = 1;
		    }
		  }
		}
		die "\$lefff{$e_hw[$lid]}{$lefffcat}{$e_type[$lid]} undefined" if $ok == 0;
	      }
	      #print STDERR "<$hw>/$lid/$e_type[$lid]!!!\n" if $hw eq "abaissement";
	      if ($e_type[$lid] eq "") {
		#print STDERR "push \@{\$ilex{$hw}{$cat}[$id]{subcat}}, \$lefff{$e_hw[$lid]}{$lefffcat}{\"\"}[$_]{subcat}=$lefff{$e_hw[$lid]}{$lefffcat}{\"\"}[$_]{subcat};\n" if $hw =~ /abaissement/;
		push @{$ilex{$hw}{$cat}[$id]{subcat}}, $lefff{$e_hw[$lid]}{$lefffcat}{""}[$_]{subcat};
		push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, $lefff{$e_hw[$lid]}{$lefffcat}{""}[$_]{syntfeats};
		push @{$ilex{$hw}{$cat}[$id]{redistr}}, $lefff{$e_hw[$lid]}{$lefffcat}{""}[$_]{redistr};
		push @{$ilex{$hw}{$cat}[$id]{leffflemma}}, $e_hw[$lid];
		push @{$ilex{$hw}{$cat}[$id]{infotype}}, ($e_infotype[$lid] eq "" ? "(default)" : $e_infotype[$lid]);
	      } else {
		for my $f (split /,/, $e_type[$lid]) {
		  if (defined $lefff{$e_hw[$lid]}{$lefffcat} && defined $lefff{$e_hw[$lid]}{$lefffcat}{$f}) {
		    for (0..$#{$lefff{$e_hw[$lid]}{$lefffcat}{$f}}) {
		      push @{$ilex{$hw}{$cat}[$id]{subcat}}, $lefff{$e_hw[$lid]}{$lefffcat}{$f}[$_]{subcat};
		      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, $lefff{$e_hw[$lid]}{$lefffcat}{$f}[$_]{syntfeats};
		      push @{$ilex{$hw}{$cat}[$id]{redistr}}, $lefff{$e_hw[$lid]}{$lefffcat}{$f}[$_]{redistr};
		      push @{$ilex{$hw}{$cat}[$id]{leffflemma}}, $e_hw[$lid];
		      push @{$ilex{$hw}{$cat}[$id]{infotype}}, ($e_infotype[$lid] eq "" ? "(default)" : $e_infotype[$lid]);
		    }
		  }
		}
	      }
	      last;
	    }
	    last;
	  } elsif ($infotype eq "self") {
	    $ilex{$hw}{$cat}[$id]{semid} = "Lemma";
	    if ($cat =~ /^ADJ(ord|pos|qua|gen)|PROpos$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "<Suj:cln|sn>";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "cat=adj";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%adj_personnel";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^ADJcar$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "<Suj:(sn)>";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "cat=adj,adjtype=car";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%adj_personnel";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^ADVint$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "<Suj:cln|sn>";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "cat=adj";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%adj_personnel";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	      #		} elsif ($cat eq "ADJdem") {
	    } elsif ($cat =~ /^NOMcom$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "<Objde:(de-sinf|de-sn),Objà:(à-sinf)>";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "cat=nc";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^NOMpro$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "<Suj:(sn)>";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "cat=np";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^ADVgen$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "cat=adv";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^PRE$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "<Obj:sa|sadv|sn>";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "cat=prep";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^INT$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "cat=pres";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^PROind$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^PROper$/) { # todo
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^PROper_refl/) { # todo
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "refl=+";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "case=acc";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "case=dat";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^PROdem$/) { # todo
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^DETpos$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "det=+";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^DET(ind|ndf)$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "define=-,det=+";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^PROcar$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "protype=car";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^DETcar$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "define=-,det=+,dettype=car";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^DETdef$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "define=+,det=+";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^DETdem$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "demonstrative=+,det=+";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^DETrel/) { # to be confirmed
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "det=+";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^DETint/) { # to be confirmed
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "det=+,qu=+";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^ADVneg$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "neg=+";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^CONsub$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      if ($hw =~ /(^| )([kq]u?e?'?)$/) {
		push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "wh=+";
	      } else {
		push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "";
	      }
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^PROrel/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^PROint$/) { # todo (case)
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^CONcoo$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "<arg1,arg2>";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } elsif ($cat =~ /^PROimp$/) {
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "<arg1,arg2>";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "";
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    } else {
	      #print STDERR ">>> $cat\n";
	      push @{$ilex{$hw}{$cat}[$id]{subcat}}, "";
	      die "ERROR: '$hw/$cat/$id' has an empty cat" if $cat eq "";
	      push @{$ilex{$hw}{$cat}[$id]{syntfeats}}, "cat=".cat_lemma2lefffcat($cat,$hw);
	      push @{$ilex{$hw}{$cat}[$id]{redistr}}, "\%default";
	      push @{$ilex{$hw}{$cat}[$id]{infotype}}, "(default)";
	    }
	  }
	}
      }
    }
  }
}



# print, out : outputstream version in terminal

for $hw (sort {remove_diacritics($a) cmp remove_diacritics($b)} keys %ilex) {
  for $cat (sort {$a cmp $b} keys %{$ilex{$hw}}) {
    die "ERROR: '$hw/$cat' has an empty cat" if $cat eq "";
    my $upos = cat_lemma2upos($cat,$hw);
    for $id (1..$#{$ilex{$hw}{$cat}}) {
      die "$hw/$cat/$id has no subcat" unless defined $ilex{$hw}{$cat}[$id]{subcat};
      for my $subid (0..$#{$ilex{$hw}{$cat}[$id]{subcat}}) {
	my $lefffcat = cat_lemma2lefffcat($cat,$hw);
	my $res = ""; # gg
	$res .= "".$ilex{$hw}{$cat}[$id]{id}.""; #gg
	$res .= "\t"; #gg
	$res .= "$hw";
	if ($#{$ilex{$hw}{$cat}} > 1) {
	  $res .= "__" if $hw !~ /__/;
	  $res .= "___".$id;
	}
	$res .= "\t";
	if ($lefffcat eq "v") {
	  if ($ilex{$hw}{$cat}[$id]{inflclass} =~ /^[0-9]?$/) {
	    if ($hw =~ /[^i]er$/) {
	      $ilex{$hw}{$cat}[$id]{inflclass} = "v-er";
	    } else {
	      $ilex{$hw}{$cat}[$id]{inflclass} = "v-unkconj";
	    }
	  } elsif ($ilex{$hw}{$cat}[$id]{inflclass} =~ /^v-ir$/) {
	    if ($hw =~ /ir$/) {
	      $ilex{$hw}{$cat}[$id]{inflclass} = "v-unkconj";
	    } else {
	      #print STDERR "WARNING: Not only the class 'v-ir' is underspecified, but in addition it seems incompatible with lemma $hw";
	      if ($hw =~ /[^i]er$/) {
		$ilex{$hw}{$cat}[$id]{inflclass} = "v-er";
	      } else {
		$ilex{$hw}{$cat}[$id]{inflclass} = "v-unkconj";
	      }
	    }
	  }
	} else {
	  die "Invalid inflection class '$ilex{$hw}{$cat}[$id]{inflclass}' for entry $hw\n";
	}
	$res .= $ilex{$hw}{$cat}[$id]{inflclass};
	$res .=  "\t";
	$res .=  "100;";
	$res .=  $ilex{$hw}{$cat}[$id]{semid};
	$res .=  ";";
	die "ERROR: '$hw/$cat' has an empty cat" if $cat eq "";
	$res .=  $lefffcat;
	$res .=  ";";
	$res .=  $ilex{$hw}{$cat}[$id]{subcat}[$subid];
	$res .=  ";";
	$res .=  "upos=".$upos;
	$res .=  "," unless $ilex{$hw}{$cat}[$id]{syntfeats}[$subid] eq "";
	$res .=  $ilex{$hw}{$cat}[$id]{syntfeats}[$subid];
	$res .=  ";";
	$res .=  $ilex{$hw}{$cat}[$id]{redistr}[$subid];
	$res .=  "\t# ";
	$res .=  "<link src=\"TL\" loc=\"$ilex{$hw}{$cat}[$id]{tlloc}\" entry=\"$ilex{$hw}{$cat}[$id]{tlhw}\" ms=\"$ilex{$hw}{$cat}[$id]{tlms}\" def=\"$tl_defs{$ilex{$hw}{$cat}[$id]{tlsourcehw}}{$tlsourcehw_hw2cat{$ilex{$hw}{$cat}[$id]{tlsourcehw}}{$hw}}{def}\"/>" if defined $ilex{$hw}{$cat}[$id]{tlsourcehw};
	$res .=  "<link src=\"Godefroy\" loc=\"$ilex{$hw}{$cat}[$id]{godefroyloc}\" entry=\"$ilex{$hw}{$cat}[$id]{godefroyentry}\" def=\"$ilex{$hw}{$cat}[$id]{godefroydef}\"/>" if defined $ilex{$hw}{$cat}[$id]{godefroyentry};
	if (defined $ilex{$hw}{$cat}[$id]{dectentry}) {
	  $res .=  "<link src=\"DECT\" loc=\"$ilex{$hw}{$cat}[$id]{dectpage}\" entry=\"$ilex{$hw}{$cat}[$id]{dectentry}\" ms=\"$ilex{$hw}{$cat}[$id]{dectcat}\" freq=\"$ilex{$hw}{$cat}[$id]{dectfreq}\"";
	  $res .=  " def=\"$ilex{$hw}{$cat}[$id]{dectdef}\"" if $ilex{$hw}{$cat}[$id]{dectdef} ne "";
	  $res .=  "/>";
	  $res .=  "<link src=\"F-B\" ref=\"DECT\" entry=\"$ilex{$hw}{$cat}[$id]{dectfb}\"/>" if $ilex{$hw}{$cat}[$id]{dectfb} ne "";
	  $res .=  "<link src=\"TLF\" ref=\"DECT\" entry=\"$ilex{$hw}{$cat}[$id]{decttlf}\"/>" if $ilex{$hw}{$cat}[$id]{decttlf} ne "";
	  $res .=  "<link src=\"AND\" ref=\"DECT\" entry=\"$ilex{$hw}{$cat}[$id]{dectand}\"/>" if $ilex{$hw}{$cat}[$id]{dectand} ne "";
	  $res .=  "<link src=\"DMF\" ref=\"DECT\" entry=\"$ilex{$hw}{$cat}[$id]{dectdmf}\"/>" if $ilex{$hw}{$cat}[$id]{dectdmf} ne "";
	  $res .=  "<link src=\"FEW\" ref=\"DECT\" loc=\"$ilex{$hw}{$cat}[$id]{dectfewref}\" entry=\"$ilex{$hw}{$cat}[$id]{dectfew}\"/>" if $ilex{$hw}{$cat}[$id]{dectfew} ne "";
	  
	}
	$res .=  "<syntinfosource via=\"$ilex{$hw}{$cat}[$id]{infotype}[$subid]\"";
	$res .=  " leffflemma=\"$ilex{$hw}{$cat}[$id]{leffflemma}[$subid]\"" if defined($ilex{$hw}{$cat}[$id]{leffflemma}[$subid]) && $ilex{$hw}{$cat}[$id]{leffflemma}[$subid] ne "";
	$res .=  " synttype=\"$ilex{$hw}{$cat}[$id]{synttype}[$subid]\"" if defined($ilex{$hw}{$cat}[$id]{synttype}[$subid]) && $ilex{$hw}{$cat}[$id]{synttype}[$subid] ne "";
	$res .=  "/>";
	$res .=  "<variantof lemma=\"$ilex{$hw}{$cat}[$id]{varianthw}\" id=\"$ilex{$hw}{$cat}[$id]{variantid}\" cat=\"$ilex{$hw}{$cat}[$id]{variantcat}\"/>" if defined($ilex{$hw}{$cat}[$id]{varianthw});
	if (defined($ilex{$hw}{$cat}[$id]{variants})) {
	  for (0..$#{$ilex{$hw}{$cat}[$id]{variants}}) {
	    $res .= "<hasvariant lemma=\"$ilex{$hw}{$cat}[$id]{variants}[$_]{hw}\" id=\"$ilex{$hw}{$cat}[$id]{variants}[$_]{id}\" cat=\"$ilex{$hw}{$cat}[$id]{variants}[$_]{cat}\"/>";
	  }
	}
	$res .= "\n";
	print $res;
      }
    }
  }
}



# # print, out
# my %fh;

# for $hw (sort {remove_diacritics($a) cmp remove_diacritics($b)} keys %ilex) {
#   for $cat (sort {$a cmp $b} keys %{$ilex{$hw}}) {
#     die "ERROR: '$hw/$cat' has an empty cat" if $cat eq "";
# #    #print STDERR "$cat/$hw\n";
#     my $upos = cat_lemma2upos($cat,$hw);
#     unless (defined($fh{$upos})) {
#       open $fh{$upos}, ">$upos.ilex" || die $!;
#       binmode $fh{$upos}, ":utf8";
#     }
#     for $id (1..$#{$ilex{$hw}{$cat}}) {
#       die "$hw/$cat/$id has no subcat" unless defined $ilex{$hw}{$cat}[$id]{subcat};
#       for my $subid (0..$#{$ilex{$hw}{$cat}[$id]{subcat}}) {
# 	my $lefffcat = cat_lemma2lefffcat($cat,$hw);
# 	print {$fh{$upos}} "".$ilex{$hw}{$cat}[$id]{id}.""; #gg
# 	print {$fh{$upos}} "\t"; #gg
# 	print {$fh{$upos}} "$hw";
# 	if ($#{$ilex{$hw}{$cat}} > 1) {
# 	  print {$fh{$upos}} "__" if $hw !~ /__/;
# 	  print {$fh{$upos}} "___".$id;
# 	}
# 	print {$fh{$upos}} "\t";
# 	if ($lefffcat eq "v") {
# 	  if ($ilex{$hw}{$cat}[$id]{inflclass} =~ /^[0-9]?$/) {
# 	    if ($hw =~ /[^i]er$/) {
# 	      $ilex{$hw}{$cat}[$id]{inflclass} = "v-er";
# 	    } else {
# 	      $ilex{$hw}{$cat}[$id]{inflclass} = "v-unkconj";
# 	    }
# 	  } elsif ($ilex{$hw}{$cat}[$id]{inflclass} =~ /^v-ir$/) {
# 	    if ($hw =~ /ir$/) {
# 	      $ilex{$hw}{$cat}[$id]{inflclass} = "v-unkconj";
# 	    } else {
# 	      #print STDERR "WARNING: Not only the class 'v-ir' is underspecified, but in addition it seems incompatible with lemma $hw";
# 	      if ($hw =~ /[^i]er$/) {
# 		$ilex{$hw}{$cat}[$id]{inflclass} = "v-er";
# 	      } else {
# 		$ilex{$hw}{$cat}[$id]{inflclass} = "v-unkconj";
# 	      }
# 	    }
# 	  }
# 	} else {
# 	  die "Invalid inflection class '$ilex{$hw}{$cat}[$id]{inflclass}' for entry $hw\n";
# 	}
# 	print {$fh{$upos}} $ilex{$hw}{$cat}[$id]{inflclass};
# 	print {$fh{$upos}} "\t";
# 	print {$fh{$upos}} "100;";
# 	print {$fh{$upos}} $ilex{$hw}{$cat}[$id]{semid};
# 	print {$fh{$upos}} ";";
# 	die "ERROR: '$hw/$cat' has an empty cat" if $cat eq "";
# 	print {$fh{$upos}} $lefffcat;
# 	print {$fh{$upos}} ";";
# 	print {$fh{$upos}} $ilex{$hw}{$cat}[$id]{subcat}[$subid];
# 	print {$fh{$upos}} ";";
# 	print {$fh{$upos}} "upos=".$upos;
# 	print {$fh{$upos}} "," unless $ilex{$hw}{$cat}[$id]{syntfeats}[$subid] eq "";
# 	print {$fh{$upos}} $ilex{$hw}{$cat}[$id]{syntfeats}[$subid];
# 	print {$fh{$upos}} ";";
# 	print {$fh{$upos}} $ilex{$hw}{$cat}[$id]{redistr}[$subid];
# 	print {$fh{$upos}} "\t# ";
# 	print {$fh{$upos}} "<link src=\"TL\" loc=\"$ilex{$hw}{$cat}[$id]{tlloc}\" entry=\"$ilex{$hw}{$cat}[$id]{tlhw}\" ms=\"$ilex{$hw}{$cat}[$id]{tlms}\" def=\"$tl_defs{$ilex{$hw}{$cat}[$id]{tlsourcehw}}{$tlsourcehw_hw2cat{$ilex{$hw}{$cat}[$id]{tlsourcehw}}{$hw}}{def}\"/>" if defined $ilex{$hw}{$cat}[$id]{tlsourcehw};
# 	print {$fh{$upos}} "<link src=\"Godefroy\" loc=\"$ilex{$hw}{$cat}[$id]{godefroyloc}\" entry=\"$ilex{$hw}{$cat}[$id]{godefroyentry}\" def=\"$ilex{$hw}{$cat}[$id]{godefroydef}\"/>" if defined $ilex{$hw}{$cat}[$id]{godefroyentry};
# 	if (defined $ilex{$hw}{$cat}[$id]{dectentry}) {
# 	  print {$fh{$upos}} "<link src=\"DECT\" loc=\"$ilex{$hw}{$cat}[$id]{dectpage}\" entry=\"$ilex{$hw}{$cat}[$id]{dectentry}\" ms=\"$ilex{$hw}{$cat}[$id]{dectcat}\" freq=\"$ilex{$hw}{$cat}[$id]{dectfreq}\"";
# 	  print {$fh{$upos}} " def=\"$ilex{$hw}{$cat}[$id]{dectdef}\"" if $ilex{$hw}{$cat}[$id]{dectdef} ne "";
# 	  print {$fh{$upos}} "/>";
# 	  print {$fh{$upos}} "<link src=\"F-B\" ref=\"DECT\" entry=\"$ilex{$hw}{$cat}[$id]{dectfb}\"/>" if $ilex{$hw}{$cat}[$id]{dectfb} ne "";
# 	  print {$fh{$upos}} "<link src=\"TLF\" ref=\"DECT\" entry=\"$ilex{$hw}{$cat}[$id]{decttlf}\"/>" if $ilex{$hw}{$cat}[$id]{decttlf} ne "";
# 	  print {$fh{$upos}} "<link src=\"AND\" ref=\"DECT\" entry=\"$ilex{$hw}{$cat}[$id]{dectand}\"/>" if $ilex{$hw}{$cat}[$id]{dectand} ne "";
# 	  print {$fh{$upos}} "<link src=\"DMF\" ref=\"DECT\" entry=\"$ilex{$hw}{$cat}[$id]{dectdmf}\"/>" if $ilex{$hw}{$cat}[$id]{dectdmf} ne "";
# 	  print {$fh{$upos}} "<link src=\"FEW\" ref=\"DECT\" loc=\"$ilex{$hw}{$cat}[$id]{dectfewref}\" entry=\"$ilex{$hw}{$cat}[$id]{dectfew}\"/>" if $ilex{$hw}{$cat}[$id]{dectfew} ne "";
	  
# 	}
# 	print {$fh{$upos}} "<syntinfosource via=\"$ilex{$hw}{$cat}[$id]{infotype}[$subid]\"";
# 	print {$fh{$upos}} " leffflemma=\"$ilex{$hw}{$cat}[$id]{leffflemma}[$subid]\"" if defined($ilex{$hw}{$cat}[$id]{leffflemma}[$subid]) && $ilex{$hw}{$cat}[$id]{leffflemma}[$subid] ne "";
# 	print {$fh{$upos}} " synttype=\"$ilex{$hw}{$cat}[$id]{synttype}[$subid]\"" if defined($ilex{$hw}{$cat}[$id]{synttype}[$subid]) && $ilex{$hw}{$cat}[$id]{synttype}[$subid] ne "";
# 	print {$fh{$upos}} "/>";
# 	print {$fh{$upos}} "<variantof lemma=\"$ilex{$hw}{$cat}[$id]{varianthw}\" id=\"$ilex{$hw}{$cat}[$id]{variantid}\" cat=\"$ilex{$hw}{$cat}[$id]{variantcat}\"/>" if defined($ilex{$hw}{$cat}[$id]{varianthw});
# 	if (defined($ilex{$hw}{$cat}[$id]{variants})) {
# 	  for (0..$#{$ilex{$hw}{$cat}[$id]{variants}}) {
# 	    print {$fh{$upos}} "<hasvariant lemma=\"$ilex{$hw}{$cat}[$id]{variants}[$_]{hw}\" id=\"$ilex{$hw}{$cat}[$id]{variants}[$_]{id}\" cat=\"$ilex{$hw}{$cat}[$id]{variants}[$_]{cat}\"/>";
# 	  }
# 	}
# 	print {$fh{$upos}} "\n";
#       }
#     }
#   }
# }





sub remove_diacritics {
  my ($s) = @_;
  $s =~ tr/ǽǣáàâäąãăåćčçďéèêëęěğìíîĩĭıïĺľłńñňòóôõöøŕřśšşťţùúûũüǔỳýŷÿźẑżžÁÀÂÄĄÃĂÅĆČÇĎÉÈÊËĘĚĞÌÍÎĨĬİÏĹĽŁŃÑŇÒÓÔÕÖØŔŘŚŠŞŤŢÙÚÛŨÜǓỲÝŶŸŹẐŻŽ/ææaaaaaaaacccdeeeeeegiiiiiiilllnnnoooooorrsssttuuuuuuyyyyzzzzAAAAAAAACCCDEEEEEEGIIIIIIILLLNNNOOOOOORRSSSTTUUUUUUYYYYZZZZ/;
  $s =~ s/œ/oe/g;
  $s =~ s/æ/ae/g;
  $s =~ s/ƣ/oi/g;
  $s =~ s/ĳ/ij/g;
  $s =~ s/ȣ/ou/g;
  $s =~ s/Œ/OE/g;
  $s =~ s/Æ/AE/g;
  $s =~ s/Ƣ/OI/g;
  $s =~ s/Ĳ/IJ/g;
  $s =~ s/Ȣ/OU/g;
  return $s;
}

sub cat_lemma2lefffcat {
  my ($cat,$lemma) = @_;
  return "det" if $cat eq "ADJcar"; # ??? à vérifier
  return "adj" if $cat eq "ADJdem"; #? (only ‘cil’)
  return "det" if $cat eq "ADJind";
  return "adj" if $cat eq "ADJint"; #? (only ‘confait’)
  return "adj" if $cat eq "ADJord";
  return "adj" if $cat eq "ADJpos";
  return "adj" if $cat eq "ADJqua";
  return "adv" if $cat eq "ADVgen";
  return "pri" if $cat eq "ADVint"; # @pro_nom|@pro_acc|@pro_loc|ε
  return "advneg" if $cat eq "ADVneg";
  return "adv" if $cat eq "ADVrel"; # ???
  return "coo" if $cat eq "CONcoo"; # ε
  return "que" if $cat eq "CONsub" && $lemma eq "que"; # wh=+ (?)
  return "csu" if $cat eq "CONsub" && $lemma ne "que"; # ε
  return "csu" if $cat eq "CONsub_o"; # ε
  return "que" if $cat eq "CONsub_pre" && $lemma eq "que"; # wh=+
  return "det" if $cat eq "DETcar"; # define=-,det=+
  return "det" if $cat eq "DETdef"; # define=+,det=+
  return "det" if $cat eq "DETdem"; # demonstrative=+,det=+
  return "det" if $cat eq "DETind"; # define=-,det=+
  return "det" if $cat eq "DETint"; # det=+,qu=+
  return "det" if $cat eq "DETndf"; # define=-,det=+
  return "det" if $cat eq "DETpos"; # @poss,det=+
  return "prel" if $cat eq "DETrel"; # @pro_nom|@pro_acc|@pro_gen|@pro_loc|ε
  return "prel" if $cat eq "DETrel_o";
  return "pres" if $cat eq "INT";
  return "nc" if $cat eq "NOMcom";
  return "np" if $cat eq "NOMpro";
  return "prep" if $cat eq "PRE"; #?
  return "advPref" if $cat eq "PREFIX";
  return "pro" if $cat eq "PROcar"; # ε
  return "pro" if $cat eq "PROdem"; # ε
  return "ilimp" if $cat eq "PROimp"; # imp=+
  return "pro" if $cat eq "PROind"; # define=-
  return "pri" if $cat eq "PROint"; # @pro_nom|@pro_acc|ε
  return "pro" if $cat eq "PROper"; # ε
  return "clr" if $cat eq "PROper_refl"; # refl=+
  return "adj" if $cat eq "PROpos";
  return "prel" if $cat eq "PROrel"; # @pro_nom|@pro_acc|@pro_gen|@pro_loc|ε
  return "prel" if $cat eq "PROrel_adv"; # @pro_nom|@pro_acc|@pro_gen|@pro_loc|ε
  return "csu" if $cat eq "PROrel_ind"; # wh=+
  return "prel" if $cat eq "PROrel_o"; # @pro_nom|@pro_acc|@pro_gen|@pro_loc|ε
  return "v" if $cat eq "VER";
  return $cat if $cat =~ /^parentf|parento|epsilon|poncts|ponctw$/;
  die "Unknown input category '$cat' (lemma '$lemma')";
  return "__ERROR__";
}

sub cat_lemma2upos {
  my ($cat,$lemma) = @_;
  return "DET" if $cat eq "ADJcar"; # ??? à vérifier
  return "ADJ" if $cat eq "ADJdem"; #? (only ‘cil’)
  return "DET" if $cat eq "ADJind";
  return "ADJ" if $cat eq "ADJint"; #? (only ‘confait’)
  return "ADJ" if $cat eq "ADJord";
  return "ADJ" if $cat eq "ADJpos";
  return "ADJ" if $cat eq "ADJqua";
  return "ADV" if $cat eq "ADVgen";
  return "PRON" if $cat eq "ADVint"; # @pro_nom|@pro_acc|@pro_loc|ε
  return "ADV" if $cat eq "ADVneg";
  return "ADV" if $cat eq "ADVrel"; # ???
  return "CCONJ" if $cat eq "CONcoo"; # ε
  return "SCONJ" if $cat eq "CONsub" && $lemma eq "que"; # wh=+ (?)
  return "SCONJ" if $cat eq "CONsub" && $lemma ne "que"; # ε
  return "SCONJ" if $cat eq "CONsub_o"; # ε
  return "SCONJ" if $cat eq "CONsub_pre" && $lemma eq "que"; # wh=+
  return "DET" if $cat eq "DETcar"; # define=-,det=+
  return "DET" if $cat eq "DETdef"; # define=+,det=+
  return "DET" if $cat eq "DETdem"; # demonstrative=+,det=+
  return "DET" if $cat eq "DETind"; # define=-,det=+
  return "DET" if $cat eq "DETint"; # det=+,qu=+
  return "DET" if $cat eq "DETndf"; # define=-,det=+
  return "DET" if $cat eq "DETpos"; # @poss,det=+
  return "PRON" if $cat eq "DETrel"; # @pro_nom|@pro_acc|@pro_gen|@pro_loc|ε
  return "PRON" if $cat eq "DETrel_o";
  return "INTJ" if $cat eq "INT";
  return "NOUN" if $cat eq "NOMcom";
  return "PROPN" if $cat eq "NOMpro";
  return "ADP" if $cat eq "PRE"; #?
  return "ADV" if $cat eq "PREFIX";
  return "PRON" if $cat eq "PROcar"; # ε
  return "PRON" if $cat eq "PROdem"; # ε
  return "PRON" if $cat eq "PROimp"; # imp=+
  return "PRON" if $cat eq "PROind"; # define=-
  return "PRON" if $cat eq "PROint"; # @pro_nom|@pro_acc|ε
  return "PRON" if $cat eq "PROper"; # ε
  return "PRON" if $cat eq "PROper_refl"; # refl=+
  return "ADJ" if $cat eq "PROpos";
  return "PRON" if $cat eq "PROrel"; # @pro_nom|@pro_acc|@pro_gen|@pro_loc|ε
  return "PRON" if $cat eq "PROrel_adv"; # @pro_nom|@pro_acc|@pro_gen|@pro_loc|ε
  return "SCONJ" if $cat eq "PROrel_ind"; # wh=+
  return "PRON" if $cat eq "PROrel_o"; # @pro_nom|@pro_acc|@pro_gen|@pro_loc|ε
  return "VERB" if $cat eq "VER";
  return "PUNCT" if $cat =~ /^parentf|parento|poncts|ponctw$/;
  return "epsilon" if $cat eq "epsilon";
  die "Unknown input category '$cat' (lemma '$lemma')";
  return "__ERROR__";
}


sub process_last_udsentence {
  my %scats;
  for my $i (1..$#words) {
    next unless $words[$words[$i]{sgov}]{upos} eq "VERB";
    next unless $words[$i]{slabel} =~ /^[nc]subj|[xc]comp|i?obj/;
    $scats{$words[$i]{sgov}}{$words[$i]{slabel}}{form} = $words[$i]{form};
    $scats{$words[$i]{sgov}}{$words[$i]{slabel}}{upos} = $words[$i]{upos};
    $scats{$words[$i]{sgov}}{$words[$i]{slabel}}{lemma} = $words[$i]{lemma};
  }
  for my $i (sort keys %scats) {
#      #print STDERR $words[$i]{line}."\t$i\t".$words[$i]{form}."\t".(join "|", sort {$a cmp $b} keys %{$form_lemma{$words[$i]{form}}})."\t".join(";", map {udlabel2alexina($_,$scats{$i}{$_}{upos})} sort {oblrank($a) <=> oblrank($b)} keys $scats{$i})."\n";
    if (scalar keys %{$form_lemma{$words[$i]{form}}} == 1) {
      $possible_scats{join "|", sort {$a cmp $b} keys %{$form_lemma{$words[$i]{form}}}}{join(";", map {udlabel2alexina($_,$scats{$i}{$_}{upos})} sort {oblrank($a) <=> oblrank($b)} keys %{ $scats{$i} } )}++;
    }
  }
}

sub oblrank {
  $_ = shift;
  return 1 if /subj/;
  return 3 if /iobj/;
  return 2;
}

sub udlabel2alexina {
  my ($l,$upos) = @_;
  if ($l =~ /^nsubj$/ && $upos eq "PRON") {return "Suj:cln"}
  elsif ($l =~ /^nsubj$/) {return "Suj:sn"}
  elsif ($l =~ /^csubj$/) {return "Suj:sinf|scompl"}
  elsif ($l =~ /^obj$/) {return "Obj:sn|cla"}
  elsif ($l =~ /^ccomp$/) {return "Obj:scompl"}
  elsif ($l =~ /^xcomp$/ && $upos eq "VERB") {return "Obj:sinf"}
  elsif ($l =~ /^xcomp$/ && $upos =~ /^NOUN|PROPN$/) {return "Att:sn"}
  elsif ($l =~ /^xcomp$/ && $upos eq "ADJ") {return "Att:sa"}
  elsif ($l =~ /^xcomp$/ && $upos eq "ADV") {return "Att:sadv"}
  elsif ($l =~ /^xcomp$/ && $upos eq "PRON") {return "Att:sn"}
  elsif ($l =~ /^iobj$/ && $upos =~ /^NOUN|PROPN$/) {return "Obji:sn"}
  elsif ($l =~ /^iobj$/ && $upos eq "PRON") {return "Obji:clo"}
  die "$l / $upos";
}

sub get_info_from_self_or_variants {
  my ($hw,$cat,$id,$info,$die_if_not_in_lefff) = @_;
  my $r = "";
  if ($info eq "self") {
    $r = $hw;
  } elsif ($info eq "selfdmf") {
    if (defined($ilex{$hw}{$cat}[$id]{dectdmf}) && $ilex{$hw}{$cat}[$id]{dectdmf} ne "" && defined($lefff{$ilex{$hw}{$cat}[$id]{dectdmf}})) {
      return $ilex{$hw}{$cat}[$id]{dectdmf};
    }
    return $r;
  } elsif (defined($ilex{$hw}{$cat}[$id]{$info}) && $ilex{$hw}{$cat}[$id]{$info} ne "") {
    $r = $ilex{$hw}{$cat}[$id]{$info};
  } elsif (defined($ilex{$hw}{$cat}[$id]{varianthw})
	   && defined($ilex{$ilex{$hw}{$cat}[$id]{varianthw}})
	   && defined($ilex{$ilex{$hw}{$cat}[$id]{varianthw}}{$ilex{$hw}{$cat}[$id]{variantcat}})
	   && defined($ilex{$ilex{$hw}{$cat}[$id]{varianthw}}{$ilex{$hw}{$cat}[$id]{variantcat}}[$ilex{$hw}{$cat}[$id]{variantid}]{$info})
	   && $ilex{$ilex{$hw}{$cat}[$id]{varianthw}}{$ilex{$hw}{$cat}[$id]{variantcat}}[$ilex{$hw}{$cat}[$id]{variantid}]{$info} ne ""
	  ) {
    $r = $ilex{$ilex{$hw}{$cat}[$id]{varianthw}}{$ilex{$hw}{$cat}[$id]{variantcat}}[$ilex{$hw}{$cat}[$id]{variantid}]{$info};#." (V)";
  } elsif (defined($ilex{$hw}{$cat}[$id]{variants})) {
    for (0..$#{$ilex{$hw}{$cat}[$id]{variants}}) {
      if (defined($ilex{$ilex{$hw}{$cat}[$id]{varianthw}})
	  && defined($ilex{$ilex{$hw}{$cat}[$id]{varianthw}}{$ilex{$hw}{$cat}[$id]{variantcat}})
	  && defined($ilex{$ilex{$hw}{$cat}[$id]{varianthw}}{$ilex{$hw}{$cat}[$id]{variantcat}}[$ilex{$hw}{$cat}[$id]{variantid}]{$info})
	  && $ilex{$ilex{$hw}{$cat}[$id]{varianthw}}{$ilex{$hw}{$cat}[$id]{variantcat}}[$ilex{$hw}{$cat}[$id]{variantid}]{$info} ne "") {
	$r = $ilex{$ilex{$hw}{$cat}[$id]{varianthw}}{$ilex{$hw}{$cat}[$id]{variantcat}}[$ilex{$hw}{$cat}[$id]{variantid}]{$info};#." (<V)";
	last;
      }
    }
  }
  die if $die_if_not_in_lefff && !defined($lefff{$r});
  return $r;
}
